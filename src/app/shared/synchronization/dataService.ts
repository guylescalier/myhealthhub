import { SyncAction } from "../flux";
import { DataType } from "./dataType";

export interface TypedSyncAction {
    dataType: DataType;
    syncAction: SyncAction;
}

export interface DataService {
    id: string;
}
