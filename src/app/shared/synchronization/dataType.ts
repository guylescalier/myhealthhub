export enum DataType {
    None = "None",
    StepCount = "StepCount",
    Sleep = "Sleep",
    Weight = "Weight"
}
