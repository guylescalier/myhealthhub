// tslint:disable:no-submodule-imports
import { Inject, Injectable, InjectionToken } from "@angular/core";
import * as _ from "lodash";
import * as moment from "moment";
import { NGXLogger } from "ngx-logger";
import { withLatestFrom } from "rxjs/operators";

import { Configuration, ConfigurationService } from "../configuration";
import {
    asStepCountReader,
    isStepCountReader,
    isStepCountWriter,
} from "../data";
import { arrayToMap } from "../extensions";
import { Dispatcher, SyncAction } from "../flux";
import { DataService } from "./dataService";
import { DataType } from "./dataType";

export const DATA_SERVICES_TOKEN: InjectionToken<DataService> = new InjectionToken<DataService>("DATA_SERVICES");

@Injectable()
export class SynchronizationService {
    public readonly dataReaders: Map<DataType, Set<string>>;
    public readonly dataWriters: Map<DataType, Set<string>>;
    private readonly dataServices: Map<string, DataService>;

    constructor(
        private readonly logger: NGXLogger,
        private readonly dispatcher: Dispatcher,
        private readonly configurationService: ConfigurationService,
        // tslint:disable-next-line:array-type
        @Inject(DATA_SERVICES_TOKEN) dataServices: DataService[]
    ) {
        this.dataServices = arrayToMap(dataServices, x => x.id, x => x);

        this.dataReaders = new Map();
        this.dataWriters = new Map();
        this.setCapacities(dataServices);

        this.dispatcher.syncActions
            .pipe(
                withLatestFrom(this.configurationService.configuration)
            )
            .subscribe(([syncAction, configuration]) => this.synchronize(syncAction, configuration));
    }

    private setCapacities(dataServices: Array<DataService>): void {
        _.forEach(DataType, x => {
            this.dataReaders.set(x, new Set());
            this.dataWriters.set(x, new Set());
        });
        _.forEach(dataServices, x => {
            if (isStepCountReader(x)) {
                this.dataReaders.get(DataType.StepCount).add(x.id);
            }
            if (isStepCountWriter(x)) {
                this.dataWriters.get(DataType.StepCount).add(x.id);
            }
        });
    }

    private synchronize(syncAction: SyncAction, configuration: Configuration): void {
        if (syncAction.date) {
            syncAction.date = moment.utc(syncAction.date).startOf("day").toDate();
        }

        this.logger.debug(`synchronize(${JSON.stringify(syncAction)})`);

        configuration.dataReaders.forEach((dataReaderId, dataType) => {
            try {
                const dataService: DataService = this.dataServices.get(dataReaderId);
                switch (dataType) {
                    case DataType.StepCount:
                        asStepCountReader(dataService)
                            .readStepCounts(syncAction)
                            .subscribe(
                                data => this.logger.debug(`Data read : ${data.date} -> ${data.stepCount} steps`),
                                error => this.logger.error(error),
                                () => this.logger.debug("Data stream completed")
                            );
                        break;
                }
            } catch (error) {
                this.logger.error(`Error synchronizing ${dataType} data with ${dataReaderId} service :`, error);
            }
        });
    }
}
