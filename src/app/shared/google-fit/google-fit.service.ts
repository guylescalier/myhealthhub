import { Injectable } from "@angular/core";
import * as _ from "lodash";
import * as moment from "moment";
import { NGXLogger } from "ngx-logger";
import { Subject } from "rxjs";
import * as applicationModule from "tns-core-modules/application";
import { setTimeout } from "tns-core-modules/timer";

import { DataService, TypedSyncAction } from "../synchronization";

import DataReadRequest = com.google.android.gms.fitness.request.DataReadRequest;
import DataType = com.google.android.gms.fitness.data.DataType;
import Fitness = com.google.android.gms.fitness.Fitness;
import GoogleApiAvailability = com.google.android.gms.common.GoogleApiAvailability;
import GoogleApiClient = com.google.android.gms.common.api.GoogleApiClient;
import TimeUnit = java.util.concurrent.TimeUnit;
import FitnessOptions = com.google.android.gms.fitness.FitnessOptions;
import GoogleSignIn = com.google.android.gms.auth.api.signin.GoogleSignIn;
import DataSource = com.google.android.gms.fitness.data.DataSource;
import DataSet = com.google.android.gms.fitness.data.DataSet;
import DataPoint = com.google.android.gms.fitness.data.DataPoint;
import ResultCallback = com.google.android.gms.common.api.ResultCallback;
import DataTypeResult = com.google.android.gms.fitness.result.DataTypeResult;
import HistoryClient = com.google.android.gms.fitness.HistoryClient;
import Task = com.google.android.gms.tasks.Task;
import DataReadResponse = com.google.android.gms.fitness.result.DataReadResponse;
import OnSuccessListener = com.google.android.gms.tasks.OnSuccessListener;
import OnFailureListener = com.google.android.gms.tasks.OnFailureListener;
import OnCompleteListener = com.google.android.gms.tasks.OnCompleteListener;
import Field = com.google.android.gms.fitness.data.Field;

import JList = java.util.List;

@Injectable()
export class GoogleFitService implements DataService {
    private static readonly GOOGLE_FIT_PERMISSIONS_REQUEST_CODE: number = 1;

    public readonly id: string = GoogleFitService.name;
    public readonly syncActionsObserver: Subject<TypedSyncAction>;
    private readonly androidContext: any = applicationModule.android.startActivity;

    constructor(
        private readonly logger: NGXLogger
    ) {
        // SHA-1 fingerprint -> 9F:7D:F4:0C:EF:79:C5:83:27:B1:AC:5F:76:7A:9D:6F:18:F5:52:C8
        // Package name -> com.guylescalier.MyHealthHub

        setTimeout(
            () => {
                this.logger.debug("GoogleFitService created !");

                try {
                    // Create a FitnessOptions instance, declaring the Fit API data types and access required by your app
                    const fitnessOptions: FitnessOptions = FitnessOptions.builder()
                        .addDataType(DataType.TYPE_WEIGHT, FitnessOptions.ACCESS_READ)
                        .build();

                    // Check if the user has previously granted the necessary data access, and if not, initiate the authorization flow
                    if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this.androidContext), fitnessOptions)) {
                        GoogleSignIn.requestPermissions(
                            this.androidContext,
                            GoogleFitService.GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                            GoogleSignIn.getLastSignedInAccount(this.androidContext),
                            fitnessOptions);
                    } else {
                        this.accessGoogleFit();
                    }
                } catch (error) {
                    this.logger.error("GoogleFitService error :", error);
                }
            }, 2000);
    }

    public onActivityResult(requestCode: number, resultCode: number, intent: android.content.Intent): void {
        this.logger.debug(`onActivityResult(${requestCode}, ${resultCode})`);
        if (resultCode === android.app.Activity.RESULT_OK) {
            if (requestCode === GoogleFitService.GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
                this.accessGoogleFit();
            }
        }
    }

    public accessGoogleFit(): void {
        this.logger.debug("accessGoogleFit");

        const startTime: number = moment.utc([2018, 11, 19]).valueOf();
        const endTime: number = moment.utc([2018, 11, 21]).valueOf();

        this.logger.debug(startTime);
        this.logger.debug(endTime);

        const readRequest: DataReadRequest = new DataReadRequest.Builder()
            .read(DataType.TYPE_WEIGHT)
            .read(DataType.TYPE_BODY_FAT_PERCENTAGE)
            .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
            .build();

        Fitness
            .getHistoryClient(
                this.androidContext,
                GoogleSignIn.getLastSignedInAccount(this.androidContext))
            .readData(readRequest)
            .addOnSuccessListener(new OnSuccessListener<DataReadResponse>({
                onSuccess: (dataReadResponse: DataReadResponse) => {
                    this.logger.debug("Success");
                    const dataSets: JList<DataSet> = dataReadResponse.getResult().getDataSets();
                    this.logger.debug("dataSets :", dataSets.toArray().length);
                    _.forEach(dataSets.toArray(), (dataSet: DataSet) => {
                        this.logger.debug("Data returned for Data type : ", dataSet.getDataType().getName());
                        const dataPoints: JList<DataPoint> = dataSet.getDataPoints();
                        this.logger.debug("dataPoints :", dataPoints.toArray().length);
                        _.forEach(dataPoints.toArray(), (dataPoint: DataPoint) => {
                            this.logger.debug("Data : ", dataPoint);
                            this.logger.debug("Type : " + dataPoint.getDataType().getName());
                            this.logger.debug("Start : " + moment(dataPoint.getStartTime(TimeUnit.MILLISECONDS)).toDate());
                            this.logger.debug("End : " + moment(dataPoint.getEndTime(TimeUnit.MILLISECONDS)).toDate());
                            const fields: JList<Field> = dataPoint.getDataType().getFields();
                            _.forEach(fields.toArray(), (field: Field) => {
                                this.logger.debug(field.getName() + " : " + dataPoint.getValue(field));
                            });
                        });
                    });
                }
            }))
            .addOnFailureListener(new OnFailureListener({
                onFailure(e: java.lang.Exception): void {
                    this.logger.error("Failure :", e);
                }
            }));
    }
}
