// export { }

// declare global {
//     interface Map<K, V> {
//         toArray(): Array<[K, V]>;

//         toMap<VNew>(
//             valueSelector: (value: K) => VNew
//         ): Map<K, VNew>;
//     }

//     interface Array<T> {
//         toMap<K, V>(
//             keySelector: (value: T) => K,
//             valueSelector: (value: T) => V
//         ): Map<K, V>;
//     }
// }

// Map.prototype.toArray = function (): Array<[any, any]> {
//     const thisMap: Map<any, any> = this;
//     return Array.from(thisMap, ([key, value]) => [key, value] as [any, any]);
// };

// Map.prototype.toMap = function <VNew>(valueSelector: (value: any) => VNew): Map<any, VNew> {
//     const thisMap: Map<any, any> = this;
//     return Array.from(thisMap.keys()).toMap(x => x, x => valueSelector(x));
// }

// Array.prototype.toMap = function <K, V>(keySelector: (value: any) => K, valueSelector: (value: any) => V): Map<K, V> {
//     const thisArray: Array<any> = this;
//     return new Map(thisArray.map(x => [keySelector(x), valueSelector(x)] as [K, V]));
// }

export function setToArray<T>(set: Set<T>): Array<T> {
    return Array.from(set.values());
}

export function mapToArray<K, V>(map: Map<K, V>): Array<[K, V]> {
    return Array.from(map, ([key, value]) => [key, value] as [K, V]);
}

export function mapOfSetToArray<K, V>(map: Map<K, Set<V>>): Array<[K, Array<V>]> {
    return Array.from(map, ([key, value]) => [key, setToArray(value)] as [K, Array<V>]);
}

export function mapToMap<K, V, VNew>(
    map: Map<K, V>,
    valueSelector: (value: K) => VNew
): Map<K, VNew> {
    return arrayToMap(Array.from(map.keys()), x => x, x => valueSelector(x));
}

export function arrayToMap<T, K, V>(
    array: Array<T>,
    keySelector: (value: T) => K,
    valueSelector: (value: T) => V
): Map<K, V> {
    return new Map(array.map(x => [keySelector(x), valueSelector(x)] as [K, V]));
}
