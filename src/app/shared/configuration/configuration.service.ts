import { Injectable, isDevMode } from "@angular/core";
import { NGXLogger } from "ngx-logger";
import { Observable, ReplaySubject } from "rxjs";
import { deepFreeze, ReplicationBuilder } from "typescript-immutable-helper";

import { Dispatcher } from "../flux";
import { DataType } from "../synchronization";
import { Configuration } from "./configuration";

@Injectable()
export class ConfigurationService {
  private readonly configurationSubject: ReplaySubject<Configuration> = new ReplaySubject<Configuration>(1);

  public get configuration(): Observable<Configuration> {
    return this.configurationSubject;
  }

  private state: Configuration;

  constructor(
    private logger: NGXLogger,
    dispatcher: Dispatcher
  ) {
    const state: Configuration = {
      dataReaders: new Map([
        // [DataType.StepCount, "SamsungHealth"]
      ])
    };
    this.state = isDevMode() ? deepFreeze(state) : state;

    this.configurationSubject.next(this.state);

    dispatcher.configActions.subscribe(action => this.setSource(action.dataType, action.providerId));
  }

  private setSource(dataType: DataType, providerId: string): void {
    this.logger.debug(`setSource(${dataType}, ${providerId})`);

    this.state = ReplicationBuilder
      .forObject(this.state)
      .replaceValueOf("dataReaders")
      .withCloneAndDo((x: Map<DataType, string>) => x.set(dataType, providerId))
      .build();

    this.configurationSubject.next(this.state);
  }
}
