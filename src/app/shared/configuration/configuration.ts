import { DataType } from "../synchronization";

export interface Configuration {
    dataReaders: Map<DataType, string>;
}
