import { Observable, Subject } from "rxjs";

import { SyncAction } from "../flux";
import { DataService } from "../synchronization";

export interface StepCountData {
    date: Date;
    stepCount: number;
}

export interface StepCountReader extends DataService {
    readStepCounts(action: SyncAction): Observable<StepCountData>;
}

export interface StepCountWriter extends DataService {
    stepCountsObserver: Subject<StepCountData>;
}

export function isStepCountReader(object: DataService): object is StepCountReader {
    return (object as StepCountReader).readStepCounts !== undefined;
}

export function asStepCountReader(object: DataService): StepCountReader {
    if (!isStepCountReader(object)) {
        throw new Error("Not a StepCountReader");
    }

    return object;
}

export function isStepCountWriter(object: DataService): object is StepCountWriter {
    return (object as StepCountWriter).stepCountsObserver !== undefined;
}

// const dataTypeSymbols: Map<string, symbol> = new Map();
// for (let dataType in DataType) {
//     const dataTypeSymbol: unique symbol = Symbol();
//     dataTypeSymbols.set(dataType, dataTypeSymbol);
// }

// const dataTypeSymbols = Object.keys(DataType).map(x => {
//     const symbol = Symbol();
//     return {
//         dataType: x,
//         symbol: symbol as typeof symbol
//     }
// });

// const test = dataTypeSymbols[0];
// const test2: ReturnType<typeof test>["symbol"] = test.symbol

// class DataTypeSymbols {
//     static readonly StepCount: unique symbol = Symbol();
// }

// function readerInterface<T extends DataType>(x: new () => T) {
//     // const dataType: DataType = DataType[x.name];
//     // const symbol: unique symbol = dataTypeSymbols.get(x.name) as unique symbol;
//     // const dataTypeSymbol = DataTypeSymbols[x.name];
//     // const symbol = dataTypeSymbols.find(x => x.dataType == dataType.name).symbol;
//     switch (x.name) {
//         case DataType.StepCount:
//             return interface {
//                 // [T]: Observable<T>;
//                 // [key: string]: Observable<T>;
//                 stepCounts: Observable<DataType.StepCount>;
//             }
//         default:
//             break;
//     }
// }

// class Test implements readerInterface<DataType.StepCount>()

// export function DataReader(dataType: DataType) {
//     switch (dataType) {
//         case DataType.StepCount:
//             return <T extends { new(...args: any[]): {} }>(target: T) => class extends target {
//                 [DataType.StepCount]: Observable<StepCountData>;
//             }
//         default:
//             throw new Error(`DataType ${dataType} not supported`);
//     }
// }

// function init<T extends { new(...args: any[]): {} }>(target: T) {
//     return class extends target {
//         toto: number;
//     }
// }

// @DataReader(DataType.StepCount)
// @init
// class Test {

//     coucou() {
//         this[DataType.StepCount]
//     }
// }
