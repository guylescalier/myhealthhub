import { Injectable } from "@angular/core";
import * as _ from "lodash";
import * as moment from "moment";
import { NGXLogger } from "ngx-logger";
import { EMPTY, Observable, Subject } from "rxjs";
import { catchError, switchMap, tap } from "rxjs/operators";
import * as applicationModule from "tns-core-modules/application";

import { StepCountData, StepCountReader } from "../data";
import { SyncAction } from "../flux";
import { DataService, TypedSyncAction } from "../synchronization";

// import healthdata = com.samsung.android.sdk.healthdata;
import HealthConnectionErrorResult = com.samsung.android.sdk.healthdata.HealthConnectionErrorResult;
import HealthConstants = com.samsung.android.sdk.healthdata.HealthConstants;
import HealthData = com.samsung.android.sdk.healthdata.HealthData;
import HealthDataUtil = com.samsung.android.sdk.healthdata.HealthDataUtil;
import HealthDataResolver = com.samsung.android.sdk.healthdata.HealthDataResolver;
import HealthDataService = com.samsung.android.sdk.healthdata.HealthDataService;
import HealthDataStore = com.samsung.android.sdk.healthdata.HealthDataStore;
import HealthPermissionManager = com.samsung.android.sdk.healthdata.HealthPermissionManager;

import Filter = com.samsung.android.sdk.healthdata.HealthDataResolver.Filter;
import ReadRequest = com.samsung.android.sdk.healthdata.HealthDataResolver.ReadRequest;
import ReadResult = com.samsung.android.sdk.healthdata.HealthDataResolver.ReadResult;

import ConnectionListener = com.samsung.android.sdk.healthdata.HealthDataStore.ConnectionListener;

import PermissionKey = com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionKey;
import PermissionResult = com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionResult;
import PermissionType = com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionType;

import ResultListener = com.samsung.android.sdk.healthdata.HealthResultHolder.ResultListener;

import JBoolean = java.lang.Boolean;
import JInteger = java.lang.Integer;
import JLong = java.lang.Long;
import JObject = java.lang.Object;

import JHashSet = java.util.HashSet;
import JIterator = java.util.Iterator;
import JList = java.util.List;
import JMap = java.util.Map;

@Injectable()
export class SamsungHealthService implements DataService, StepCountReader {

    public readonly id: string = SamsungHealthService.name;
    public readonly syncActionsObserver: Subject<TypedSyncAction>;
    public readonly stepCountsObservable: Observable<StepCountData>;
    // private disposeSignal: Subject<void>;

    private readonly androidContext: any = applicationModule.android.startActivity;

    private readonly keySet: JHashSet<PermissionKey>;

    // private readonly dataStore: Observable<HealthDataStore>;

    constructor(
        private readonly logger: NGXLogger
    ) {
        const healthDataService: HealthDataService = new HealthDataService();
        healthDataService.initialize(this.androidContext);

        this.keySet = new JHashSet<PermissionKey>();
        this.keySet.add(new PermissionKey(HealthConstants.StepCount.HEALTH_DATA_TYPE, PermissionType.READ));
        this.keySet.add(new PermissionKey("com.samsung.shealth.step_daily_trend", PermissionType.READ));
    }

    public readStepCounts(action: SyncAction): Observable<StepCountData> {
        return this.getDataStore()
            .pipe(
                switchMap(dataStore => this.read(
                    dataStore,
                    result => this.getStepCountData(result),
                    action.date,
                    action.fromDate)),
                catchError(error => {
                    this.logger.error(error);
                    return EMPTY;
                })
            );
    }

    private read<T>(
        dataStore: HealthDataStore,
        getData: (result: ReadResult) => Observable<T>,
        date?: Date,
        fromDate?: boolean
    ): Observable<T> {
        return new Observable<ReadResult>(observer => {
            try {
                const resolver: HealthDataResolver = new HealthDataResolver(dataStore, null);
                const request: ReadRequest = this.buildStepCountRequest(date, fromDate);

                resolver
                    .read(request)
                    .setResultListener(new ResultListener<ReadResult>({
                        onResult: result => {
                            switch (result.mStatus) {
                                case ReadResult.STATUS_SUCCESSFUL:
                                    observer.next(result);
                                    observer.complete();
                                    break;
                                case ReadResult.STATUS_CANCELED:
                                    observer.error("Request error - CANCELED");
                                    break;
                                case ReadResult.STATUS_FAILED:
                                    observer.error("Request error - FAILED");
                                    break;
                                case ReadResult.STATUS_INVALID_INPUT_DATA:
                                    observer.error("Request error - INVALID_INPUT_DATA");
                                    break;
                                case ReadResult.STATUS_OUT_OF_SPACE:
                                    observer.error("Request error - OUT_OF_SPACE");
                                    break;
                                case ReadResult.STATUS_UNKNOWN:
                                    observer.error("Request error - UNKNOWN");
                                    break;
                            }
                        }
                    }));
            } catch (error) {
                throw new Error("Reading health data fails : " + error);
            }
        })
            .pipe(
                switchMap(x => getData(x)),
                tap(() => dataStore.disconnectService())
            );
        // .pipe(
        //     retryWhen(errors => errors.pipe(delayWhen(() => timer(2000))))
        // );
    }

    private getStepCountData(result: ReadResult): Observable<StepCountData> {
        return new Observable<StepCountData>(observer => {
            const iterator: JIterator<HealthData> = result.iterator();

            while (iterator.hasNext()) {
                const data: HealthData = iterator.next();
                const date: Date = moment(data.getLong("day_time")).toDate();
                const binningData: JList<any> = HealthDataUtil.getStructuredDataList(
                    data.getBlob("binning_data"),
                    JObject.class
                );
                const stepCount: number = _.sumBy(binningData.toArray(), x => x.get("count").longValue());

                observer.next({
                    date,
                    stepCount
                });
            }
        });
    }

    private buildStepCountRequest(date?: Date, fromDate: boolean = false): ReadRequest {
        let requestBuilder: ReadRequest.Builder = new ReadRequest.Builder()
            .setDataType("com.samsung.shealth.step_daily_trend");

        const sourceTypeFilter: Filter = Filter.eq("source_type", new JInteger(-2));

        if (date) {
            const unixDate: number = moment.utc(date).startOf("day").valueOf();

            const dayTimeFilter: Filter = fromDate ?
                Filter.greaterThanEquals("day_time", new JLong(unixDate)) :
                Filter.eq("day_time", new JLong(unixDate));

            requestBuilder = requestBuilder.setFilter(
                Filter.and(dayTimeFilter, [sourceTypeFilter])
            );
        } else {
            requestBuilder = requestBuilder.setFilter(sourceTypeFilter);
        }

        return requestBuilder.build();
    }

    private getDataStore(): Observable<HealthDataStore> {
        return new Observable<HealthDataStore>(observer => {
            const dataStore: HealthDataStore = new HealthDataStore(
                this.androidContext,
                new ConnectionListener({
                    onConnected: () => {
                        observer.next(dataStore);
                        // observer.complete();
                    },
                    onConnectionFailed: (error: HealthConnectionErrorResult) => {
                        let message: string = "Connection with Samsung Health is not available";

                        if (error.hasResolution()) {
                            switch (error.getErrorCode()) {
                                case HealthConnectionErrorResult.PLATFORM_NOT_INSTALLED:
                                    message = "Please install Samsung Health";
                                    break;
                                case HealthConnectionErrorResult.OLD_VERSION_PLATFORM:
                                    message = "Please upgrade Samsung Health";
                                    break;
                                case HealthConnectionErrorResult.PLATFORM_DISABLED:
                                    message = "Please enable Samsung Health";
                                    break;
                                case HealthConnectionErrorResult.USER_AGREEMENT_NEEDED:
                                    message = "Please agree with Samsung Health policy";
                                    break;
                                default:
                                    message = "Please make Samsung Health available";
                                    break;
                            }
                        }

                        observer.error(message);
                    },
                    onDisconnected: () => {
                        observer.error("Disconnected from Samsung Health");
                    }
                }));
            dataStore.connectService();
        })
            .pipe(
                switchMap(x => this.ensurePermissions(x, this.keySet))
            );
    }

    private ensurePermissions(
        dataStore: HealthDataStore,
        keySet: JHashSet<PermissionKey>
    ): Observable<HealthDataStore> {
        return new Observable<HealthDataStore>(observer => {
            try {
                const permissionManager: HealthPermissionManager = new HealthPermissionManager(dataStore);
                const resultMap: JMap<PermissionKey, JBoolean> = permissionManager.isPermissionAcquired(keySet);

                if (resultMap.containsValue(JBoolean.FALSE)) {
                    // Request the permission for reading step counts if it is not acquired
                    permissionManager
                        .requestPermissions(keySet, this.androidContext)
                        .setResultListener(new ResultListener<PermissionResult>({
                            onResult: result => {
                                if (result.getResultMap().containsValue(JBoolean.FALSE)) {
                                    dataStore.disconnectService();
                                    observer.error("Requesting permission fails");
                                } else {
                                    observer.next(dataStore);
                                }
                            }
                        }));
                } else {
                    observer.next(dataStore);
                }
            } catch (error) {
                dataStore.disconnectService();
                observer.error("Permission setting fails : " + error);
            }
        });
    }
    // public getResultListener(): com.samsung.android.sdk.healthdata.HealthResultHolder.ResultListener<com.samsung.android.sdk.healthdata.HealthDataResolver.ReadResult> {
    //     return new com.samsung.android.sdk.healthdata.HealthResultHolder.ResultListener<com.samsung.android.sdk.healthdata.HealthDataResolver.ReadResult>({
    //         onResult: result => {

    //             try {
    //                 console.log("Count : " + result.mCount);
    //                 const iterator: java.util.Iterator<com.samsung.android.sdk.healthdata.HealthData> = result.iterator();

    //                 // new Observable<com.samsung.android.sdk.healthdata.HealthData>(x => {
    //                 //     while (iterator.hasNext()) {
    //                 //         x.next(iterator.next());
    //                 //     }
    //                 // });

    //                 while (iterator.hasNext()) {
    //                     const data: com.samsung.android.sdk.healthdata.HealthData = iterator.next();
    //                     // const stepCount = data.getInt(com.samsung.android.sdk.healthdata.HealthConstants.StepCount.COUNT);
    //                     // console.log(data.getContentValues());
    //                     // console.log("day_time : " + data.getLong("day_time") + " | " + moment(data.getLong("day_time")).format())
    //                     const binningData: java.util.List<any> = com.samsung.android.sdk.healthdata.HealthDataUtil.getStructuredDataList(
    //                         data.getBlob("binning_data"),
    //                         java.lang.Object.class
    //                     );

    //                     // console.log(binningData);
    //                     // console.log(binningData.size());

    //                     // _.forEach(binningData.toArray(), x => console.log(x.get("count").longValue()));
    //                     console.log(_.sumBy(binningData.toArray(), x => x.get("count").longValue()));
    //                     // console.log(_.sumBy(binningData.toArray(), x => 0));
    //                     // for (let index = 0; index < binningData.size(); index++) {
    //                     //     console.log(binningData.get(index));
    //                     // }
    //                 }
    //             } finally {
    //                 result.close();
    //             }
    //         }
    //     });
    // }
}

// tslint:disable-next-line:max-classes-per-file
// class BinningStepCountData extends java.lang.Object {
//     public static class: java.lang.Class<BinningStepCountData>;
// }
