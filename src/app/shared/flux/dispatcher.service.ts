import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

import { DataType } from "../synchronization";

export interface ConfigAction {
    dataType: DataType;
    providerId: string;
}

export interface SyncAction {
    date?: Date;
    fromDate?: boolean;
}

@Injectable()
export class Dispatcher {
    private readonly configActionsSubject: Subject<ConfigAction> = new Subject<ConfigAction>();

    public get configActions(): Observable<ConfigAction> {
        return this.configActionsSubject;
    }

    private readonly syncActionsSubject: Subject<SyncAction> = new Subject<SyncAction>();

    public get syncActions(): Observable<SyncAction> {
        return this.syncActionsSubject;
    }

    public setDataSource(dataType: DataType, providerId: string): void {
        this.configActionsSubject.next({
            dataType,
            providerId
        });
    }

    public sync(date: Date): void {
        this.syncActionsSubject.next({
            date
        });
    }

    // syncFrom(date: Date): void {
    //     this.subject.next({
    //         date,
    //         fromDate: true
    //     });
    // }

    // syncAll(): void {
    //     this.subject.next({});
    // }
}
