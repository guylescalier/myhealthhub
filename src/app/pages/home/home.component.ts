// tslint:disable:no-submodule-imports
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";
import { NGXLogger, NGXLogInterface } from "ngx-logger";
import * as applicationModule from "tns-core-modules/application";
import { setTimeout } from "tns-core-modules/timer";
import { ConfigurationService } from "~/app/shared/configuration";
import { mapOfSetToArray, mapToArray } from "~/app/shared/extensions";
import { Dispatcher } from "~/app/shared/flux";
import { SamsungHealthService } from "~/app/shared/samsung-health";
import { DataType, SynchronizationService } from "~/app/shared/synchronization";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit, OnDestroy {
    public logs: string = "";
    private androidContext: any = applicationModule.android.startActivity;

    constructor(
        private cd: ChangeDetectorRef,
        private logger: NGXLogger,
        private dispatcher: Dispatcher,
        private configurationService: ConfigurationService,
        private synchronizationService: SynchronizationService
    ) {
        // Use the component constructor to inject providers.
    }

    public ngOnInit(): void {
        this.androidContext.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        this.logger.registerMonitor({
            onLog: (log: NGXLogInterface): void => {
                // console.log(log);
                this.cd.detectChanges();
                // this.logs += `${formatDate(new Date(), "HH:mm:ss.SSS", "en-US")} - ${log.message}\n`;
                this.logs += `# ${log.message}\n`;
                log.additional.forEach(x => {
                    let tmp: string;
                    if (typeof x === "string") {
                        tmp = x;
                    } else if (typeof x.toString === "function") {
                        tmp = x.toString();
                    } else {
                        tmp = JSON.stringify(x);
                    }
                    this.logs += ` - ${tmp}\n`;
                });
            }
        });

        setTimeout(
            () => {
                this.configurationService.configuration.subscribe(
                    configuration => this.logger.debug("Configuration changed :", mapToArray(configuration.dataReaders)),
                    error => this.logger.error(error)
                );

                this.dispatcher.setDataSource(DataType.StepCount, SamsungHealthService.name);

                this.logger.debug("Data readers :", mapOfSetToArray(this.synchronizationService.dataReaders));
                this.logger.debug("Data writers :", mapOfSetToArray(this.synchronizationService.dataWriters));

                this.dispatcher.sync(new Date());
            },
            1000);
    }

    public ngOnDestroy(): void {
        //
    }
}
