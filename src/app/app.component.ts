import { Component } from "@angular/core";

import { ConfigurationService } from "./shared/configuration";
import { Dispatcher } from "./shared/flux";
import { GoogleFitService } from "./shared/google-fit";
import { SamsungHealthService } from "./shared/samsung-health";
import {
    DATA_SERVICES_TOKEN,
    SynchronizationService,
} from "./shared/synchronization";

@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "app.component.html",
    providers: [
        Dispatcher,
        ConfigurationService,
        SynchronizationService,
        { provide: DATA_SERVICES_TOKEN, useClass: SamsungHealthService, multi: true },
        { provide: DATA_SERVICES_TOKEN, useClass: GoogleFitService, multi: true }
    ]
})
export class AppComponent {
    constructor() {
        //
    }
}
