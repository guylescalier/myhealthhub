declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export class Auth extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.auth.api.Auth>;
							public static PROXY_API: com.google.android.gms.common.api.Api<com.google.android.gms.auth.api.AuthProxyOptions>;
							public static CREDENTIALS_API: com.google.android.gms.common.api.Api<com.google.android.gms.auth.api.Auth.AuthCredentialsOptions>;
							public static GOOGLE_SIGN_IN_API: com.google.android.gms.common.api.Api<com.google.android.gms.auth.api.signin.GoogleSignInOptions>;
							public static ProxyApi: com.google.android.gms.auth.api.proxy.ProxyApi;
							public static CredentialsApi: com.google.android.gms.auth.api.credentials.CredentialsApi;
							public static GoogleSignInApi: com.google.android.gms.auth.api.signin.GoogleSignInApi;
						}
						export module Auth {
							export class AuthCredentialsOptions extends java.lang.Object implements com.google.android.gms.common.api.Api.ApiOptions.Optional {
								public static class: java.lang.Class<com.google.android.gms.auth.api.Auth.AuthCredentialsOptions>;
								public toBundle(): globalAndroid.os.Bundle;
								public constructor(param0: com.google.android.gms.auth.api.Auth.AuthCredentialsOptions.Builder);
							}
							export module AuthCredentialsOptions {
								export class Builder extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.Auth.AuthCredentialsOptions.Builder>;
									public constructor();
									public forceEnableSaveDialog(): com.google.android.gms.auth.api.Auth.AuthCredentialsOptions.Builder;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class Credential extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.Credential>;
								public static EXTRA_KEY: string;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.Credential>;
								public getProfilePictureUri(): globalAndroid.net.Uri;
								public describeContents(): number;
								public getAccountType(): string;
								public getName(): string;
								public getGivenName(): string;
								public getPassword(): string;
								public equals(param0: any): boolean;
								public getFamilyName(): string;
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public getId(): string;
								public hashCode(): number;
								public getIdTokens(): java.util.List<com.google.android.gms.auth.api.credentials.IdToken>;
							}
							export module Credential {
								export class Builder extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.Credential.Builder>;
									public constructor(param0: string);
									public setProfilePictureUri(param0: globalAndroid.net.Uri): com.google.android.gms.auth.api.credentials.Credential.Builder;
									public constructor(param0: com.google.android.gms.auth.api.credentials.Credential);
									public build(): com.google.android.gms.auth.api.credentials.Credential;
									public setName(param0: string): com.google.android.gms.auth.api.credentials.Credential.Builder;
									public setPassword(param0: string): com.google.android.gms.auth.api.credentials.Credential.Builder;
									public setAccountType(param0: string): com.google.android.gms.auth.api.credentials.Credential.Builder;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class CredentialPickerConfig extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialPickerConfig>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.CredentialPickerConfig>;
								public describeContents(): number;
								public shouldShowAddAccountButton(): boolean;
								public shouldShowCancelButton(): boolean;
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public isForNewAccount(): boolean;
							}
							export module CredentialPickerConfig {
								export class Builder extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialPickerConfig.Builder>;
									public constructor();
									public setShowAddAccountButton(param0: boolean): com.google.android.gms.auth.api.credentials.CredentialPickerConfig.Builder;
									public setPrompt(param0: number): com.google.android.gms.auth.api.credentials.CredentialPickerConfig.Builder;
									public build(): com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
									public setShowCancelButton(param0: boolean): com.google.android.gms.auth.api.credentials.CredentialPickerConfig.Builder;
									public setForNewAccount(param0: boolean): com.google.android.gms.auth.api.credentials.CredentialPickerConfig.Builder;
								}
								export class Prompt extends java.lang.Object implements java.lang.annotation.Annotation {
									public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialPickerConfig.Prompt>;
									/**
									 * Constructs a new instance of the com.google.android.gms.auth.api.credentials.CredentialPickerConfig$Prompt interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
									 */
									public constructor(implementation: {
										equals(param0: any): boolean;
										hashCode(): number;
										toString(): string;
										annotationType(): java.lang.Class<any>;
									});
									public constructor();
									public static SIGN_UP: number;
									public static CONTINUE: number;
									public static SIGN_IN: number;
									public hashCode(): number;
									public equals(param0: any): boolean;
									public annotationType(): java.lang.Class<any>;
									public toString(): string;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class CredentialRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialRequest>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.CredentialRequest>;
								public getServerClientId(): string;
								public getSupportsPasswordLogin(): boolean;
								public isPasswordLoginSupported(): boolean;
								public getIdTokenNonce(): string;
								public getCredentialPickerConfig(): com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
								public getCredentialHintPickerConfig(): com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
								public isIdTokenRequested(): boolean;
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public getAccountTypesSet(): java.util.Set<string>;
								public getAccountTypes(): native.Array<string>;
							}
							export module CredentialRequest {
								export class Builder extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialRequest.Builder>;
									public setIdTokenRequested(param0: boolean): com.google.android.gms.auth.api.credentials.CredentialRequest.Builder;
									public setIdTokenNonce(param0: string): com.google.android.gms.auth.api.credentials.CredentialRequest.Builder;
									public constructor();
									public setCredentialHintPickerConfig(param0: com.google.android.gms.auth.api.credentials.CredentialPickerConfig): com.google.android.gms.auth.api.credentials.CredentialRequest.Builder;
									public setSupportsPasswordLogin(param0: boolean): com.google.android.gms.auth.api.credentials.CredentialRequest.Builder;
									public setAccountTypes(param0: native.Array<string>): com.google.android.gms.auth.api.credentials.CredentialRequest.Builder;
									public setPasswordLoginSupported(param0: boolean): com.google.android.gms.auth.api.credentials.CredentialRequest.Builder;
									public build(): com.google.android.gms.auth.api.credentials.CredentialRequest;
									public setServerClientId(param0: string): com.google.android.gms.auth.api.credentials.CredentialRequest.Builder;
									public setCredentialPickerConfig(param0: com.google.android.gms.auth.api.credentials.CredentialPickerConfig): com.google.android.gms.auth.api.credentials.CredentialRequest.Builder;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class CredentialRequestResponse extends com.google.android.gms.common.api.Response<com.google.android.gms.auth.api.credentials.CredentialRequestResult> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialRequestResponse>;
								public constructor(param0: any);
								public getCredential(): com.google.android.gms.auth.api.credentials.Credential;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class CredentialRequestResult extends java.lang.Object implements com.google.android.gms.common.api.Result {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialRequestResult>;
								/**
								 * Constructs a new instance of the com.google.android.gms.auth.api.credentials.CredentialRequestResult interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
								 */
								public constructor(implementation: {
									getCredential(): com.google.android.gms.auth.api.credentials.Credential;
									getStatus(): com.google.android.gms.common.api.Status;
								});
								public constructor();
								public getCredential(): com.google.android.gms.auth.api.credentials.Credential;
								public getStatus(): com.google.android.gms.common.api.Status;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class Credentials extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.Credentials>;
								public static getClient(param0: globalAndroid.app.Activity): com.google.android.gms.auth.api.credentials.CredentialsClient;
								public static getClient(param0: globalAndroid.app.Activity, param1: com.google.android.gms.auth.api.credentials.CredentialsOptions): com.google.android.gms.auth.api.credentials.CredentialsClient;
								public static getClient(param0: globalAndroid.content.Context): com.google.android.gms.auth.api.credentials.CredentialsClient;
								public static getClient(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.credentials.CredentialsOptions): com.google.android.gms.auth.api.credentials.CredentialsClient;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class CredentialsApi extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialsApi>;
								/**
								 * Constructs a new instance of the com.google.android.gms.auth.api.credentials.CredentialsApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
								 */
								public constructor(implementation: {
									request(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.CredentialRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.api.credentials.CredentialRequestResult>;
									getHintPickerIntent(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.HintRequest): globalAndroid.app.PendingIntent;
									save(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.Credential): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
									delete(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.Credential): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
									disableAutoSignIn(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
								});
								public constructor();
								public static ACTIVITY_RESULT_ADD_ACCOUNT: number;
								public static ACTIVITY_RESULT_OTHER_ACCOUNT: number;
								public static ACTIVITY_RESULT_NO_HINTS_AVAILABLE: number;
								public static CREDENTIAL_PICKER_REQUEST_CODE: number;
								public getHintPickerIntent(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.HintRequest): globalAndroid.app.PendingIntent;
								public request(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.CredentialRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.api.credentials.CredentialRequestResult>;
								public save(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.Credential): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
								public disableAutoSignIn(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
								public delete(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.Credential): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class CredentialsClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.auth.api.Auth.AuthCredentialsOptions> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialsClient>;
								public disableAutoSignIn(): com.google.android.gms.tasks.Task<java.lang.Void>;
								public request(param0: com.google.android.gms.auth.api.credentials.CredentialRequest): com.google.android.gms.tasks.Task<com.google.android.gms.auth.api.credentials.CredentialRequestResponse>;
								public save(param0: com.google.android.gms.auth.api.credentials.Credential): com.google.android.gms.tasks.Task<java.lang.Void>;
								public delete(param0: com.google.android.gms.auth.api.credentials.Credential): com.google.android.gms.tasks.Task<java.lang.Void>;
								public getHintPickerIntent(param0: com.google.android.gms.auth.api.credentials.HintRequest): globalAndroid.app.PendingIntent;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class CredentialsOptions extends com.google.android.gms.auth.api.Auth.AuthCredentialsOptions {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialsOptions>;
								public static DEFAULT: com.google.android.gms.auth.api.credentials.CredentialsOptions;
							}
							export module CredentialsOptions {
								export class Builder extends com.google.android.gms.auth.api.Auth.AuthCredentialsOptions.Builder {
									public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.CredentialsOptions.Builder>;
									public constructor();
									public forceEnableSaveDialog(): com.google.android.gms.auth.api.Auth.AuthCredentialsOptions.Builder;
									public forceEnableSaveDialog(): com.google.android.gms.auth.api.credentials.CredentialsOptions.Builder;
									public build(): com.google.android.gms.auth.api.credentials.CredentialsOptions;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class HintRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.HintRequest>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.HintRequest>;
								public getServerClientId(): string;
								public describeContents(): number;
								public getHintPickerConfig(): com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
								public getIdTokenNonce(): string;
								public isIdTokenRequested(): boolean;
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public getAccountTypes(): native.Array<string>;
								public isEmailAddressIdentifierSupported(): boolean;
							}
							export module HintRequest {
								export class Builder extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.HintRequest.Builder>;
									public constructor();
									public setPhoneNumberIdentifierSupported(param0: boolean): com.google.android.gms.auth.api.credentials.HintRequest.Builder;
									public setHintPickerConfig(param0: com.google.android.gms.auth.api.credentials.CredentialPickerConfig): com.google.android.gms.auth.api.credentials.HintRequest.Builder;
									public setIdTokenRequested(param0: boolean): com.google.android.gms.auth.api.credentials.HintRequest.Builder;
									public setIdTokenNonce(param0: string): com.google.android.gms.auth.api.credentials.HintRequest.Builder;
									public setServerClientId(param0: string): com.google.android.gms.auth.api.credentials.HintRequest.Builder;
									public setEmailAddressIdentifierSupported(param0: boolean): com.google.android.gms.auth.api.credentials.HintRequest.Builder;
									public setAccountTypes(param0: native.Array<string>): com.google.android.gms.auth.api.credentials.HintRequest.Builder;
									public build(): com.google.android.gms.auth.api.credentials.HintRequest;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class IdToken extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.IdToken>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.IdToken>;
								public describeContents(): number;
								public getAccountType(): string;
								public constructor(param0: string, param1: string);
								public equals(param0: any): boolean;
								public getIdToken(): string;
								public constructor();
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class IdentityProviders extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.IdentityProviders>;
								public static FACEBOOK: string;
								public static GOOGLE: string;
								public static LINKEDIN: string;
								public static MICROSOFT: string;
								public static PAYPAL: string;
								public static TWITTER: string;
								public static YAHOO: string;
								public static getIdentityProviderForAccount(param0: globalAndroid.accounts.Account): string;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class zzc extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.Credential> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.zzc>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class zzd extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.zzd>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class zze extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.CredentialPickerConfig> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.zze>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class zzf extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.zzf>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class zzg extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.CredentialRequest> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.zzg>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class zzh extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.zzh>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class zzi extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.zzi>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class zzj extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.HintRequest> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.zzj>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module credentials {
							export class zzk extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.credentials.IdToken> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.credentials.zzk>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export class GoogleSignIn extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.signin.GoogleSignIn>;
								public static requestPermissions(param0: globalAndroid.app.Activity, param1: number, param2: com.google.android.gms.auth.api.signin.GoogleSignInAccount, param3: com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension): void;
								public static getClient(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.signin.GoogleSignInOptions): com.google.android.gms.auth.api.signin.GoogleSignInClient;
								public static hasPermissions(param0: com.google.android.gms.auth.api.signin.GoogleSignInAccount, param1: native.Array<com.google.android.gms.common.api.Scope>): boolean;
								public static getSignedInAccountFromIntent(param0: globalAndroid.content.Intent): com.google.android.gms.tasks.Task<com.google.android.gms.auth.api.signin.GoogleSignInAccount>;
								public static hasPermissions(param0: com.google.android.gms.auth.api.signin.GoogleSignInAccount, param1: com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension): boolean;
								public static getClient(param0: globalAndroid.app.Activity, param1: com.google.android.gms.auth.api.signin.GoogleSignInOptions): com.google.android.gms.auth.api.signin.GoogleSignInClient;
								public static getLastSignedInAccount(param0: globalAndroid.content.Context): com.google.android.gms.auth.api.signin.GoogleSignInAccount;
								public static getAccountForScopes(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Scope, param2: native.Array<com.google.android.gms.common.api.Scope>): com.google.android.gms.auth.api.signin.GoogleSignInAccount;
								public static requestPermissions(param0: globalAndroid.app.Activity, param1: number, param2: com.google.android.gms.auth.api.signin.GoogleSignInAccount, param3: native.Array<com.google.android.gms.common.api.Scope>): void;
								public static requestPermissions(param0: globalAndroid.support.v4.app.Fragment, param1: number, param2: com.google.android.gms.auth.api.signin.GoogleSignInAccount, param3: native.Array<com.google.android.gms.common.api.Scope>): void;
								public static requestPermissions(param0: globalAndroid.support.v4.app.Fragment, param1: number, param2: com.google.android.gms.auth.api.signin.GoogleSignInAccount, param3: com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension): void;
								public static getAccountForExtension(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension): com.google.android.gms.auth.api.signin.GoogleSignInAccount;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export class GoogleSignInApi extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.signin.GoogleSignInApi>;
								/**
								 * Constructs a new instance of the com.google.android.gms.auth.api.signin.GoogleSignInApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
								 */
								public constructor(implementation: {
									getSignInIntent(param0: com.google.android.gms.common.api.GoogleApiClient): globalAndroid.content.Intent;
									silentSignIn(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.OptionalPendingResult<com.google.android.gms.auth.api.signin.GoogleSignInResult>;
									signOut(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
									revokeAccess(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
									getSignInResultFromIntent(param0: globalAndroid.content.Intent): com.google.android.gms.auth.api.signin.GoogleSignInResult;
								});
								public constructor();
								public static EXTRA_SIGN_IN_ACCOUNT: string;
								public getSignInIntent(param0: com.google.android.gms.common.api.GoogleApiClient): globalAndroid.content.Intent;
								public silentSignIn(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.OptionalPendingResult<com.google.android.gms.auth.api.signin.GoogleSignInResult>;
								public getSignInResultFromIntent(param0: globalAndroid.content.Intent): com.google.android.gms.auth.api.signin.GoogleSignInResult;
								public signOut(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
								public revokeAccess(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export class GoogleSignInClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.auth.api.signin.GoogleSignInOptions> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.signin.GoogleSignInClient>;
								public revokeAccess(): com.google.android.gms.tasks.Task<java.lang.Void>;
								public silentSignIn(): com.google.android.gms.tasks.Task<com.google.android.gms.auth.api.signin.GoogleSignInAccount>;
								public signOut(): com.google.android.gms.tasks.Task<java.lang.Void>;
								public getSignInIntent(): globalAndroid.content.Intent;
							}
							export module GoogleSignInClient {
								export class zzc extends com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<com.google.android.gms.auth.api.signin.GoogleSignInResult,com.google.android.gms.auth.api.signin.GoogleSignInAccount> {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.GoogleSignInClient.zzc>;
									public convert(param0: any): any;
								}
								export class zzd extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.GoogleSignInClient.zzd>;
									public static values$50KLMJ33DTMIUPRFDTJMOP9FC5N68SJFD5I2UPRDECNM2TBKD0NM2S395TPMIPRED5N2UHRFDTJMOPAJD5JMSIBE8DM6IPBEEGI4IRBGDHIMQPBEEHGN8QBFDOTG____0(): native.Array<number>;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export class GoogleSignInResult extends java.lang.Object implements com.google.android.gms.common.api.Result {
								public static class: java.lang.Class<com.google.android.gms.auth.api.signin.GoogleSignInResult>;
								public constructor(param0: com.google.android.gms.auth.api.signin.GoogleSignInAccount, param1: com.google.android.gms.common.api.Status);
								public getStatus(): com.google.android.gms.common.api.Status;
								public getSignInAccount(): com.google.android.gms.auth.api.signin.GoogleSignInAccount;
								public isSuccess(): boolean;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export class GoogleSignInStatusCodes extends com.google.android.gms.common.api.CommonStatusCodes {
								public static class: java.lang.Class<com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes>;
								public static SIGN_IN_FAILED: number;
								public static SIGN_IN_CANCELLED: number;
								public static SIGN_IN_CURRENTLY_IN_PROGRESS: number;
								public static getStatusCodeString(param0: number): string;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export class RevocationBoundService extends globalAndroid.app.Service {
								public static class: java.lang.Class<com.google.android.gms.auth.api.signin.RevocationBoundService>;
								public onBind(param0: globalAndroid.content.Intent): globalAndroid.os.IBinder;
								public onTrimMemory(param0: number): void;
								public constructor();
								public constructor(param0: globalAndroid.content.Context);
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export class SignInAccount extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
								public static class: java.lang.Class<com.google.android.gms.auth.api.signin.SignInAccount>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.signin.SignInAccount>;
								public describeContents(): number;
								public getGoogleSignInAccount(): com.google.android.gms.auth.api.signin.GoogleSignInAccount;
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class SignInConfiguration extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.SignInConfiguration>;
									public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.signin.internal.SignInConfiguration>;
									public constructor();
									public hashCode(): number;
									public describeContents(): number;
									public constructor(param0: string, param1: com.google.android.gms.auth.api.signin.GoogleSignInOptions);
									public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
									public equals(param0: any): boolean;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class SignInHubActivity {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.SignInHubActivity>;
									public constructor();
									public onActivityResult(param0: number, param1: number, param2: globalAndroid.content.Intent): void;
									public onSaveInstanceState(param0: globalAndroid.os.Bundle): void;
									public onCreate(param0: globalAndroid.os.Bundle): void;
									public dispatchPopulateAccessibilityEvent(param0: globalAndroid.view.accessibility.AccessibilityEvent): boolean;
								}
								export module SignInHubActivity {
									export class zzc extends globalAndroid.support.v4.app.LoaderManager.LoaderCallbacks<java.lang.Void> {
										public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.SignInHubActivity.zzc>;
										public onLoaderReset(param0: globalAndroid.support.v4.content.Loader<java.lang.Void>): void;
										public onCreateLoader(param0: number, param1: globalAndroid.os.Bundle): globalAndroid.support.v4.content.Loader<java.lang.Void>;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzc extends com.google.android.gms.auth.api.signin.internal.zzt {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzc>;
									public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
									public constructor();
									public asBinder(): globalAndroid.os.IBinder;
									public isBinderAlive(): boolean;
									public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
									public getInterfaceDescriptor(): string;
									public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public constructor(param0: string);
									public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public pingBinder(): boolean;
									public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
									public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzd extends java.lang.Object implements java.lang.Runnable {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzd>;
									public run(): void;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zze extends globalAndroid.support.v4.content.AsyncTaskLoader<java.lang.Void> implements com.google.android.gms.common.api.internal.SignInConnectionListener  {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zze>;
									public onComplete(): void;
									public onStartLoading(): void;
									public constructor(param0: globalAndroid.content.Context, param1: java.util.Set<com.google.android.gms.common.api.GoogleApiClient>);
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzf extends java.lang.Object implements com.google.android.gms.auth.api.signin.GoogleSignInApi {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzf>;
									public constructor();
									public getSignInIntent(param0: com.google.android.gms.common.api.GoogleApiClient): globalAndroid.content.Intent;
									public getSignInResultFromIntent(param0: globalAndroid.content.Intent): com.google.android.gms.auth.api.signin.GoogleSignInResult;
									public revokeAccess(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
									public signOut(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
									public silentSignIn(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.OptionalPendingResult<com.google.android.gms.auth.api.signin.GoogleSignInResult>;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzg extends java.lang.Object /* com.google.android.gms.common.internal.GmsClient<com.google.android.gms.auth.api.signin.internal.zzu>*/ {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzg>;
									public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
									public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
									public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
									public getSignInIntent(): globalAndroid.content.Intent;
									public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
									public getConnectionHint(): globalAndroid.os.Bundle;
									public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
									public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
									public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
									public isConnecting(): boolean;
									public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param7: string);
									public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
									public providesSignIn(): boolean;
									public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.ClientSettings, param3: com.google.android.gms.auth.api.signin.GoogleSignInOptions, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
									public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener);
									public disconnect(): void;
									public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param4: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param5: string);
									public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
									public getEndpointPackageName(): string;
									public getMinApkVersion(): number;
									public getServiceDescriptor(): string;
									public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
									public requiresSignIn(): boolean;
									public requiresAccount(): boolean;
									public requiresGooglePlayServices(): boolean;
									public getStartServiceAction(): string;
									public getServiceBrokerBinder(): globalAndroid.os.IBinder;
									public isConnected(): boolean;
									public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzh extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzh>;
									public static getSignInResultFromIntent(param0: globalAndroid.content.Intent): com.google.android.gms.auth.api.signin.GoogleSignInResult;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzi extends com.google.android.gms.auth.api.signin.internal.zzo<com.google.android.gms.auth.api.signin.GoogleSignInResult> {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzi>;
									public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
									public setResult(param0: any): void;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzj extends com.google.android.gms.auth.api.signin.internal.zzc {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzj>;
									public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
									public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public asBinder(): globalAndroid.os.IBinder;
									public isBinderAlive(): boolean;
									public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
									public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public pingBinder(): boolean;
									public getInterfaceDescriptor(): string;
									public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
									public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
									public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzk extends com.google.android.gms.auth.api.signin.internal.zzo<com.google.android.gms.common.api.Status> {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzk>;
									public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
									public setResult(param0: any): void;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzl extends com.google.android.gms.auth.api.signin.internal.zzc {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzl>;
									public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
									public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public asBinder(): globalAndroid.os.IBinder;
									public isBinderAlive(): boolean;
									public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
									public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public pingBinder(): boolean;
									public getInterfaceDescriptor(): string;
									public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
									public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
									public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzm extends com.google.android.gms.auth.api.signin.internal.zzo<com.google.android.gms.common.api.Status> {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzm>;
									public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
									public setResult(param0: any): void;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzn extends com.google.android.gms.auth.api.signin.internal.zzc {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzn>;
									public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
									public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public asBinder(): globalAndroid.os.IBinder;
									public isBinderAlive(): boolean;
									public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
									public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public pingBinder(): boolean;
									public getInterfaceDescriptor(): string;
									public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
									public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
									public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export abstract class zzo<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.auth.api.signin.internal.zzg>*/ {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzo<any>>;
									public constructor();
									public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
									public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
									public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
									public setResult(param0: any): void;
									public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
									public constructor(param0: globalAndroid.os.Looper);
									public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzp extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzp>;
									public clear(): void;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzq extends java.lang.Object implements globalAndroid.os.IInterface {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzq>;
									/**
									 * Constructs a new instance of the com.google.android.gms.auth.api.signin.internal.zzq interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
									 */
									public constructor(implementation: {
										zzj(): void;
										zzk(): void;
										asBinder(): globalAndroid.os.IBinder;
									});
									public constructor();
									public asBinder(): globalAndroid.os.IBinder;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export abstract class zzr extends com.google.android.gms.internal.auth.api.zzd implements com.google.android.gms.auth.api.signin.internal.zzq {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzr>;
									public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
									public constructor();
									public asBinder(): globalAndroid.os.IBinder;
									public isBinderAlive(): boolean;
									public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
									public getInterfaceDescriptor(): string;
									public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public constructor(param0: string);
									public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public pingBinder(): boolean;
									public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
									public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzs extends java.lang.Object implements globalAndroid.os.IInterface {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzs>;
									/**
									 * Constructs a new instance of the com.google.android.gms.auth.api.signin.internal.zzs interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
									 */
									public constructor(implementation: {
										zzc(param0: com.google.android.gms.auth.api.signin.GoogleSignInAccount, param1: com.google.android.gms.common.api.Status): void;
										zze(param0: com.google.android.gms.common.api.Status): void;
										zzf(param0: com.google.android.gms.common.api.Status): void;
										asBinder(): globalAndroid.os.IBinder;
									});
									public constructor();
									public asBinder(): globalAndroid.os.IBinder;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export abstract class zzt extends com.google.android.gms.internal.auth.api.zzd implements com.google.android.gms.auth.api.signin.internal.zzs {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzt>;
									public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
									public constructor();
									public asBinder(): globalAndroid.os.IBinder;
									public isBinderAlive(): boolean;
									public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
									public getInterfaceDescriptor(): string;
									public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public constructor(param0: string);
									public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public pingBinder(): boolean;
									public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
									public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzu extends java.lang.Object implements globalAndroid.os.IInterface {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzu>;
									/**
									 * Constructs a new instance of the com.google.android.gms.auth.api.signin.internal.zzu interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
									 */
									public constructor(implementation: {
										zzc(param0: any /* com.google.android.gms.auth.api.signin.internal.zzs*/, param1: com.google.android.gms.auth.api.signin.GoogleSignInOptions): void;
										zzd(param0: any /* com.google.android.gms.auth.api.signin.internal.zzs*/, param1: com.google.android.gms.auth.api.signin.GoogleSignInOptions): void;
										zze(param0: any /* com.google.android.gms.auth.api.signin.internal.zzs*/, param1: com.google.android.gms.auth.api.signin.GoogleSignInOptions): void;
										asBinder(): globalAndroid.os.IBinder;
									});
									public constructor();
									public asBinder(): globalAndroid.os.IBinder;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzv extends com.google.android.gms.internal.auth.api.zzc implements com.google.android.gms.auth.api.signin.internal.zzu {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzv>;
									public asBinder(): globalAndroid.os.IBinder;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzw extends com.google.android.gms.auth.api.signin.internal.zzr {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzw>;
									public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
									public constructor();
									public asBinder(): globalAndroid.os.IBinder;
									public isBinderAlive(): boolean;
									public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
									public getInterfaceDescriptor(): string;
									public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public constructor(param0: string);
									public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public constructor(param0: globalAndroid.content.Context);
									public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public pingBinder(): boolean;
									public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
									public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzx extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.signin.internal.SignInConfiguration> {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzx>;
									public constructor();
									public createFromParcel(param0: globalAndroid.os.Parcel): any;
									public newArray(param0: number): native.Array<any>;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export module internal {
								export class zzy extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.signin.internal.zzy>;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export class zzc extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.signin.zzc>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module signin {
							export class zzd extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.signin.SignInAccount> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.signin.zzd>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export class zzc extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.auth.api.zzr,com.google.android.gms.auth.api.Auth.AuthCredentialsOptions> {
							public static class: java.lang.Class<com.google.android.gms.auth.api.zzc>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export class zzd extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.auth.api.signin.internal.zzg,com.google.android.gms.auth.api.signin.GoogleSignInOptions> {
							public static class: java.lang.Class<com.google.android.gms.auth.api.zzd>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzc extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzc>;
							public constructor(param0: globalAndroid.os.IBinder, param1: string);
							public asBinder(): globalAndroid.os.IBinder;
							public obtainAndWriteInterfaceToken(): globalAndroid.os.Parcel;
							public transactAndReadExceptionReturnVoid(param0: number, param1: globalAndroid.os.Parcel): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzd extends globalAndroid.os.Binder implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzd>;
							public constructor();
							public onTransact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zze extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zze>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzf extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzf>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.auth.api.zzf interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
							});
							public constructor();
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzg extends com.google.android.gms.internal.auth.api.zzv {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzg>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzh extends java.lang.Object implements com.google.android.gms.auth.api.credentials.CredentialRequestResult {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzh>;
							public getStatus(): com.google.android.gms.common.api.Status;
							public getCredential(): com.google.android.gms.auth.api.credentials.Credential;
							public constructor(param0: com.google.android.gms.common.api.Status, param1: com.google.android.gms.auth.api.credentials.Credential);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzi extends java.lang.Object implements com.google.android.gms.auth.api.credentials.CredentialsApi {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzi>;
							public constructor();
							public save(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.Credential): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public request(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.CredentialRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.api.credentials.CredentialRequestResult>;
							public delete(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.Credential): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public disableAutoSignIn(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public getHintPickerIntent(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.credentials.HintRequest): globalAndroid.app.PendingIntent;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzj extends com.google.android.gms.internal.auth.api.zzp<com.google.android.gms.auth.api.credentials.CredentialRequestResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzj>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzk extends com.google.android.gms.internal.auth.api.zzg {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzk>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzl extends com.google.android.gms.internal.auth.api.zzp<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzl>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzm extends com.google.android.gms.internal.auth.api.zzp<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzm>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzn extends com.google.android.gms.internal.auth.api.zzp<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzn>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzo extends com.google.android.gms.internal.auth.api.zzg {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzo>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export abstract class zzp<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.auth.api.zzr>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzp<any>>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzq extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzq>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzr extends java.lang.Object /* com.google.android.gms.common.internal.GmsClient<com.google.android.gms.internal.auth.api.zzw>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzr>;
							public getStartServiceAction(): string;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param7: string);
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getGetServiceRequestExtraArgs(): globalAndroid.os.Bundle;
							public requiresAccount(): boolean;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.ClientSettings, param3: com.google.android.gms.auth.api.Auth.AuthCredentialsOptions, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param4: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param5: string);
							public isConnecting(): boolean;
							public getMinApkVersion(): number;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener);
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzs extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzs>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.api.zzs>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: com.google.android.gms.auth.api.credentials.Credential);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzt extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.api.zzs>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzt>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzu extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzu>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.auth.api.zzu interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zzc(param0: com.google.android.gms.common.api.Status, param1: com.google.android.gms.auth.api.credentials.Credential): void;
								zzc(param0: com.google.android.gms.common.api.Status): void;
								zzc(param0: com.google.android.gms.common.api.Status, param1: string): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export abstract class zzv extends com.google.android.gms.internal.auth.api.zzd implements com.google.android.gms.internal.auth.api.zzu {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzv>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzw extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzw>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.auth.api.zzw interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zzc(param0: any /* com.google.android.gms.internal.auth.api.zzu*/, param1: com.google.android.gms.auth.api.credentials.CredentialRequest): void;
								zzc(param0: any /* com.google.android.gms.internal.auth.api.zzu*/, param1: any /* com.google.android.gms.internal.auth.api.zzy*/): void;
								zzc(param0: any /* com.google.android.gms.internal.auth.api.zzu*/, param1: any /* com.google.android.gms.internal.auth.api.zzs*/): void;
								zzc(param0: any /* com.google.android.gms.internal.auth.api.zzu*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzx extends com.google.android.gms.internal.auth.api.zzc implements com.google.android.gms.internal.auth.api.zzw {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzx>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzy extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzy>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.api.zzy>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: com.google.android.gms.auth.api.credentials.Credential);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth.api {
						export class zzz extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.api.zzy>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.api.zzz>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

//Generics information:
//com.google.android.gms.auth.api.signin.internal.zzo:1
//com.google.android.gms.internal.auth.api.zzp:1

