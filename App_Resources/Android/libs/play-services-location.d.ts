declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zza extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zza>;
							public constructor(param0: globalAndroid.os.IBinder, param1: string);
							public asBinder(): globalAndroid.os.IBinder;
							public obtainAndWriteInterfaceToken(): globalAndroid.os.Parcel;
							public transactAndReadException(param0: number, param1: globalAndroid.os.Parcel): globalAndroid.os.Parcel;
							public transactOneway(param0: number, param1: globalAndroid.os.Parcel): void;
							public transactAndReadExceptionReturnVoid(param0: number, param1: globalAndroid.os.Parcel): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzaa extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzaa>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export abstract class zzab extends com.google.android.gms.location.LocationServices.zza<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzab>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzac extends com.google.android.gms.internal.location.zzak {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzac>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public constructor(param0: com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder<com.google.android.gms.common.api.Status>);
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzad extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzad>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzad>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getStatus(): com.google.android.gms.common.api.Status;
							public constructor(param0: com.google.android.gms.common.api.Status);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzae extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzad>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzae>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzaf extends java.lang.Object implements com.google.android.gms.location.GeofencingApi {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzaf>;
							public constructor();
							public addGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.GeofencingRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public removeGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: java.util.List<string>): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public addGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: java.util.List<com.google.android.gms.location.Geofence>, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public removeGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzag extends com.google.android.gms.internal.location.zzai {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzag>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzah extends com.google.android.gms.internal.location.zzai {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzah>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export abstract class zzai extends com.google.android.gms.location.LocationServices.zza<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzai>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzaj extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzaj>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.location.zzaj interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: any /* com.google.android.gms.internal.location.zzad*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export abstract class zzak extends com.google.android.gms.internal.location.zzb implements com.google.android.gms.internal.location.zzaj {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzak>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzal extends com.google.android.gms.internal.location.zza implements com.google.android.gms.internal.location.zzaj {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzal>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzam extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzam>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.location.zzam interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: number, param1: native.Array<string>): void;
								zzb(param0: number, param1: native.Array<string>): void;
								zza(param0: number, param1: globalAndroid.app.PendingIntent): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export abstract class zzan extends com.google.android.gms.internal.location.zzb implements com.google.android.gms.internal.location.zzam {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzan>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzao extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzao>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.location.zzao interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.location.GeofencingRequest, param1: globalAndroid.app.PendingIntent, param2: any /* com.google.android.gms.internal.location.zzam*/): void;
								zza(param0: any /* com.google.android.gms.location.zzal*/, param1: any /* com.google.android.gms.internal.location.zzam*/): void;
								zza(param0: number, param1: boolean, param2: globalAndroid.app.PendingIntent): void;
								zza(param0: com.google.android.gms.location.ActivityTransitionRequest, param1: globalAndroid.app.PendingIntent, param2: com.google.android.gms.common.api.internal.IStatusCallback): void;
								zza(param0: globalAndroid.app.PendingIntent, param1: com.google.android.gms.common.api.internal.IStatusCallback): void;
								zzb(param0: globalAndroid.app.PendingIntent): void;
								zza(param0: any /* com.google.android.gms.internal.location.zzbf*/): void;
								zza(param0: boolean): void;
								zza(param0: globalAndroid.location.Location): void;
								zza(param0: string): globalAndroid.location.Location;
								zza(param0: any /* com.google.android.gms.internal.location.zzaj*/): void;
								zzb(param0: string): com.google.android.gms.location.LocationAvailability;
								zza(param0: com.google.android.gms.location.LocationSettingsRequest, param1: any /* com.google.android.gms.internal.location.zzaq*/, param2: string): void;
								zza(param0: any /* com.google.android.gms.internal.location.zzo*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzap extends com.google.android.gms.internal.location.zza implements com.google.android.gms.internal.location.zzao {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzap>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzaq extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzaq>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.location.zzaq interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.location.LocationSettingsResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export abstract class zzar extends com.google.android.gms.internal.location.zzb implements com.google.android.gms.internal.location.zzaq {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzar>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzas extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzas>;
							public getLastLocation(): globalAndroid.location.Location;
							public removeAllListeners(): void;
							public constructor(param0: globalAndroid.content.Context, param1: any /* com.google.android.gms.internal.location.zzbj<com.google.android.gms.internal.location.zzao>*/);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzat extends com.google.android.gms.location.zzv {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzat>;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public release(): void;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public onLocationResult(param0: com.google.android.gms.location.LocationResult): void;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public onLocationAvailability(param0: com.google.android.gms.location.LocationAvailability): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzau extends com.google.android.gms.common.api.internal.ListenerHolder.Notifier<com.google.android.gms.location.LocationCallback> {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzau>;
							public onNotifyListenerFailed(): void;
							public notifyListener(param0: any): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzav extends com.google.android.gms.common.api.internal.ListenerHolder.Notifier<com.google.android.gms.location.LocationCallback> {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzav>;
							public onNotifyListenerFailed(): void;
							public notifyListener(param0: any): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzaw extends com.google.android.gms.location.zzs {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzaw>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzax extends com.google.android.gms.location.zzy {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzax>;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public release(): void;
							public pingBinder(): boolean;
							public onLocationChanged(param0: globalAndroid.location.Location): void;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzay extends com.google.android.gms.common.api.internal.ListenerHolder.Notifier<com.google.android.gms.location.LocationListener> {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzay>;
							public onNotifyListenerFailed(): void;
							public notifyListener(param0: any): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzaz extends com.google.android.gms.internal.location.zzk {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzaz>;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param7: string);
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public getConnectionHint(): globalAndroid.os.Bundle;
							public requiresAccount(): boolean;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param3: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener, param4: string, param5: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param4: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param5: string);
							public isConnecting(): boolean;
							public getLastLocation(): globalAndroid.location.Location;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param3: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener, param4: string);
							public getMinApkVersion(): number;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener);
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzb extends globalAndroid.os.Binder implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzb>;
							public constructor();
							public onTransact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzba extends com.google.android.gms.internal.location.zzan {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzba>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public constructor(param0: com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder<com.google.android.gms.common.api.Status>);
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbb extends com.google.android.gms.internal.location.zzan {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbb>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public constructor(param0: com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder<com.google.android.gms.common.api.Status>);
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbc extends com.google.android.gms.internal.location.zzar {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbc>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public constructor(param0: com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder<com.google.android.gms.location.LocationSettingsResult>);
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbd extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbd>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzbd>*/;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbe extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzbd>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbe>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbf extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbf>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzbf>*/;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbg extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzbf>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbg>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbh extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.location.Geofence {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbh>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzbh>*/;
							public constructor();
							public getRequestId(): string;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public constructor(param0: string, param1: number, param2: number, param3: number, param4: number, param5: number, param6: number, param7: number, param8: number);
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbi extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzbh>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbi>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbj<T>  extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbj<any>>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.location.zzbj<any> interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								getService(): T;
								checkConnected(): void;
							});
							public constructor();
							public checkConnected(): void;
							public getService(): T;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbk extends java.lang.Object implements com.google.android.gms.location.SettingsApi {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbk>;
							public constructor();
							public checkLocationSettings(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationSettingsRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.location.LocationSettingsResult>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbl extends com.google.android.gms.location.LocationServices.zza<com.google.android.gms.location.LocationSettingsResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbl>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzbm extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzbm>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzc extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzc>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzd extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzd>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.location.zzd interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
							});
							public constructor();
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zze extends java.lang.Object implements com.google.android.gms.location.ActivityRecognitionApi {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zze>;
							public constructor();
							public removeActivityUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public requestActivityUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: number, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzf extends com.google.android.gms.internal.location.zzj {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzf>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzg extends com.google.android.gms.internal.location.zzj {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzg>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzh extends com.google.android.gms.internal.location.zzj {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzh>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzi extends com.google.android.gms.internal.location.zzj {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzi>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export abstract class zzj extends com.google.android.gms.location.ActivityRecognition.zza<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzj>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzk extends java.lang.Object /* com.google.android.gms.common.internal.GmsClient<com.google.android.gms.internal.location.zzao>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzk>;
							public getStartServiceAction(): string;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param7: string);
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getGetServiceRequestExtraArgs(): globalAndroid.os.Bundle;
							public requiresAccount(): boolean;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param3: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener, param4: string, param5: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param4: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param5: string);
							public isConnecting(): boolean;
							public getMinApkVersion(): number;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener);
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzl extends java.lang.Object /* com.google.android.gms.internal.location.zzbj<com.google.android.gms.internal.location.zzao>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzl>;
							public getService(): any;
							public checkConnected(): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzm extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzm>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzm>*/;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzn extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzm>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzn>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzo extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzo>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzo>*/;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzp extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.location.zzo>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzp>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzq extends java.lang.Object implements com.google.android.gms.location.FusedLocationProviderApi {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzq>;
							public constructor();
							public getLocationAvailability(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.location.LocationAvailability;
							public removeLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: com.google.android.gms.location.LocationListener, param3: globalAndroid.os.Looper): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: com.google.android.gms.location.LocationCallback, param3: globalAndroid.os.Looper): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public getLastLocation(param0: com.google.android.gms.common.api.GoogleApiClient): globalAndroid.location.Location;
							public removeLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public setMockMode(param0: com.google.android.gms.common.api.GoogleApiClient, param1: boolean): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public setMockLocation(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.location.Location): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public flushLocations(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: com.google.android.gms.location.LocationListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public removeLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationCallback): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzr extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzr>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzs extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzs>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzt extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzt>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzu extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzu>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzv extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzv>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzw extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzw>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzx extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzx>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzy extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzy>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module location {
						export class zzz extends com.google.android.gms.internal.location.zzab {
							public static class: java.lang.Class<com.google.android.gms.internal.location.zzz>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class ActivityRecognition extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.ActivityRecognition>;
						public static CLIENT_NAME: string;
						public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
						public static ActivityRecognitionApi: com.google.android.gms.location.ActivityRecognitionApi;
						public static getClient(param0: globalAndroid.content.Context): com.google.android.gms.location.ActivityRecognitionClient;
						public static getClient(param0: globalAndroid.app.Activity): com.google.android.gms.location.ActivityRecognitionClient;
					}
					export module ActivityRecognition {
						export abstract class zza<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.location.zzaz>*/ {
							public static class: java.lang.Class<com.google.android.gms.location.ActivityRecognition.zza<any>>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class ActivityRecognitionApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.ActivityRecognitionApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.location.ActivityRecognitionApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							requestActivityUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: number, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							removeActivityUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							zza(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.ActivityTransitionRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							zza(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						});
						public constructor();
						public requestActivityUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: number, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public removeActivityUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class ActivityRecognitionClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
						public static class: java.lang.Class<com.google.android.gms.location.ActivityRecognitionClient>;
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.GoogleApi.Settings);
						public requestActivityTransitionUpdates(param0: com.google.android.gms.location.ActivityTransitionRequest, param1: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public constructor(param0: globalAndroid.app.Activity);
						public constructor(param0: globalAndroid.content.Context);
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: globalAndroid.os.Looper);
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public requestActivityUpdates(param0: number, param1: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: globalAndroid.os.Looper, param4: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public removeActivityTransitionUpdates(param0: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public removeActivityUpdates(param0: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public constructor(param0: globalAndroid.app.Activity, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public constructor(param0: globalAndroid.app.Activity, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.GoogleApi.Settings);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class ActivityRecognitionResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.ActivityRecognitionResult>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityRecognitionResult>;
						public constructor();
						public constructor(param0: com.google.android.gms.location.DetectedActivity, param1: number, param2: number);
						public describeContents(): number;
						public static extractResult(param0: globalAndroid.content.Intent): com.google.android.gms.location.ActivityRecognitionResult;
						public getElapsedRealtimeMillis(): number;
						public constructor(param0: java.util.List<com.google.android.gms.location.DetectedActivity>, param1: number, param2: number, param3: number, param4: globalAndroid.os.Bundle);
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public equals(param0: any): boolean;
						public hashCode(): number;
						public getTime(): number;
						public toString(): string;
						public constructor(param0: java.util.List<com.google.android.gms.location.DetectedActivity>, param1: number, param2: number);
						public getActivityConfidence(param0: number): number;
						public static hasResult(param0: globalAndroid.content.Intent): boolean;
						public getMostProbableActivity(): com.google.android.gms.location.DetectedActivity;
						public getProbableActivities(): java.util.List<com.google.android.gms.location.DetectedActivity>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class ActivityTransition extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.ActivityTransition>;
						public static ACTIVITY_TRANSITION_ENTER: number;
						public static ACTIVITY_TRANSITION_EXIT: number;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityTransition>;
						public getTransitionType(): number;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public hashCode(): number;
						public equals(param0: any): boolean;
						public getActivityType(): number;
						public toString(): string;
					}
					export module ActivityTransition {
						export class Builder extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.location.ActivityTransition.Builder>;
							public constructor();
							public build(): com.google.android.gms.location.ActivityTransition;
							public setActivityType(param0: number): com.google.android.gms.location.ActivityTransition.Builder;
							public setActivityTransition(param0: number): com.google.android.gms.location.ActivityTransition.Builder;
						}
						export class SupportedActivityTransition extends java.lang.Object implements java.lang.annotation.Annotation {
							public static class: java.lang.Class<com.google.android.gms.location.ActivityTransition.SupportedActivityTransition>;
							/**
							 * Constructs a new instance of the com.google.android.gms.location.ActivityTransition$SupportedActivityTransition interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								equals(param0: any): boolean;
								hashCode(): number;
								toString(): string;
								annotationType(): java.lang.Class<any>;
							});
							public constructor();
							public hashCode(): number;
							public annotationType(): java.lang.Class<any>;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class ActivityTransitionEvent extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.ActivityTransitionEvent>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityTransitionEvent>;
						public constructor();
						public getTransitionType(): number;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public hashCode(): number;
						public equals(param0: any): boolean;
						public getElapsedRealTimeNanos(): number;
						public constructor(param0: number, param1: number, param2: number);
						public getActivityType(): number;
						public toString(): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class ActivityTransitionRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.ActivityTransitionRequest>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityTransitionRequest>;
						public static IS_SAME_TRANSITION: java.util.Comparator<com.google.android.gms.location.ActivityTransition>;
						public constructor();
						public serializeToIntentExtra(param0: globalAndroid.content.Intent): void;
						public constructor(param0: java.util.List<com.google.android.gms.location.ActivityTransition>, param1: string, param2: java.util.List<com.google.android.gms.common.internal.ClientIdentity>);
						public constructor(param0: java.util.List<com.google.android.gms.location.ActivityTransition>);
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public equals(param0: any): boolean;
						public hashCode(): number;
						public toString(): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class ActivityTransitionResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.ActivityTransitionResult>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityTransitionResult>;
						public static extractResult(param0: globalAndroid.content.Intent): com.google.android.gms.location.ActivityTransitionResult;
						public constructor();
						public constructor(param0: java.util.List<com.google.android.gms.location.ActivityTransitionEvent>);
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public static hasResult(param0: globalAndroid.content.Intent): boolean;
						public equals(param0: any): boolean;
						public hashCode(): number;
						public getTransitionEvents(): java.util.List<com.google.android.gms.location.ActivityTransitionEvent>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class DetectedActivity extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.DetectedActivity>;
						public static IN_VEHICLE: number;
						public static ON_BICYCLE: number;
						public static ON_FOOT: number;
						public static STILL: number;
						public static UNKNOWN: number;
						public static TILTING: number;
						public static WALKING: number;
						public static RUNNING: number;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.DetectedActivity>;
						public getConfidence(): number;
						public constructor();
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public equals(param0: any): boolean;
						public hashCode(): number;
						public getType(): number;
						public constructor(param0: number, param1: number);
						public toString(): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class FusedLocationProviderApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.FusedLocationProviderApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.location.FusedLocationProviderApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							getLastLocation(param0: com.google.android.gms.common.api.GoogleApiClient): globalAndroid.location.Location;
							getLocationAvailability(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.location.LocationAvailability;
							requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: com.google.android.gms.location.LocationListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: com.google.android.gms.location.LocationListener, param3: globalAndroid.os.Looper): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: com.google.android.gms.location.LocationCallback, param3: globalAndroid.os.Looper): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							removeLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							removeLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							removeLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationCallback): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							setMockMode(param0: com.google.android.gms.common.api.GoogleApiClient, param1: boolean): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							setMockLocation(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.location.Location): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							flushLocations(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						});
						public constructor();
						public static KEY_MOCK_LOCATION: string;
						public static KEY_LOCATION_CHANGED: string;
						public setMockLocation(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.location.Location): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public getLastLocation(param0: com.google.android.gms.common.api.GoogleApiClient): globalAndroid.location.Location;
						public requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: com.google.android.gms.location.LocationCallback, param3: globalAndroid.os.Looper): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public removeLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public flushLocations(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: com.google.android.gms.location.LocationListener, param3: globalAndroid.os.Looper): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public getLocationAvailability(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.location.LocationAvailability;
						public removeLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public removeLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationCallback): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public requestLocationUpdates(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationRequest, param2: com.google.android.gms.location.LocationListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public setMockMode(param0: com.google.android.gms.common.api.GoogleApiClient, param1: boolean): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class FusedLocationProviderClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
						public static class: java.lang.Class<com.google.android.gms.location.FusedLocationProviderClient>;
						public static KEY_VERTICAL_ACCURACY: string;
						public constructor(param0: globalAndroid.app.Activity);
						public constructor(param0: globalAndroid.content.Context);
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: globalAndroid.os.Looper);
						public flushLocations(): com.google.android.gms.tasks.Task<java.lang.Void>;
						public getLocationAvailability(): com.google.android.gms.tasks.Task<com.google.android.gms.location.LocationAvailability>;
						public removeLocationUpdates(param0: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public requestLocationUpdates(param0: com.google.android.gms.location.LocationRequest, param1: com.google.android.gms.location.LocationCallback, param2: globalAndroid.os.Looper): com.google.android.gms.tasks.Task<java.lang.Void>;
						public requestLocationUpdates(param0: com.google.android.gms.location.LocationRequest, param1: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.GoogleApi.Settings);
						public getLastLocation(): com.google.android.gms.tasks.Task<globalAndroid.location.Location>;
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public removeLocationUpdates(param0: com.google.android.gms.location.LocationCallback): com.google.android.gms.tasks.Task<java.lang.Void>;
						public setMockLocation(param0: globalAndroid.location.Location): com.google.android.gms.tasks.Task<java.lang.Void>;
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: globalAndroid.os.Looper, param4: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public constructor(param0: globalAndroid.app.Activity, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public constructor(param0: globalAndroid.app.Activity, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.GoogleApi.Settings);
						public setMockMode(param0: boolean): com.google.android.gms.tasks.Task<java.lang.Void>;
					}
					export module FusedLocationProviderClient {
						export class zza extends com.google.android.gms.internal.location.zzak {
							public static class: java.lang.Class<com.google.android.gms.location.FusedLocationProviderClient.zza>;
							public constructor();
							public constructor(param0: com.google.android.gms.tasks.TaskCompletionSource<java.lang.Void>);
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class Geofence extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.Geofence>;
						/**
						 * Constructs a new instance of the com.google.android.gms.location.Geofence interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							getRequestId(): string;
						});
						public constructor();
						public static NEVER_EXPIRE: number;
						public static GEOFENCE_TRANSITION_EXIT: number;
						public static GEOFENCE_TRANSITION_ENTER: number;
						public static GEOFENCE_TRANSITION_DWELL: number;
						public getRequestId(): string;
					}
					export module Geofence {
						export class Builder extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.location.Geofence.Builder>;
							public constructor();
							public setLoiteringDelay(param0: number): com.google.android.gms.location.Geofence.Builder;
							public setExpirationDuration(param0: number): com.google.android.gms.location.Geofence.Builder;
							public setNotificationResponsiveness(param0: number): com.google.android.gms.location.Geofence.Builder;
							public setRequestId(param0: string): com.google.android.gms.location.Geofence.Builder;
							public setCircularRegion(param0: number, param1: number, param2: number): com.google.android.gms.location.Geofence.Builder;
							public setTransitionTypes(param0: number): com.google.android.gms.location.Geofence.Builder;
							public build(): com.google.android.gms.location.Geofence;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class GeofenceStatusCodes extends com.google.android.gms.common.api.CommonStatusCodes {
						public static class: java.lang.Class<com.google.android.gms.location.GeofenceStatusCodes>;
						public static GEOFENCE_NOT_AVAILABLE: number;
						public static GEOFENCE_TOO_MANY_GEOFENCES: number;
						public static GEOFENCE_TOO_MANY_PENDING_INTENTS: number;
						public static getStatusCodeString(param0: number): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class GeofencingApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.GeofencingApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.location.GeofencingApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							addGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: java.util.List<com.google.android.gms.location.Geofence>, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							addGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.GeofencingRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							removeGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							removeGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: java.util.List<string>): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						});
						public constructor();
						public removeGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public removeGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: java.util.List<string>): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public addGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.GeofencingRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public addGeofences(param0: com.google.android.gms.common.api.GoogleApiClient, param1: java.util.List<com.google.android.gms.location.Geofence>, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class GeofencingClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
						public static class: java.lang.Class<com.google.android.gms.location.GeofencingClient>;
						public removeGeofences(param0: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.GoogleApi.Settings);
						public constructor(param0: globalAndroid.app.Activity);
						public constructor(param0: globalAndroid.content.Context);
						public addGeofences(param0: com.google.android.gms.location.GeofencingRequest, param1: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: globalAndroid.os.Looper);
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: globalAndroid.os.Looper, param4: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public removeGeofences(param0: java.util.List<string>): com.google.android.gms.tasks.Task<java.lang.Void>;
						public constructor(param0: globalAndroid.app.Activity, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public constructor(param0: globalAndroid.app.Activity, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.GoogleApi.Settings);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class GeofencingEvent extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.GeofencingEvent>;
						public static fromIntent(param0: globalAndroid.content.Intent): com.google.android.gms.location.GeofencingEvent;
						public getErrorCode(): number;
						public getTriggeringGeofences(): java.util.List<com.google.android.gms.location.Geofence>;
						public getTriggeringLocation(): globalAndroid.location.Location;
						public getGeofenceTransition(): number;
						public hasError(): boolean;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class GeofencingRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.GeofencingRequest>;
						public static INITIAL_TRIGGER_ENTER: number;
						public static INITIAL_TRIGGER_EXIT: number;
						public static INITIAL_TRIGGER_DWELL: number;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.GeofencingRequest>;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public getGeofences(): java.util.List<com.google.android.gms.location.Geofence>;
						public getInitialTrigger(): number;
						public toString(): string;
					}
					export module GeofencingRequest {
						export class Builder extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.location.GeofencingRequest.Builder>;
							public constructor();
							public build(): com.google.android.gms.location.GeofencingRequest;
							public addGeofences(param0: java.util.List<com.google.android.gms.location.Geofence>): com.google.android.gms.location.GeofencingRequest.Builder;
							public addGeofence(param0: com.google.android.gms.location.Geofence): com.google.android.gms.location.GeofencingRequest.Builder;
							public setInitialTrigger(param0: number): com.google.android.gms.location.GeofencingRequest.Builder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationAvailability extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.LocationAvailability>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationAvailability>;
						public isLocationAvailable(): boolean;
						public describeContents(): number;
						public static extractLocationAvailability(param0: globalAndroid.content.Intent): com.google.android.gms.location.LocationAvailability;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public hashCode(): number;
						public equals(param0: any): boolean;
						public static hasLocationAvailability(param0: globalAndroid.content.Intent): boolean;
						public toString(): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationCallback extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.LocationCallback>;
						public onLocationResult(param0: com.google.android.gms.location.LocationResult): void;
						public constructor();
						public onLocationAvailability(param0: com.google.android.gms.location.LocationAvailability): void;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationListener extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.LocationListener>;
						/**
						 * Constructs a new instance of the com.google.android.gms.location.LocationListener interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							onLocationChanged(param0: globalAndroid.location.Location): void;
						});
						public constructor();
						public onLocationChanged(param0: globalAndroid.location.Location): void;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.LocationRequest>;
						public static PRIORITY_HIGH_ACCURACY: number;
						public static PRIORITY_BALANCED_POWER_ACCURACY: number;
						public static PRIORITY_LOW_POWER: number;
						public static PRIORITY_NO_POWER: number;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationRequest>;
						public constructor();
						public setExpirationTime(param0: number): com.google.android.gms.location.LocationRequest;
						public describeContents(): number;
						public setMaxWaitTime(param0: number): com.google.android.gms.location.LocationRequest;
						public getNumUpdates(): number;
						public setExpirationDuration(param0: number): com.google.android.gms.location.LocationRequest;
						public setFastestInterval(param0: number): com.google.android.gms.location.LocationRequest;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public hashCode(): number;
						public equals(param0: any): boolean;
						public getInterval(): number;
						public getMaxWaitTime(): number;
						public toString(): string;
						public isFastestIntervalExplicitlySet(): boolean;
						public setNumUpdates(param0: number): com.google.android.gms.location.LocationRequest;
						public setInterval(param0: number): com.google.android.gms.location.LocationRequest;
						public setPriority(param0: number): com.google.android.gms.location.LocationRequest;
						public getPriority(): number;
						public setSmallestDisplacement(param0: number): com.google.android.gms.location.LocationRequest;
						public static create(): com.google.android.gms.location.LocationRequest;
						public getSmallestDisplacement(): number;
						public getExpirationTime(): number;
						public getFastestInterval(): number;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.LocationResult>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationResult>;
						public static extractResult(param0: globalAndroid.content.Intent): com.google.android.gms.location.LocationResult;
						public describeContents(): number;
						public static create(param0: java.util.List<globalAndroid.location.Location>): com.google.android.gms.location.LocationResult;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public hashCode(): number;
						public equals(param0: any): boolean;
						public static hasResult(param0: globalAndroid.content.Intent): boolean;
						public getLocations(): java.util.List<globalAndroid.location.Location>;
						public getLastLocation(): globalAndroid.location.Location;
						public toString(): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationServices extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.LocationServices>;
						public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
						public static FusedLocationApi: com.google.android.gms.location.FusedLocationProviderApi;
						public static GeofencingApi: com.google.android.gms.location.GeofencingApi;
						public static SettingsApi: com.google.android.gms.location.SettingsApi;
						public static getFusedLocationProviderClient(param0: globalAndroid.content.Context): com.google.android.gms.location.FusedLocationProviderClient;
						public static getFusedLocationProviderClient(param0: globalAndroid.app.Activity): com.google.android.gms.location.FusedLocationProviderClient;
						public static getGeofencingClient(param0: globalAndroid.content.Context): com.google.android.gms.location.GeofencingClient;
						public static getGeofencingClient(param0: globalAndroid.app.Activity): com.google.android.gms.location.GeofencingClient;
						public static getSettingsClient(param0: globalAndroid.content.Context): com.google.android.gms.location.SettingsClient;
						public static getSettingsClient(param0: globalAndroid.app.Activity): com.google.android.gms.location.SettingsClient;
					}
					export module LocationServices {
						export abstract class zza<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.location.zzaz>*/ {
							public static class: java.lang.Class<com.google.android.gms.location.LocationServices.zza<any>>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationSettingsRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.LocationSettingsRequest>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationSettingsRequest>;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
					}
					export module LocationSettingsRequest {
						export class Builder extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.location.LocationSettingsRequest.Builder>;
							public constructor();
							public setAlwaysShow(param0: boolean): com.google.android.gms.location.LocationSettingsRequest.Builder;
							public addAllLocationRequests(param0: java.util.Collection<com.google.android.gms.location.LocationRequest>): com.google.android.gms.location.LocationSettingsRequest.Builder;
							public build(): com.google.android.gms.location.LocationSettingsRequest;
							public setNeedBle(param0: boolean): com.google.android.gms.location.LocationSettingsRequest.Builder;
							public addLocationRequest(param0: com.google.android.gms.location.LocationRequest): com.google.android.gms.location.LocationSettingsRequest.Builder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationSettingsResponse extends com.google.android.gms.common.api.Response<com.google.android.gms.location.LocationSettingsResult> {
						public static class: java.lang.Class<com.google.android.gms.location.LocationSettingsResponse>;
						public getLocationSettingsStates(): com.google.android.gms.location.LocationSettingsStates;
						public constructor();
						public constructor(param0: any);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationSettingsResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
						public static class: java.lang.Class<com.google.android.gms.location.LocationSettingsResult>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationSettingsResult>;
						public getLocationSettingsStates(): com.google.android.gms.location.LocationSettingsStates;
						public constructor();
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public constructor(param0: com.google.android.gms.common.api.Status, param1: com.google.android.gms.location.LocationSettingsStates);
						public constructor(param0: com.google.android.gms.common.api.Status);
						public getStatus(): com.google.android.gms.common.api.Status;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationSettingsStates extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.LocationSettingsStates>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationSettingsStates>;
						public isLocationUsable(): boolean;
						public constructor();
						public isLocationPresent(): boolean;
						public isBlePresent(): boolean;
						public isGpsUsable(): boolean;
						public isBleUsable(): boolean;
						public constructor(param0: boolean, param1: boolean, param2: boolean, param3: boolean, param4: boolean, param5: boolean);
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public isNetworkLocationUsable(): boolean;
						public static fromIntent(param0: globalAndroid.content.Intent): com.google.android.gms.location.LocationSettingsStates;
						public isNetworkLocationPresent(): boolean;
						public isGpsPresent(): boolean;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationSettingsStatusCodes extends com.google.android.gms.common.api.CommonStatusCodes {
						public static class: java.lang.Class<com.google.android.gms.location.LocationSettingsStatusCodes>;
						public static SETTINGS_CHANGE_UNAVAILABLE: number;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class LocationStatusCodes extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.LocationStatusCodes>;
						public static SUCCESS: number;
						public static ERROR: number;
						public static GEOFENCE_NOT_AVAILABLE: number;
						public static GEOFENCE_TOO_MANY_GEOFENCES: number;
						public static GEOFENCE_TOO_MANY_PENDING_INTENTS: number;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class SettingsApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.location.SettingsApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.location.SettingsApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							checkLocationSettings(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationSettingsRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.location.LocationSettingsResult>;
						});
						public constructor();
						public checkLocationSettings(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.location.LocationSettingsRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.location.LocationSettingsResult>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class SettingsClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
						public static class: java.lang.Class<com.google.android.gms.location.SettingsClient>;
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.GoogleApi.Settings);
						public constructor(param0: globalAndroid.app.Activity);
						public constructor(param0: globalAndroid.content.Context);
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: globalAndroid.os.Looper);
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: globalAndroid.os.Looper, param4: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public checkLocationSettings(param0: com.google.android.gms.location.LocationSettingsRequest): com.google.android.gms.tasks.Task<com.google.android.gms.location.LocationSettingsResponse>;
						public constructor(param0: globalAndroid.app.Activity, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.internal.StatusExceptionMapper);
						public constructor(param0: globalAndroid.app.Activity, param1: com.google.android.gms.common.api.Api<any>, param2: any, param3: com.google.android.gms.common.api.GoogleApi.Settings);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zza extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.location.zzaz,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
						public static class: java.lang.Class<com.google.android.gms.location.zza>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzaa extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationAvailability> {
						public static class: java.lang.Class<com.google.android.gms.location.zzaa>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzab extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationRequest> {
						public static class: java.lang.Class<com.google.android.gms.location.zzab>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzac extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationResult> {
						public static class: java.lang.Class<com.google.android.gms.location.zzac>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzad extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.location.zzaz,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
						public static class: java.lang.Class<com.google.android.gms.location.zzad>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzae extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.zzae>;
						public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.zzae>*/;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzaf extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.zzae>*/ {
						public static class: java.lang.Class<com.google.android.gms.location.zzaf>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzag extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationSettingsRequest> {
						public static class: java.lang.Class<com.google.android.gms.location.zzag>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzah extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationSettingsResult> {
						public static class: java.lang.Class<com.google.android.gms.location.zzah>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzai extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.LocationSettingsStates> {
						public static class: java.lang.Class<com.google.android.gms.location.zzai>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzaj extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.zzaj>;
						public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.zzaj>*/;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public hashCode(): number;
						public equals(param0: any): boolean;
						public toString(): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzak extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.zzaj>*/ {
						public static class: java.lang.Class<com.google.android.gms.location.zzak>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzal extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.zzal>;
						public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.zzal>*/;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzam extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.zzal>*/ {
						public static class: java.lang.Class<com.google.android.gms.location.zzam>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzb extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityRecognitionResult> {
						public static class: java.lang.Class<com.google.android.gms.location.zzb>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzc extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityTransition> {
						public static class: java.lang.Class<com.google.android.gms.location.zzc>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzd extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityTransitionEvent> {
						public static class: java.lang.Class<com.google.android.gms.location.zzd>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zze extends java.util.Comparator<com.google.android.gms.location.ActivityTransition> {
						public static class: java.lang.Class<com.google.android.gms.location.zze>;
						public thenComparing(param0: any /* any*/, param1: java.util.Comparator<any>): java.util.Comparator<any>;
						public static comparing(param0: any /* any*/, param1: java.util.Comparator<any>): java.util.Comparator<any>;
						public thenComparingLong(param0: any /* any*/): java.util.Comparator<any>;
						public equals(param0: any): boolean;
						public static comparingInt(param0: any /* any*/): java.util.Comparator<any>;
						public static comparingDouble(param0: any /* any*/): java.util.Comparator<any>;
						public static reverseOrder(): java.util.Comparator<any>;
						public static comparing(param0: any /* any*/): java.util.Comparator<any>;
						public thenComparingInt(param0: any /* any*/): java.util.Comparator<any>;
						public reversed(): java.util.Comparator<any>;
						public static nullsFirst(param0: java.util.Comparator<any>): java.util.Comparator<any>;
						public thenComparingDouble(param0: any /* any*/): java.util.Comparator<any>;
						public static nullsLast(param0: java.util.Comparator<any>): java.util.Comparator<any>;
						public thenComparing(param0: any /* any*/): java.util.Comparator<any>;
						public static naturalOrder(): java.util.Comparator<any>;
						public compare(param0: any, param1: any): number;
						public thenComparing(param0: java.util.Comparator<any>): java.util.Comparator<any>;
						public static comparingLong(param0: any /* any*/): java.util.Comparator<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzf extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityTransitionRequest> {
						public static class: java.lang.Class<com.google.android.gms.location.zzf>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzg extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.ActivityTransitionResult> {
						public static class: java.lang.Class<com.google.android.gms.location.zzg>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzh extends java.util.Comparator<com.google.android.gms.location.DetectedActivity> {
						public static class: java.lang.Class<com.google.android.gms.location.zzh>;
						public thenComparing(param0: any /* any*/, param1: java.util.Comparator<any>): java.util.Comparator<any>;
						public static comparing(param0: any /* any*/, param1: java.util.Comparator<any>): java.util.Comparator<any>;
						public thenComparingLong(param0: any /* any*/): java.util.Comparator<any>;
						public equals(param0: any): boolean;
						public static comparingInt(param0: any /* any*/): java.util.Comparator<any>;
						public static comparingDouble(param0: any /* any*/): java.util.Comparator<any>;
						public static reverseOrder(): java.util.Comparator<any>;
						public static comparing(param0: any /* any*/): java.util.Comparator<any>;
						public thenComparingInt(param0: any /* any*/): java.util.Comparator<any>;
						public reversed(): java.util.Comparator<any>;
						public static nullsFirst(param0: java.util.Comparator<any>): java.util.Comparator<any>;
						public thenComparingDouble(param0: any /* any*/): java.util.Comparator<any>;
						public static nullsLast(param0: java.util.Comparator<any>): java.util.Comparator<any>;
						public thenComparing(param0: any /* any*/): java.util.Comparator<any>;
						public static naturalOrder(): java.util.Comparator<any>;
						public compare(param0: any, param1: any): number;
						public thenComparing(param0: java.util.Comparator<any>): java.util.Comparator<any>;
						public static comparingLong(param0: any /* any*/): java.util.Comparator<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzi extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.DetectedActivity> {
						public static class: java.lang.Class<com.google.android.gms.location.zzi>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzj extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.location.zzj>;
						public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.zzj>*/;
						public constructor();
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public hashCode(): number;
						public equals(param0: any): boolean;
						public toString(): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzk extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.zzj>*/ {
						public static class: java.lang.Class<com.google.android.gms.location.zzk>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzl extends com.google.android.gms.common.api.internal.TaskApiCall<com.google.android.gms.internal.location.zzaz,globalAndroid.location.Location> {
						public static class: java.lang.Class<com.google.android.gms.location.zzl>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzm extends com.google.android.gms.common.api.internal.TaskApiCall<com.google.android.gms.internal.location.zzaz,com.google.android.gms.location.LocationAvailability> {
						public static class: java.lang.Class<com.google.android.gms.location.zzm>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzn extends com.google.android.gms.common.api.internal.RegisterListenerMethod<com.google.android.gms.internal.location.zzaz,com.google.android.gms.location.LocationCallback> {
						public static class: java.lang.Class<com.google.android.gms.location.zzn>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzo extends com.google.android.gms.common.api.internal.UnregisterListenerMethod<com.google.android.gms.internal.location.zzaz,com.google.android.gms.location.LocationCallback> {
						public static class: java.lang.Class<com.google.android.gms.location.zzo>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzp extends com.google.android.gms.internal.location.zzak {
						public static class: java.lang.Class<com.google.android.gms.location.zzp>;
						public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
						public getInterfaceDescriptor(): string;
						public isBinderAlive(): boolean;
						public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
						public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						public pingBinder(): boolean;
						public asBinder(): globalAndroid.os.IBinder;
						public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
						public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
						public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
						public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzq extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.GeofencingRequest> {
						public static class: java.lang.Class<com.google.android.gms.location.zzq>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzr extends java.lang.Object implements globalAndroid.os.IInterface {
						public static class: java.lang.Class<com.google.android.gms.location.zzr>;
						/**
						 * Constructs a new instance of the com.google.android.gms.location.zzr interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							asBinder(): globalAndroid.os.IBinder;
						});
						public constructor();
						public asBinder(): globalAndroid.os.IBinder;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzs extends com.google.android.gms.internal.location.zzb implements com.google.android.gms.location.zzr {
						public static class: java.lang.Class<com.google.android.gms.location.zzs>;
						public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
						public getInterfaceDescriptor(): string;
						public isBinderAlive(): boolean;
						public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
						public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						public pingBinder(): boolean;
						public asBinder(): globalAndroid.os.IBinder;
						public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
						public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
						public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
						public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
						public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzt extends com.google.android.gms.internal.location.zza implements com.google.android.gms.location.zzr {
						public static class: java.lang.Class<com.google.android.gms.location.zzt>;
						public asBinder(): globalAndroid.os.IBinder;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzu extends java.lang.Object implements globalAndroid.os.IInterface {
						public static class: java.lang.Class<com.google.android.gms.location.zzu>;
						/**
						 * Constructs a new instance of the com.google.android.gms.location.zzu interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							onLocationResult(param0: com.google.android.gms.location.LocationResult): void;
							onLocationAvailability(param0: com.google.android.gms.location.LocationAvailability): void;
							asBinder(): globalAndroid.os.IBinder;
						});
						public constructor();
						public onLocationResult(param0: com.google.android.gms.location.LocationResult): void;
						public onLocationAvailability(param0: com.google.android.gms.location.LocationAvailability): void;
						public asBinder(): globalAndroid.os.IBinder;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export abstract class zzv extends com.google.android.gms.internal.location.zzb implements com.google.android.gms.location.zzu {
						public static class: java.lang.Class<com.google.android.gms.location.zzv>;
						public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
						public constructor();
						public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
						public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
						public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
						public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
						public onLocationResult(param0: com.google.android.gms.location.LocationResult): void;
						public getInterfaceDescriptor(): string;
						public onLocationAvailability(param0: com.google.android.gms.location.LocationAvailability): void;
						public isBinderAlive(): boolean;
						public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						public pingBinder(): boolean;
						public asBinder(): globalAndroid.os.IBinder;
						public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
						public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
						public constructor(param0: string);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzw extends com.google.android.gms.internal.location.zza implements com.google.android.gms.location.zzu {
						public static class: java.lang.Class<com.google.android.gms.location.zzw>;
						public onLocationResult(param0: com.google.android.gms.location.LocationResult): void;
						public onLocationAvailability(param0: com.google.android.gms.location.LocationAvailability): void;
						public asBinder(): globalAndroid.os.IBinder;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzx extends java.lang.Object implements globalAndroid.os.IInterface {
						public static class: java.lang.Class<com.google.android.gms.location.zzx>;
						/**
						 * Constructs a new instance of the com.google.android.gms.location.zzx interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							onLocationChanged(param0: globalAndroid.location.Location): void;
							asBinder(): globalAndroid.os.IBinder;
						});
						public constructor();
						public onLocationChanged(param0: globalAndroid.location.Location): void;
						public asBinder(): globalAndroid.os.IBinder;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export abstract class zzy extends com.google.android.gms.internal.location.zzb implements com.google.android.gms.location.zzx {
						public static class: java.lang.Class<com.google.android.gms.location.zzy>;
						public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
						public constructor();
						public onLocationChanged(param0: globalAndroid.location.Location): void;
						public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
						public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
						public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
						public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
						public getInterfaceDescriptor(): string;
						public isBinderAlive(): boolean;
						public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						public pingBinder(): boolean;
						public asBinder(): globalAndroid.os.IBinder;
						public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
						public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
						public constructor(param0: string);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export class zzz extends com.google.android.gms.internal.location.zza implements com.google.android.gms.location.zzx {
						public static class: java.lang.Class<com.google.android.gms.location.zzz>;
						public onLocationChanged(param0: globalAndroid.location.Location): void;
						public asBinder(): globalAndroid.os.IBinder;
					}
				}
			}
		}
	}
}

//Generics information:
//com.google.android.gms.internal.location.zzbj:1
//com.google.android.gms.location.ActivityRecognition.zza:1
//com.google.android.gms.location.LocationServices.zza:1

