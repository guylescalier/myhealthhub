declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class AccountChangeEvent extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.auth.AccountChangeEvent>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.AccountChangeEvent>;
						public constructor();
						public constructor(param0: number, param1: string, param2: number, param3: number, param4: string);
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public hashCode(): number;
						public equals(param0: any): boolean;
						public getAccountName(): string;
						public getChangeType(): number;
						public getChangeData(): string;
						public getEventIndex(): number;
						public toString(): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class AccountChangeEventsRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.auth.AccountChangeEventsRequest>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.AccountChangeEventsRequest>;
						public constructor();
						public getAccount(): globalAndroid.accounts.Account;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public setAccountName(param0: string): com.google.android.gms.auth.AccountChangeEventsRequest;
						public getAccountName(): string;
						public setEventIndex(param0: number): com.google.android.gms.auth.AccountChangeEventsRequest;
						public getEventIndex(): number;
						public setAccount(param0: globalAndroid.accounts.Account): com.google.android.gms.auth.AccountChangeEventsRequest;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class AccountChangeEventsResponse extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
						public static class: java.lang.Class<com.google.android.gms.auth.AccountChangeEventsResponse>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.AccountChangeEventsResponse>;
						public constructor();
						public getEvents(): java.util.List<com.google.android.gms.auth.AccountChangeEvent>;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public constructor(param0: java.util.List<com.google.android.gms.auth.AccountChangeEvent>);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class CookieUtil extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.auth.CookieUtil>;
						public static getCookieValue(param0: string, param1: string, param2: string, param3: string, param4: java.lang.Boolean, param5: java.lang.Boolean, param6: java.lang.Long): string;
						public static getCookieUrl(param0: string, param1: java.lang.Boolean): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class GoogleAuthException extends java.lang.Exception {
						public static class: java.lang.Class<com.google.android.gms.auth.GoogleAuthException>;
						public constructor();
						public constructor(param0: java.lang.Throwable);
						public constructor(param0: string, param1: java.lang.Throwable);
						public constructor(param0: string, param1: java.lang.Throwable, param2: boolean, param3: boolean);
						public constructor(param0: string);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class GoogleAuthUtil extends com.google.android.gms.auth.zzd {
						public static class: java.lang.Class<com.google.android.gms.auth.GoogleAuthUtil>;
						public static GOOGLE_ACCOUNT_TYPE: string;
						public static WORK_ACCOUNT_TYPE: string;
						public static KEY_SUPPRESS_PROGRESS_SCREEN: string;
						public static CHANGE_TYPE_ACCOUNT_ADDED: number;
						public static CHANGE_TYPE_ACCOUNT_REMOVED: number;
						public static CHANGE_TYPE_ACCOUNT_RENAMED_FROM: number;
						public static CHANGE_TYPE_ACCOUNT_RENAMED_TO: number;
						public static getTokenWithNotification(param0: globalAndroid.content.Context, param1: string, param2: string, param3: globalAndroid.os.Bundle, param4: globalAndroid.content.Intent): string;
						public static clearToken(param0: globalAndroid.content.Context, param1: string): void;
						public static invalidateToken(param0: globalAndroid.content.Context, param1: string): void;
						public static getToken(param0: globalAndroid.content.Context, param1: string, param2: string, param3: globalAndroid.os.Bundle): string;
						public static getAccountChangeEvents(param0: globalAndroid.content.Context, param1: number, param2: string): java.util.List<com.google.android.gms.auth.AccountChangeEvent>;
						public static getTokenWithNotification(param0: globalAndroid.content.Context, param1: string, param2: string, param3: globalAndroid.os.Bundle, param4: string, param5: globalAndroid.os.Bundle): string;
						public static getTokenWithNotification(param0: globalAndroid.content.Context, param1: globalAndroid.accounts.Account, param2: string, param3: globalAndroid.os.Bundle): string;
						public static getTokenWithNotification(param0: globalAndroid.content.Context, param1: globalAndroid.accounts.Account, param2: string, param3: globalAndroid.os.Bundle, param4: globalAndroid.content.Intent): string;
						public static requestGoogleAccountsAccess(param0: globalAndroid.content.Context): java.lang.Boolean;
						public static getTokenWithNotification(param0: globalAndroid.content.Context, param1: globalAndroid.accounts.Account, param2: string, param3: globalAndroid.os.Bundle, param4: string, param5: globalAndroid.os.Bundle): string;
						public static getToken(param0: globalAndroid.content.Context, param1: globalAndroid.accounts.Account, param2: string): string;
						public static getAccountId(param0: globalAndroid.content.Context, param1: string): string;
						public static getTokenWithNotification(param0: globalAndroid.content.Context, param1: string, param2: string, param3: globalAndroid.os.Bundle): string;
						public static getToken(param0: globalAndroid.content.Context, param1: globalAndroid.accounts.Account, param2: string, param3: globalAndroid.os.Bundle): string;
						public static removeAccount(param0: globalAndroid.content.Context, param1: globalAndroid.accounts.Account): globalAndroid.os.Bundle;
						public static getToken(param0: globalAndroid.content.Context, param1: string, param2: string): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class GooglePlayServicesAvailabilityException extends com.google.android.gms.auth.UserRecoverableAuthException {
						public static class: java.lang.Class<com.google.android.gms.auth.GooglePlayServicesAvailabilityException>;
						public getConnectionStatusCode(): number;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class TokenData extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
						public static class: java.lang.Class<com.google.android.gms.auth.TokenData>;
						public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.TokenData>;
						public describeContents(): number;
						public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						public equals(param0: any): boolean;
						public hashCode(): number;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class UserRecoverableAuthException extends com.google.android.gms.auth.GoogleAuthException {
						public static class: java.lang.Class<com.google.android.gms.auth.UserRecoverableAuthException>;
						public constructor();
						public constructor(param0: java.lang.Throwable);
						public getIntent(): globalAndroid.content.Intent;
						public constructor(param0: string, param1: java.lang.Throwable);
						public constructor(param0: string, param1: java.lang.Throwable, param2: boolean, param3: boolean);
						public constructor(param0: string, param1: globalAndroid.content.Intent);
						public constructor(param0: string);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class UserRecoverableNotifiedException extends com.google.android.gms.auth.GoogleAuthException {
						public static class: java.lang.Class<com.google.android.gms.auth.UserRecoverableNotifiedException>;
						public constructor();
						public constructor(param0: java.lang.Throwable);
						public constructor(param0: string, param1: java.lang.Throwable);
						public constructor(param0: string, param1: java.lang.Throwable, param2: boolean, param3: boolean);
						public constructor(param0: string);
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export class WorkAccount extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.auth.account.WorkAccount>;
							public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
							public static WorkAccountApi: com.google.android.gms.auth.account.WorkAccountApi;
							public static getClient(param0: globalAndroid.app.Activity): com.google.android.gms.auth.account.WorkAccountClient;
							public static getClient(param0: globalAndroid.content.Context): com.google.android.gms.auth.account.WorkAccountClient;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export class WorkAccountApi extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.auth.account.WorkAccountApi>;
							/**
							 * Constructs a new instance of the com.google.android.gms.auth.account.WorkAccountApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								setWorkAuthenticatorEnabled(param0: com.google.android.gms.common.api.GoogleApiClient, param1: boolean): void;
								setWorkAuthenticatorEnabledWithResult(param0: com.google.android.gms.common.api.GoogleApiClient, param1: boolean): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Result>;
								addWorkAccount(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult>;
								removeWorkAccount(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.accounts.Account): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Result>;
							});
							public constructor();
							public setWorkAuthenticatorEnabled(param0: com.google.android.gms.common.api.GoogleApiClient, param1: boolean): void;
							public removeWorkAccount(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.accounts.Account): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Result>;
							public setWorkAuthenticatorEnabledWithResult(param0: com.google.android.gms.common.api.GoogleApiClient, param1: boolean): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Result>;
							public addWorkAccount(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult>;
						}
						export module WorkAccountApi {
							export class AddAccountResult extends java.lang.Object implements com.google.android.gms.common.api.Result {
								public static class: java.lang.Class<com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult>;
								/**
								 * Constructs a new instance of the com.google.android.gms.auth.account.WorkAccountApi$AddAccountResult interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
								 */
								public constructor(implementation: {
									getAccount(): globalAndroid.accounts.Account;
									getStatus(): com.google.android.gms.common.api.Status;
								});
								public constructor();
								public getAccount(): globalAndroid.accounts.Account;
								public getStatus(): com.google.android.gms.common.api.Status;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export class WorkAccountClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
							public static class: java.lang.Class<com.google.android.gms.auth.account.WorkAccountClient>;
							public setWorkAuthenticatorEnabled(param0: boolean): com.google.android.gms.tasks.Task<java.lang.Void>;
							public removeWorkAccount(param0: globalAndroid.accounts.Account): com.google.android.gms.tasks.Task<java.lang.Void>;
							public addWorkAccount(param0: string): com.google.android.gms.tasks.Task<globalAndroid.accounts.Account>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export class zza extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.auth.account.zza>;
							/**
							 * Constructs a new instance of the com.google.android.gms.auth.account.zza interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zzc(param0: globalAndroid.accounts.Account): void;
								zza(param0: boolean): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export abstract class zzb extends com.google.android.gms.internal.auth.zzb implements com.google.android.gms.auth.account.zza {
							public static class: java.lang.Class<com.google.android.gms.auth.account.zzb>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export class zzc extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.auth.account.zzc>;
							/**
							 * Constructs a new instance of the com.google.android.gms.auth.account.zzc interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zzb(param0: boolean): void;
								zza(param0: any /* com.google.android.gms.auth.account.zza*/, param1: string): void;
								zza(param0: any /* com.google.android.gms.auth.account.zza*/, param1: globalAndroid.accounts.Account): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export abstract class zzd extends com.google.android.gms.internal.auth.zzb implements com.google.android.gms.auth.account.zzc {
							public static class: java.lang.Class<com.google.android.gms.auth.account.zzd>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export class zze extends com.google.android.gms.internal.auth.zza implements com.google.android.gms.auth.account.zzc {
							public static class: java.lang.Class<com.google.android.gms.auth.account.zze>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export class zzf extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.auth.zzr,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
							public static class: java.lang.Class<com.google.android.gms.auth.account.zzf>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module account {
						export class zzg extends com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult,globalAndroid.accounts.Account> {
							public static class: java.lang.Class<com.google.android.gms.auth.account.zzg>;
							public convert(param0: any): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export class AuthProxy extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.auth.api.AuthProxy>;
							public static API: com.google.android.gms.common.api.Api<com.google.android.gms.auth.api.AuthProxyOptions>;
							public static ProxyApi: com.google.android.gms.auth.api.proxy.ProxyApi;
							public constructor();
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export class AuthProxyOptions extends java.lang.Object implements com.google.android.gms.common.api.Api.ApiOptions.Optional {
							public static class: java.lang.Class<com.google.android.gms.auth.api.AuthProxyOptions>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class AccountTransfer extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.AccountTransfer>;
								public static ACTION_START_ACCOUNT_EXPORT: string;
								public static ACTION_ACCOUNT_IMPORT_DATA_AVAILABLE: string;
								public static ACTION_ACCOUNT_EXPORT_DATA_AVAILABLE: string;
								public static KEY_EXTRA_ACCOUNT_TYPE: string;
								public static getAccountTransferClient(param0: globalAndroid.app.Activity): com.google.android.gms.auth.api.accounttransfer.AccountTransferClient;
								public static getAccountTransferClient(param0: globalAndroid.content.Context): com.google.android.gms.auth.api.accounttransfer.AccountTransferClient;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class AccountTransferClient extends java.lang.Object /* com.google.android.gms.common.api.GoogleApi<com.google.android.gms.auth.api.accounttransfer.zzn>*/ {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.AccountTransferClient>;
								public notifyCompletion(param0: string, param1: number): com.google.android.gms.tasks.Task<java.lang.Void>;
								public getDeviceMetaData(param0: string): com.google.android.gms.tasks.Task<com.google.android.gms.auth.api.accounttransfer.DeviceMetaData>;
								public sendData(param0: string, param1: native.Array<number>): com.google.android.gms.tasks.Task<java.lang.Void>;
								public retrieveData(param0: string): com.google.android.gms.tasks.Task<native.Array<number>>;
								public showUserChallenge(param0: string, param1: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
							}
							export module AccountTransferClient {
								export class zza<T>  extends java.lang.Object /* com.google.android.gms.internal.auth.zzs*/ {
									public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zza<any>>;
									public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
									public constructor();
									public asBinder(): globalAndroid.os.IBinder;
									public isBinderAlive(): boolean;
									public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
									public getInterfaceDescriptor(): string;
									public constructor(param0: any /* com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzb<any>*/);
									public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public constructor(param0: string);
									public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
									public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
									public pingBinder(): boolean;
									public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
									public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
									public onFailure(param0: com.google.android.gms.common.api.Status): void;
								}
								export abstract class zzb<T>  extends java.lang.Object /* com.google.android.gms.common.api.internal.TaskApiCall<com.google.android.gms.internal.auth.zzu,any>*/ {
									public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzb<any>>;
									public setResult(param0: any): void;
								}
								export abstract class zzc extends com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzb<java.lang.Void> {
									public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzc>;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class AccountTransferException extends com.google.android.gms.common.api.ApiException {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.AccountTransferException>;
								public constructor(param0: com.google.android.gms.common.api.Status);
								public constructor(param0: string, param1: java.lang.Throwable, param2: boolean, param3: boolean);
								public constructor(param0: java.lang.Throwable);
								public constructor(param0: string, param1: java.lang.Throwable);
								public constructor();
								public constructor(param0: string);
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class AccountTransferStatusCodes extends com.google.android.gms.common.api.CommonStatusCodes {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.AccountTransferStatusCodes>;
								public static NOT_ALLOWED_SECURITY: number;
								public static NO_DATA_AVAILABLE: number;
								public static INVALID_REQUEST: number;
								public static CHALLENGE_NOT_ALLOWED: number;
								public static SESSION_INACTIVE: number;
								public static getStatusCodeString(param0: number): string;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class AuthenticatorTransferCompletionStatus extends java.lang.Object implements java.lang.annotation.Annotation {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.AuthenticatorTransferCompletionStatus>;
								/**
								 * Constructs a new instance of the com.google.android.gms.auth.api.accounttransfer.AuthenticatorTransferCompletionStatus interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
								 */
								public constructor(implementation: {
									equals(param0: any): boolean;
									hashCode(): number;
									toString(): string;
									annotationType(): java.lang.Class<any>;
								});
								public constructor();
								public static COMPLETED_SUCCESS: number;
								public static COMPLETED_FAILURE: number;
								public toString(): string;
								public equals(param0: any): boolean;
								public annotationType(): java.lang.Class<any>;
								public hashCode(): number;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class DeviceMetaData extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.DeviceMetaData>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.DeviceMetaData>;
								public isChallengeAllowed(): boolean;
								public isLockScreenSolved(): boolean;
								public getMinAgeOfLockScreen(): number;
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zza extends java.lang.Object /* com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.auth.zzu,com.google.android.gms.auth.api.accounttransfer.zzn>*/ {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zza>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzb extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzb>;
								/**
								 * Constructs a new instance of the com.google.android.gms.auth.api.accounttransfer.zzb interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
								 */
								public constructor(implementation: {
								});
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzc extends java.lang.Object /* com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.auth.zzu,com.google.android.gms.auth.api.accounttransfer.zzn>*/ {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzc>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzd extends com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzc {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzd>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zze extends com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzb<native.Array<number>> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zze>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzf extends com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zza<native.Array<number>> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzf>;
								public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
								public onFailure(param0: com.google.android.gms.common.api.Status): void;
								public isBinderAlive(): boolean;
								public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
								public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
								public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
								public getInterfaceDescriptor(): string;
								public pingBinder(): boolean;
								public asBinder(): globalAndroid.os.IBinder;
								public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
								public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
								public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzg extends com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzb<com.google.android.gms.auth.api.accounttransfer.DeviceMetaData> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzg>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzh extends com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zza<com.google.android.gms.auth.api.accounttransfer.DeviceMetaData> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzh>;
								public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
								public onFailure(param0: com.google.android.gms.common.api.Status): void;
								public isBinderAlive(): boolean;
								public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
								public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
								public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
								public getInterfaceDescriptor(): string;
								public pingBinder(): boolean;
								public asBinder(): globalAndroid.os.IBinder;
								public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
								public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
								public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzi extends com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzc {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzi>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzj extends com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzc {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzj>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzk extends com.google.android.gms.internal.auth.zzs {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzk>;
								public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
								public onFailure(param0: com.google.android.gms.common.api.Status): void;
								public isBinderAlive(): boolean;
								public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
								public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
								public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
								public getInterfaceDescriptor(): string;
								public pingBinder(): boolean;
								public asBinder(): globalAndroid.os.IBinder;
								public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
								public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
								public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzl extends com.google.android.gms.internal.auth.zzaz {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzl>;
								public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.zzl>*/;
								public addConcreteTypeArrayInternal(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>, param1: string, param2: java.util.ArrayList<any>): void;
								public isFieldSet(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>): boolean;
								public constructor();
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public getFieldValue(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>): any;
								public addConcreteTypeInternal(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>, param1: string, param2: com.google.android.gms.common.server.response.FastJsonResponse): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzm extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.zzl>*/ {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzm>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzn extends java.lang.Object implements com.google.android.gms.common.api.Api.ApiOptions.Optional {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzn>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzo extends com.google.android.gms.internal.auth.zzaz {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzo>;
								public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.zzo>*/;
								public setStringsInternal(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any,any>, param1: string, param2: java.util.ArrayList<string>): void;
								public isFieldSet(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>): boolean;
								public constructor();
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public getFieldValue(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>): any;
								public getFieldMappings(): java.util.Map<string,com.google.android.gms.common.server.response.FastJsonResponse.Field<any,any>>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzp extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.zzo>*/ {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzp>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzq extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzq>;
								/**
								 * Constructs a new instance of the com.google.android.gms.auth.api.accounttransfer.zzq interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
								 */
								public constructor(implementation: {
								});
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzr extends com.google.android.gms.internal.auth.zzaz {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzr>;
								public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.zzr>*/;
								public isFieldSet(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>): boolean;
								public constructor();
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public getFieldValue(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>): any;
								public addConcreteTypeInternal(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>, param1: string, param2: com.google.android.gms.common.server.response.FastJsonResponse): void;
								public setStringInternal(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any,any>, param1: string, param2: string): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzs extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.zzr>*/ {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzs>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzt extends com.google.android.gms.internal.auth.zzaz {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzt>;
								public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.zzt>*/;
								public isFieldSet(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>): boolean;
								public setIntegerInternal(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any,any>, param1: string, param2: number): void;
								public constructor();
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public getFieldValue(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any, any>): any;
								public setDecodedBytesInternal(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any,any>, param1: string, param2: native.Array<number>): void;
								public setStringInternal(param0: com.google.android.gms.common.server.response.FastJsonResponse.Field<any,any>, param1: string, param2: string): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzu extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.zzt>*/ {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzu>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module accounttransfer {
							export class zzv extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.accounttransfer.DeviceMetaData> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.accounttransfer.zzv>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module proxy {
							export class AuthApiStatusCodes extends com.google.android.gms.common.api.CommonStatusCodes {
								public static class: java.lang.Class<com.google.android.gms.auth.api.proxy.AuthApiStatusCodes>;
								public static AUTH_API_INVALID_CREDENTIALS: number;
								public static AUTH_API_ACCESS_FORBIDDEN: number;
								public static AUTH_API_CLIENT_ERROR: number;
								public static AUTH_API_SERVER_ERROR: number;
								public static AUTH_TOKEN_ERROR: number;
								public static AUTH_URL_RESOLUTION: number;
								public static AUTH_APP_CERT_ERROR: number;
								public static getStatusCodeString(param0: number): string;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module proxy {
							export class ProxyApi extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.auth.api.proxy.ProxyApi>;
								/**
								 * Constructs a new instance of the com.google.android.gms.auth.api.proxy.ProxyApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
								 */
								public constructor(implementation: {
									performProxyRequest(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.proxy.ProxyRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult>;
									getSpatulaHeader(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.api.proxy.ProxyApi.SpatulaHeaderResult>;
								});
								public constructor();
								public performProxyRequest(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.proxy.ProxyRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult>;
								public getSpatulaHeader(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.api.proxy.ProxyApi.SpatulaHeaderResult>;
							}
							export module ProxyApi {
								export class ProxyResult extends java.lang.Object implements com.google.android.gms.common.api.Result {
									public static class: java.lang.Class<com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult>;
									/**
									 * Constructs a new instance of the com.google.android.gms.auth.api.proxy.ProxyApi$ProxyResult interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
									 */
									public constructor(implementation: {
										getResponse(): com.google.android.gms.auth.api.proxy.ProxyResponse;
										getStatus(): com.google.android.gms.common.api.Status;
									});
									public constructor();
									public getStatus(): com.google.android.gms.common.api.Status;
									public getResponse(): com.google.android.gms.auth.api.proxy.ProxyResponse;
								}
								export class SpatulaHeaderResult extends java.lang.Object implements com.google.android.gms.common.api.Result {
									public static class: java.lang.Class<com.google.android.gms.auth.api.proxy.ProxyApi.SpatulaHeaderResult>;
									/**
									 * Constructs a new instance of the com.google.android.gms.auth.api.proxy.ProxyApi$SpatulaHeaderResult interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
									 */
									public constructor(implementation: {
										getSpatulaHeader(): string;
										getStatus(): com.google.android.gms.common.api.Status;
									});
									public constructor();
									public getStatus(): com.google.android.gms.common.api.Status;
									public getSpatulaHeader(): string;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module proxy {
							export class ProxyRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
								public static class: java.lang.Class<com.google.android.gms.auth.api.proxy.ProxyRequest>;
								public static VERSION_CODE: number;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.proxy.ProxyRequest>;
								public static HTTP_METHOD_GET: number;
								public static HTTP_METHOD_POST: number;
								public static HTTP_METHOD_PUT: number;
								public static HTTP_METHOD_DELETE: number;
								public static HTTP_METHOD_HEAD: number;
								public static HTTP_METHOD_OPTIONS: number;
								public static HTTP_METHOD_TRACE: number;
								public static HTTP_METHOD_PATCH: number;
								public static LAST_CODE: number;
								public url: string;
								public httpMethod: number;
								public timeoutMillis: number;
								public body: native.Array<number>;
								public toString(): string;
								public getHeaderMap(): java.util.Map<string,string>;
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							}
							export module ProxyRequest {
								export class Builder extends java.lang.Object {
									public static class: java.lang.Class<com.google.android.gms.auth.api.proxy.ProxyRequest.Builder>;
									public constructor(param0: string);
									public build(): com.google.android.gms.auth.api.proxy.ProxyRequest;
									public putHeader(param0: string, param1: string): com.google.android.gms.auth.api.proxy.ProxyRequest.Builder;
									public setHttpMethod(param0: number): com.google.android.gms.auth.api.proxy.ProxyRequest.Builder;
									public setTimeoutMillis(param0: number): com.google.android.gms.auth.api.proxy.ProxyRequest.Builder;
									public setBody(param0: native.Array<number>): com.google.android.gms.auth.api.proxy.ProxyRequest.Builder;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module proxy {
							export class ProxyResponse extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
								public static class: java.lang.Class<com.google.android.gms.auth.api.proxy.ProxyResponse>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.proxy.ProxyResponse>;
								public static STATUS_CODE_NO_CONNECTION: number;
								public googlePlayServicesStatusCode: number;
								public recoveryAction: globalAndroid.app.PendingIntent;
								public statusCode: number;
								public body: native.Array<number>;
								public static createErrorProxyResponse(param0: number, param1: globalAndroid.app.PendingIntent, param2: number, param3: java.util.Map<string,string>, param4: native.Array<number>): com.google.android.gms.auth.api.proxy.ProxyResponse;
								public constructor(param0: number, param1: java.util.Map<string,string>, param2: native.Array<number>);
								public getHeaders(): java.util.Map<string,string>;
								public constructor();
								public constructor(param0: number, param1: globalAndroid.app.PendingIntent, param2: number, param3: globalAndroid.os.Bundle, param4: native.Array<number>);
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module proxy {
							export class zza extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.proxy.ProxyRequest> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.proxy.zza>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export module proxy {
							export class zzb extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.api.proxy.ProxyResponse> {
								public static class: java.lang.Class<com.google.android.gms.auth.api.proxy.zzb>;
								public createFromParcel(param0: globalAndroid.os.Parcel): any;
								public newArray(param0: number): native.Array<any>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export module api {
						export class zza extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.auth.zzak,com.google.android.gms.auth.api.AuthProxyOptions> {
							public static class: java.lang.Class<com.google.android.gms.auth.api.zza>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zza extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.AccountChangeEvent> {
						public static class: java.lang.Class<com.google.android.gms.auth.zza>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zzb extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.AccountChangeEventsRequest> {
						public static class: java.lang.Class<com.google.android.gms.auth.zzb>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zzc extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.AccountChangeEventsResponse> {
						public static class: java.lang.Class<com.google.android.gms.auth.zzc>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zzd extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.auth.zzd>;
						public static GOOGLE_ACCOUNT_TYPE: string;
						public static WORK_ACCOUNT_TYPE: string;
						public static KEY_SUPPRESS_PROGRESS_SCREEN: string;
						public static KEY_CALLER_UID: string;
						public static KEY_ANDROID_PACKAGE_NAME: string;
						public static CHANGE_TYPE_ACCOUNT_ADDED: number;
						public static CHANGE_TYPE_ACCOUNT_REMOVED: number;
						public static CHANGE_TYPE_ACCOUNT_RENAMED_FROM: number;
						public static CHANGE_TYPE_ACCOUNT_RENAMED_TO: number;
						public static getToken(param0: globalAndroid.content.Context, param1: globalAndroid.accounts.Account, param2: string): string;
						public static getAccountId(param0: globalAndroid.content.Context, param1: string): string;
						public static clearToken(param0: globalAndroid.content.Context, param1: string): void;
						public static invalidateToken(param0: globalAndroid.content.Context, param1: string): void;
						public static getToken(param0: globalAndroid.content.Context, param1: string, param2: string, param3: globalAndroid.os.Bundle): string;
						public static getAccountChangeEvents(param0: globalAndroid.content.Context, param1: number, param2: string): java.util.List<com.google.android.gms.auth.AccountChangeEvent>;
						public static getToken(param0: globalAndroid.content.Context, param1: globalAndroid.accounts.Account, param2: string, param3: globalAndroid.os.Bundle): string;
						public static removeAccount(param0: globalAndroid.content.Context, param1: globalAndroid.accounts.Account): globalAndroid.os.Bundle;
						public static getToken(param0: globalAndroid.content.Context, param1: string, param2: string): string;
						public static requestGoogleAccountsAccess(param0: globalAndroid.content.Context): java.lang.Boolean;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zze extends com.google.android.gms.auth.zzj<com.google.android.gms.auth.TokenData> {
						public static class: java.lang.Class<com.google.android.gms.auth.zze>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zzf extends com.google.android.gms.auth.zzj<java.lang.Void> {
						public static class: java.lang.Class<com.google.android.gms.auth.zzf>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zzg extends com.google.android.gms.auth.zzj<java.util.List<com.google.android.gms.auth.AccountChangeEvent>> {
						public static class: java.lang.Class<com.google.android.gms.auth.zzg>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zzh extends com.google.android.gms.auth.zzj<globalAndroid.os.Bundle> {
						public static class: java.lang.Class<com.google.android.gms.auth.zzh>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zzi extends com.google.android.gms.auth.zzj<java.lang.Boolean> {
						public static class: java.lang.Class<com.google.android.gms.auth.zzi>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zzj<T>  extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.auth.zzj<any>>;
						/**
						 * Constructs a new instance of the com.google.android.gms.auth.zzj<any> interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							zzb(param0: globalAndroid.os.IBinder): T;
						});
						public constructor();
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module auth {
					export class zzk extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.auth.TokenData> {
						public static class: java.lang.Class<com.google.android.gms.auth.zzk>;
						public createFromParcel(param0: globalAndroid.os.Parcel): any;
						public constructor();
						public newArray(param0: number): native.Array<any>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zza extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zza>;
							public constructor(param0: globalAndroid.os.IBinder, param1: string);
							public asBinder(): globalAndroid.os.IBinder;
							public obtainAndWriteInterfaceToken(): globalAndroid.os.Parcel;
							public transactAndReadException(param0: number, param1: globalAndroid.os.Parcel): globalAndroid.os.Parcel;
							public transactAndReadExceptionReturnVoid(param0: number, param1: globalAndroid.os.Parcel): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzaa extends com.google.android.gms.internal.auth.zza implements com.google.android.gms.internal.auth.zzz {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzaa>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzab extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzab>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzab>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: string, param1: number);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzac extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzab>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzac>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzad extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzad>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzad>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: string);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzae extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzad>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzae>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzaf extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzaf>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzaf>*/;
							public constructor();
							public constructor(param0: string, param1: native.Array<number>);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzag extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzaf>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzag>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzah extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzah>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzah>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: string, param1: globalAndroid.app.PendingIntent);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzai extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzah>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzai>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzaj extends com.google.android.gms.internal.auth.zzam {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzaj>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzak extends java.lang.Object /* com.google.android.gms.common.internal.GmsClient<com.google.android.gms.internal.auth.zzan>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzak>;
							public getStartServiceAction(): string;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param7: string);
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getGetServiceRequestExtraArgs(): globalAndroid.os.Bundle;
							public requiresAccount(): boolean;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.ClientSettings, param3: com.google.android.gms.auth.api.AuthProxyOptions, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param4: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param5: string);
							public isConnecting(): boolean;
							public getMinApkVersion(): number;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener);
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzal extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzal>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.auth.zzal interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.auth.api.proxy.ProxyResponse): void;
								zzb(param0: string): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export abstract class zzam extends com.google.android.gms.internal.auth.zzb implements com.google.android.gms.internal.auth.zzal {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzam>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzan extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzan>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.auth.zzan interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: any /* com.google.android.gms.internal.auth.zzal*/, param1: com.google.android.gms.auth.api.proxy.ProxyRequest): void;
								zza(param0: any /* com.google.android.gms.internal.auth.zzal*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzao extends com.google.android.gms.internal.auth.zza implements com.google.android.gms.internal.auth.zzan {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzao>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export abstract class zzap extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult,com.google.android.gms.internal.auth.zzak>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzap>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export abstract class zzaq extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<com.google.android.gms.auth.api.proxy.ProxyApi.SpatulaHeaderResult,com.google.android.gms.internal.auth.zzak>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzaq>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzar extends java.lang.Object implements com.google.android.gms.auth.api.proxy.ProxyApi {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzar>;
							public constructor();
							public getSpatulaHeader(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.api.proxy.ProxyApi.SpatulaHeaderResult>;
							public performProxyRequest(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.auth.api.proxy.ProxyRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzas extends com.google.android.gms.internal.auth.zzap {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzas>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzat extends com.google.android.gms.internal.auth.zzaj {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzat>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzau extends com.google.android.gms.internal.auth.zzaq {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzau>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzav extends com.google.android.gms.internal.auth.zzaj {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzav>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzaw extends java.lang.Object implements com.google.android.gms.auth.api.proxy.ProxyApi.ProxyResult {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzaw>;
							public getResponse(): com.google.android.gms.auth.api.proxy.ProxyResponse;
							public getStatus(): com.google.android.gms.common.api.Status;
							public constructor(param0: com.google.android.gms.common.api.Status);
							public constructor(param0: com.google.android.gms.auth.api.proxy.ProxyResponse);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzax extends java.lang.Object implements com.google.android.gms.auth.api.proxy.ProxyApi.SpatulaHeaderResult {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzax>;
							public getStatus(): com.google.android.gms.common.api.Status;
							public getSpatulaHeader(): string;
							public constructor(param0: string);
							public constructor(param0: com.google.android.gms.common.api.Status);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzay {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzay>;
							public static values(): any /* native.Array<com.google.android.gms.internal.auth.zzay>*/;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export abstract class zzaz extends com.google.android.gms.common.server.response.FastSafeParcelableJsonResponse {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzaz>;
							public constructor();
							public toByteArray(): native.Array<number>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzb extends globalAndroid.os.Binder implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzb>;
							public constructor();
							public onTransact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzc extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzc>;
							public static writeBoolean(param0: globalAndroid.os.Parcel, param1: boolean): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzd extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzd>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.auth.zzd interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
							});
							public constructor();
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zze extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zze>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.auth.zze interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: string, param1: globalAndroid.os.Bundle): globalAndroid.os.Bundle;
								zza(param0: com.google.android.gms.auth.AccountChangeEventsRequest): com.google.android.gms.auth.AccountChangeEventsResponse;
								zza(param0: globalAndroid.accounts.Account, param1: string, param2: globalAndroid.os.Bundle): globalAndroid.os.Bundle;
								zza(param0: globalAndroid.accounts.Account): globalAndroid.os.Bundle;
								zza(param0: string): globalAndroid.os.Bundle;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export abstract class zzf extends com.google.android.gms.internal.auth.zzb implements com.google.android.gms.internal.auth.zze {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzf>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzg extends com.google.android.gms.internal.auth.zza implements com.google.android.gms.internal.auth.zze {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzg>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzh extends java.lang.Object implements com.google.android.gms.auth.account.WorkAccountApi {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzh>;
							public constructor();
							public setWorkAuthenticatorEnabled(param0: com.google.android.gms.common.api.GoogleApiClient, param1: boolean): void;
							public removeWorkAccount(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.accounts.Account): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Result>;
							public setWorkAuthenticatorEnabledWithResult(param0: com.google.android.gms.common.api.GoogleApiClient, param1: boolean): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Result>;
							public addWorkAccount(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzi extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<com.google.android.gms.common.api.Result,com.google.android.gms.internal.auth.zzr>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzi>;
							public setResult(param0: any): void;
							public createFailedResult(param0: com.google.android.gms.common.api.Status): com.google.android.gms.common.api.Result;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public createFailedResult(param0: com.google.android.gms.common.api.Status): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzj extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult,com.google.android.gms.internal.auth.zzr>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzj>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzk extends com.google.android.gms.internal.auth.zzn {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzk>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzl extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<com.google.android.gms.common.api.Result,com.google.android.gms.internal.auth.zzr>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzl>;
							public setResult(param0: any): void;
							public createFailedResult(param0: com.google.android.gms.common.api.Status): com.google.android.gms.common.api.Result;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public createFailedResult(param0: com.google.android.gms.common.api.Status): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzm extends com.google.android.gms.internal.auth.zzn {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzm>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzn extends com.google.android.gms.auth.account.zzb {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzn>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzo extends java.lang.Object implements com.google.android.gms.auth.account.WorkAccountApi.AddAccountResult {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzo>;
							public constructor(param0: com.google.android.gms.common.api.Status, param1: globalAndroid.accounts.Account);
							public getStatus(): com.google.android.gms.common.api.Status;
							public getAccount(): globalAndroid.accounts.Account;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzp extends java.lang.Object implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzp>;
							public getStatus(): com.google.android.gms.common.api.Status;
							public constructor(param0: com.google.android.gms.common.api.Status);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzq extends java.lang.Object implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzq>;
							public getStatus(): com.google.android.gms.common.api.Status;
							public constructor(param0: com.google.android.gms.common.api.Status);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzr extends java.lang.Object /* com.google.android.gms.common.internal.GmsClient<com.google.android.gms.auth.account.zzc>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzr>;
							public getStartServiceAction(): string;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param7: string);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.ClientSettings, param3: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param4: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public getConnectionHint(): globalAndroid.os.Bundle;
							public requiresAccount(): boolean;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param4: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param5: string);
							public isConnecting(): boolean;
							public getMinApkVersion(): number;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener);
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzs extends com.google.android.gms.internal.auth.zzy {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzs>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public onFailure(param0: com.google.android.gms.common.api.Status): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzt extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzt>;
							public constructor();
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzu extends java.lang.Object /* com.google.android.gms.common.internal.GmsClient<com.google.android.gms.internal.auth.zzz>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzu>;
							public getStartServiceAction(): string;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param7: string);
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getGetServiceRequestExtraArgs(): globalAndroid.os.Bundle;
							public requiresAccount(): boolean;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param4: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param5: string);
							public isConnecting(): boolean;
							public getMinApkVersion(): number;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener);
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.ClientSettings, param3: any /* com.google.android.gms.auth.api.accounttransfer.zzn*/, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzv extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzv>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzv>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: string);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzw extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.auth.zzv>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzw>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzx extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzx>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.auth.zzx interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zzb(param0: com.google.android.gms.common.api.Status): void;
								zza(param0: com.google.android.gms.common.api.Status, param1: any /* com.google.android.gms.auth.api.accounttransfer.zzt*/): void;
								zza(param0: com.google.android.gms.common.api.Status, param1: any /* com.google.android.gms.auth.api.accounttransfer.zzl*/): void;
								zzd(): void;
								onFailure(param0: com.google.android.gms.common.api.Status): void;
								zza(param0: native.Array<number>): void;
								zza(param0: com.google.android.gms.auth.api.accounttransfer.DeviceMetaData): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
							public onFailure(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export abstract class zzy extends com.google.android.gms.internal.auth.zzb implements com.google.android.gms.internal.auth.zzx {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzy>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public onFailure(param0: com.google.android.gms.common.api.Status): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module auth {
						export class zzz extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.auth.zzz>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.auth.zzz interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: any /* com.google.android.gms.internal.auth.zzx*/, param1: any /* com.google.android.gms.internal.auth.zzaf*/): void;
								zza(param0: any /* com.google.android.gms.internal.auth.zzx*/, param1: any /* com.google.android.gms.internal.auth.zzad*/): void;
								zza(param0: any /* com.google.android.gms.internal.auth.zzx*/, param1: any /* com.google.android.gms.internal.auth.zzv*/): void;
								zza(param0: any /* com.google.android.gms.internal.auth.zzx*/, param1: any /* com.google.android.gms.internal.auth.zzah*/): void;
								zza(param0: any /* com.google.android.gms.internal.auth.zzx*/, param1: any /* com.google.android.gms.internal.auth.zzab*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

//Generics information:
//com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zza:1
//com.google.android.gms.auth.api.accounttransfer.AccountTransferClient.zzb:1
//com.google.android.gms.auth.zzj:1

