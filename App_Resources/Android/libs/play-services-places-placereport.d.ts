declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export module places {
						export class PlaceReport extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
							public static class: java.lang.Class<com.google.android.gms.location.places.PlaceReport>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.places.PlaceReport>;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public static create(param0: string, param1: string): com.google.android.gms.location.places.PlaceReport;
							public describeContents(): number;
							public toString(): string;
							public equals(param0: any): boolean;
							public getTag(): string;
							public getPlaceId(): string;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module location {
					export module places {
						export class zza extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.location.places.PlaceReport> {
							public static class: java.lang.Class<com.google.android.gms.location.places.zza>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

//Generics information:

