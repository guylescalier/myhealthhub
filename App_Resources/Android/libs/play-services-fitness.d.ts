declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class BleApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.BleApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.fitness.BleApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							startBleScan(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.StartBleScanRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							stopBleScan(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.BleScanCallback): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							claimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							claimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							unclaimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							unclaimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							listClaimedBleDevices(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.BleDevicesResult>;
						});
						public constructor();
						public claimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public unclaimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public unclaimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public claimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public startBleScan(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.StartBleScanRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public listClaimedBleDevices(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.BleDevicesResult>;
						public stopBleScan(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.BleScanCallback): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class BleClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.fitness.FitnessOptions> {
						public static class: java.lang.Class<com.google.android.gms.fitness.BleClient>;
						public claimBleDevice(param0: string): com.google.android.gms.tasks.Task<java.lang.Void>;
						public unclaimBleDevice(param0: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.tasks.Task<java.lang.Void>;
						public stopBleScan(param0: com.google.android.gms.fitness.request.BleScanCallback): com.google.android.gms.tasks.Task<java.lang.Boolean>;
						public claimBleDevice(param0: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.tasks.Task<java.lang.Void>;
						public unclaimBleDevice(param0: string): com.google.android.gms.tasks.Task<java.lang.Void>;
						public startBleScan(param0: java.util.List<com.google.android.gms.fitness.data.DataType>, param1: number, param2: com.google.android.gms.fitness.request.BleScanCallback): com.google.android.gms.tasks.Task<java.lang.Void>;
						public listClaimedBleDevices(): com.google.android.gms.tasks.Task<java.util.List<com.google.android.gms.fitness.data.BleDevice>>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class ConfigApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.ConfigApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.fitness.ConfigApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							createCustomDataType(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataTypeCreateRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataTypeResult>;
							readDataType(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataTypeResult>;
							disableFit(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						});
						public constructor();
						public readDataType(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataTypeResult>;
						public disableFit(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public createCustomDataType(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataTypeCreateRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataTypeResult>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class ConfigClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.fitness.FitnessOptions> {
						public static class: java.lang.Class<com.google.android.gms.fitness.ConfigClient>;
						public disableFit(): com.google.android.gms.tasks.Task<java.lang.Void>;
						public readDataType(param0: string): com.google.android.gms.tasks.Task<com.google.android.gms.fitness.data.DataType>;
						public createCustomDataType(param0: com.google.android.gms.fitness.request.DataTypeCreateRequest): com.google.android.gms.tasks.Task<com.google.android.gms.fitness.data.DataType>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class Fitness extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.Fitness>;
						public static API: java.lang.Void;
						public static SENSORS_API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
						public static SensorsApi: com.google.android.gms.fitness.SensorsApi;
						public static RECORDING_API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
						public static RecordingApi: com.google.android.gms.fitness.RecordingApi;
						public static SESSIONS_API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
						public static SessionsApi: com.google.android.gms.fitness.SessionsApi;
						public static HISTORY_API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
						public static HistoryApi: com.google.android.gms.fitness.HistoryApi;
						public static GOALS_API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
						public static GoalsApi: com.google.android.gms.fitness.GoalsApi;
						public static CONFIG_API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
						public static ConfigApi: com.google.android.gms.fitness.ConfigApi;
						public static BLE_API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
						public static BleApi: com.google.android.gms.fitness.BleApi;
						public static SCOPE_ACTIVITY_READ: com.google.android.gms.common.api.Scope;
						public static SCOPE_ACTIVITY_READ_WRITE: com.google.android.gms.common.api.Scope;
						public static SCOPE_LOCATION_READ: com.google.android.gms.common.api.Scope;
						public static SCOPE_LOCATION_READ_WRITE: com.google.android.gms.common.api.Scope;
						public static SCOPE_BODY_READ: com.google.android.gms.common.api.Scope;
						public static SCOPE_BODY_READ_WRITE: com.google.android.gms.common.api.Scope;
						public static SCOPE_NUTRITION_READ: com.google.android.gms.common.api.Scope;
						public static SCOPE_NUTRITION_READ_WRITE: com.google.android.gms.common.api.Scope;
						public static ACTION_TRACK: string;
						public static ACTION_VIEW: string;
						public static ACTION_VIEW_GOAL: string;
						public static EXTRA_START_TIME: string;
						public static EXTRA_END_TIME: string;
						public static getSessionsClient(param0: globalAndroid.app.Activity, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.SessionsClient;
						public static getSensorsClient(param0: globalAndroid.app.Activity, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.SensorsClient;
						public static getGoalsClient(param0: globalAndroid.app.Activity, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.GoalsClient;
						public static getBleClient(param0: globalAndroid.app.Activity, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.BleClient;
						public static getHistoryClient(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.HistoryClient;
						public static getConfigClient(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.ConfigClient;
						public static getRecordingClient(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.RecordingClient;
						public static getConfigClient(param0: globalAndroid.app.Activity, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.ConfigClient;
						public static getBleClient(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.BleClient;
						public static getHistoryClient(param0: globalAndroid.app.Activity, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.HistoryClient;
						public static getStartTime(param0: globalAndroid.content.Intent, param1: java.util.concurrent.TimeUnit): number;
						public static getSessionsClient(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.SessionsClient;
						public static getGoalsClient(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.GoalsClient;
						public static getEndTime(param0: globalAndroid.content.Intent, param1: java.util.concurrent.TimeUnit): number;
						public static getSensorsClient(param0: globalAndroid.content.Context, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.SensorsClient;
						public static getRecordingClient(param0: globalAndroid.app.Activity, param1: com.google.android.gms.auth.api.signin.GoogleSignInAccount): com.google.android.gms.fitness.RecordingClient;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class FitnessActivities extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.FitnessActivities>;
						public static MIME_TYPE_PREFIX: string;
						public static EXTRA_STATUS: string;
						public static STATUS_ACTIVE: string;
						public static STATUS_COMPLETED: string;
						public static AEROBICS: string;
						public static ARCHERY: string;
						public static BADMINTON: string;
						public static BASEBALL: string;
						public static BASKETBALL: string;
						public static BIATHLON: string;
						public static BIKING: string;
						public static BIKING_HAND: string;
						public static BIKING_MOUNTAIN: string;
						public static BIKING_ROAD: string;
						public static BIKING_SPINNING: string;
						public static BIKING_STATIONARY: string;
						public static BIKING_UTILITY: string;
						public static BOXING: string;
						public static CALISTHENICS: string;
						public static CIRCUIT_TRAINING: string;
						public static CRICKET: string;
						public static CROSSFIT: string;
						public static CURLING: string;
						public static DANCING: string;
						public static DIVING: string;
						public static ELEVATOR: string;
						public static ELLIPTICAL: string;
						public static ERGOMETER: string;
						public static ESCALATOR: string;
						public static FENCING: string;
						public static FOOTBALL_AMERICAN: string;
						public static FOOTBALL_AUSTRALIAN: string;
						public static FOOTBALL_SOCCER: string;
						public static FRISBEE_DISC: string;
						public static GARDENING: string;
						public static GOLF: string;
						public static GYMNASTICS: string;
						public static HANDBALL: string;
						public static HIGH_INTENSITY_INTERVAL_TRAINING: string;
						public static HIKING: string;
						public static HOCKEY: string;
						public static HORSEBACK_RIDING: string;
						public static HOUSEWORK: string;
						public static ICE_SKATING: string;
						public static IN_VEHICLE: string;
						public static INTERVAL_TRAINING: string;
						public static JUMP_ROPE: string;
						public static KAYAKING: string;
						public static KETTLEBELL_TRAINING: string;
						public static KICK_SCOOTER: string;
						public static KICKBOXING: string;
						public static KITESURFING: string;
						public static MARTIAL_ARTS: string;
						public static MEDITATION: string;
						public static MIXED_MARTIAL_ARTS: string;
						public static ON_FOOT: string;
						public static OTHER: string;
						public static P90X: string;
						public static PARAGLIDING: string;
						public static PILATES: string;
						public static POLO: string;
						public static RACQUETBALL: string;
						public static ROCK_CLIMBING: string;
						public static ROWING: string;
						public static ROWING_MACHINE: string;
						public static RUGBY: string;
						public static RUNNING: string;
						public static RUNNING_JOGGING: string;
						public static RUNNING_SAND: string;
						public static RUNNING_TREADMILL: string;
						public static SAILING: string;
						public static SCUBA_DIVING: string;
						public static SKATEBOARDING: string;
						public static SKATING: string;
						public static SKATING_CROSS: string;
						public static SKATING_INDOOR: string;
						public static SKATING_INLINE: string;
						public static SKIING: string;
						public static SKIING_BACK_COUNTRY: string;
						public static SKIING_CROSS_COUNTRY: string;
						public static SKIING_DOWNHILL: string;
						public static SKIING_KITE: string;
						public static SKIING_ROLLER: string;
						public static SLEDDING: string;
						public static SLEEP: string;
						public static SLEEP_LIGHT: string;
						public static SLEEP_DEEP: string;
						public static SLEEP_REM: string;
						public static SLEEP_AWAKE: string;
						public static SNOWBOARDING: string;
						public static SNOWMOBILE: string;
						public static SNOWSHOEING: string;
						public static SOFTBALL: string;
						public static SQUASH: string;
						public static STAIR_CLIMBING: string;
						public static STAIR_CLIMBING_MACHINE: string;
						public static STANDUP_PADDLEBOARDING: string;
						public static STILL: string;
						public static STRENGTH_TRAINING: string;
						public static SURFING: string;
						public static SWIMMING: string;
						public static SWIMMING_POOL: string;
						public static SWIMMING_OPEN_WATER: string;
						public static TABLE_TENNIS: string;
						public static TEAM_SPORTS: string;
						public static TENNIS: string;
						public static TILTING: string;
						public static TREADMILL: string;
						public static UNKNOWN: string;
						public static VOLLEYBALL: string;
						public static VOLLEYBALL_BEACH: string;
						public static VOLLEYBALL_INDOOR: string;
						public static WAKEBOARDING: string;
						public static WALKING: string;
						public static WALKING_FITNESS: string;
						public static WALKING_NORDIC: string;
						public static WALKING_TREADMILL: string;
						public static WALKING_STROLLER: string;
						public static WATER_POLO: string;
						public static WEIGHTLIFTING: string;
						public static WHEELCHAIR: string;
						public static WINDSURFING: string;
						public static YOGA: string;
						public static ZUMBA: string;
						public static getMimeType(param0: string): string;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class FitnessOptions extends java.lang.Object implements com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension, com.google.android.gms.common.api.Api.ApiOptions.HasGoogleSignInAccountOptions {
						public static class: java.lang.Class<com.google.android.gms.fitness.FitnessOptions>;
						public static ACCESS_READ: number;
						public static ACCESS_WRITE: number;
						public static builder(): com.google.android.gms.fitness.FitnessOptions.Builder;
						public getGoogleSignInAccount(): com.google.android.gms.auth.api.signin.GoogleSignInAccount;
						public getImpliedScopes(): java.util.List<com.google.android.gms.common.api.Scope>;
						public equals(param0: any): boolean;
						public hashCode(): number;
						public toBundle(): globalAndroid.os.Bundle;
						public getExtensionType(): number;
					}
					export module FitnessOptions {
						export class Builder extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.FitnessOptions.Builder>;
							public build(): com.google.android.gms.fitness.FitnessOptions;
							public addDataType(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.FitnessOptions.Builder;
							public addDataType(param0: com.google.android.gms.fitness.data.DataType, param1: number): com.google.android.gms.fitness.FitnessOptions.Builder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class FitnessStatusCodes extends com.google.android.gms.common.api.CommonStatusCodes {
						public static class: java.lang.Class<com.google.android.gms.fitness.FitnessStatusCodes>;
						public static SUCCESS_NO_DATA_SOURCES: number;
						public static SUCCESS_ALREADY_SUBSCRIBED: number;
						public static SUCCESS_NO_CLAIMED_DEVICE: number;
						public static SUCCESS_LISTENER_NOT_REGISTERED_FOR_FITNESS_DATA_UPDATES: number;
						public static NEEDS_OAUTH_PERMISSIONS: number;
						public static CONFLICTING_DATA_TYPE: number;
						public static INCONSISTENT_DATA_TYPE: number;
						public static DATA_TYPE_NOT_FOUND: number;
						public static APP_MISMATCH: number;
						public static UNKNOWN_AUTH_ERROR: number;
						public static MISSING_BLE_PERMISSION: number;
						public static UNSUPPORTED_PLATFORM: number;
						public static TRANSIENT_ERROR: number;
						public static EQUIVALENT_SESSION_ENDED: number;
						public static APP_NOT_FIT_ENABLED: number;
						public static API_EXCEPTION: number;
						public static AGGREGATION_NOT_SUPPORTED: number;
						public static UNSUPPORTED_ACCOUNT: number;
						public static DISABLED_BLUETOOTH: number;
						public static INCONSISTENT_PACKAGE_NAME: number;
						public static DATA_SOURCE_NOT_FOUND: number;
						public static DATASET_TIMESTAMP_INCONSISTENT_WITH_UPDATE_TIME_RANGE: number;
						public static INVALID_SESSION_TIMESTAMPS: number;
						public static INVALID_DATA_POINT: number;
						public static INVALID_TIMESTAMP: number;
						public static DATA_TYPE_NOT_ALLOWED_FOR_API: number;
						public static REQUIRES_APP_WHITELISTING: number;
						public constructor();
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class GoalsApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.GoalsApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.fitness.GoalsApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							readCurrentGoals(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.GoalsReadRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.GoalsResult>;
						});
						public constructor();
						public readCurrentGoals(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.GoalsReadRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.GoalsResult>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class GoalsClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.fitness.FitnessOptions> {
						public static class: java.lang.Class<com.google.android.gms.fitness.GoalsClient>;
						public readCurrentGoals(param0: com.google.android.gms.fitness.request.GoalsReadRequest): com.google.android.gms.tasks.Task<java.util.List<com.google.android.gms.fitness.data.Goal>>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class HistoryApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.HistoryApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.fitness.HistoryApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							readData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataReadRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataReadResult>;
							readDailyTotal(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DailyTotalResult>;
							readDailyTotalFromLocalDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DailyTotalResult>;
							insertData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataSet): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							deleteData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataDeleteRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							updateData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataUpdateRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							registerDataUpdateListener(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							unregisterDataUpdateListener(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						});
						public constructor();
						public readData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataReadRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataReadResult>;
						public insertData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataSet): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public deleteData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataDeleteRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public updateData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataUpdateRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public registerDataUpdateListener(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public readDailyTotalFromLocalDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DailyTotalResult>;
						public readDailyTotal(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DailyTotalResult>;
						public unregisterDataUpdateListener(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
					}
					export module HistoryApi {
						export class ViewIntentBuilder extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.HistoryApi.ViewIntentBuilder>;
							public setDataSource(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.HistoryApi.ViewIntentBuilder;
							public setTimeInterval(param0: number, param1: number, param2: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.HistoryApi.ViewIntentBuilder;
							public setPreferredApplication(param0: string): com.google.android.gms.fitness.HistoryApi.ViewIntentBuilder;
							public build(): globalAndroid.content.Intent;
							public constructor(param0: globalAndroid.content.Context, param1: com.google.android.gms.fitness.data.DataType);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class HistoryClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.fitness.FitnessOptions> {
						public static class: java.lang.Class<com.google.android.gms.fitness.HistoryClient>;
						public deleteData(param0: com.google.android.gms.fitness.request.DataDeleteRequest): com.google.android.gms.tasks.Task<java.lang.Void>;
						public readDailyTotal(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.tasks.Task<com.google.android.gms.fitness.data.DataSet>;
						public unregisterDataUpdateListener(param0: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public insertData(param0: com.google.android.gms.fitness.data.DataSet): com.google.android.gms.tasks.Task<java.lang.Void>;
						public readData(param0: com.google.android.gms.fitness.request.DataReadRequest): com.google.android.gms.tasks.Task<com.google.android.gms.fitness.result.DataReadResponse>;
						public registerDataUpdateListener(param0: com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest): com.google.android.gms.tasks.Task<java.lang.Void>;
						public readDailyTotalFromLocalDevice(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.tasks.Task<com.google.android.gms.fitness.data.DataSet>;
						public updateData(param0: com.google.android.gms.fitness.request.DataUpdateRequest): com.google.android.gms.tasks.Task<java.lang.Void>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class RecordingApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.RecordingApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.fitness.RecordingApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							subscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							subscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							unsubscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							unsubscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							unsubscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.Subscription): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							listSubscriptions(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.ListSubscriptionsResult>;
							listSubscriptions(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.ListSubscriptionsResult>;
						});
						public constructor();
						public listSubscriptions(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.ListSubscriptionsResult>;
						public subscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public unsubscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public unsubscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public unsubscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.Subscription): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public subscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public listSubscriptions(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.ListSubscriptionsResult>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class RecordingClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.fitness.FitnessOptions> {
						public static class: java.lang.Class<com.google.android.gms.fitness.RecordingClient>;
						public unsubscribe(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.tasks.Task<java.lang.Void>;
						public subscribe(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.tasks.Task<java.lang.Void>;
						public subscribe(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.tasks.Task<java.lang.Void>;
						public listSubscriptions(): com.google.android.gms.tasks.Task<java.util.List<com.google.android.gms.fitness.data.Subscription>>;
						public listSubscriptions(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.tasks.Task<java.util.List<com.google.android.gms.fitness.data.Subscription>>;
						public unsubscribe(param0: com.google.android.gms.fitness.data.Subscription): com.google.android.gms.tasks.Task<java.lang.Void>;
						public unsubscribe(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.tasks.Task<java.lang.Void>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class SensorsApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.SensorsApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.fitness.SensorsApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							findDataSources(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataSourcesRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataSourcesResult>;
							add(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SensorRequest, param2: com.google.android.gms.fitness.request.OnDataPointListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							add(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SensorRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							remove(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.OnDataPointListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							remove(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						});
						public constructor();
						public add(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SensorRequest, param2: com.google.android.gms.fitness.request.OnDataPointListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public add(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SensorRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public remove(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.OnDataPointListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public findDataSources(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataSourcesRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataSourcesResult>;
						public remove(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class SensorsClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.fitness.FitnessOptions> {
						public static class: java.lang.Class<com.google.android.gms.fitness.SensorsClient>;
						public findDataSources(param0: com.google.android.gms.fitness.request.DataSourcesRequest): com.google.android.gms.tasks.Task<java.util.List<com.google.android.gms.fitness.data.DataSource>>;
						public remove(param0: com.google.android.gms.fitness.request.OnDataPointListener): com.google.android.gms.tasks.Task<java.lang.Boolean>;
						public add(param0: com.google.android.gms.fitness.request.SensorRequest, param1: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public remove(param0: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public add(param0: com.google.android.gms.fitness.request.SensorRequest, param1: com.google.android.gms.fitness.request.OnDataPointListener): com.google.android.gms.tasks.Task<java.lang.Void>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class SessionsApi extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.SessionsApi>;
						/**
						 * Constructs a new instance of the com.google.android.gms.fitness.SessionsApi interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
						 */
						public constructor(implementation: {
							startSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.Session): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							stopSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.SessionStopResult>;
							insertSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SessionInsertRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							readSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SessionReadRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.SessionReadResult>;
							registerForSessions(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							unregisterForSessions(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						});
						public constructor();
						public readSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SessionReadRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.SessionReadResult>;
						public startSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.Session): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public stopSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.SessionStopResult>;
						public unregisterForSessions(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public insertSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SessionInsertRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						public registerForSessions(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
					}
					export module SessionsApi {
						export class ViewIntentBuilder extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.SessionsApi.ViewIntentBuilder>;
							public build(): globalAndroid.content.Intent;
							public setSession(param0: com.google.android.gms.fitness.data.Session): com.google.android.gms.fitness.SessionsApi.ViewIntentBuilder;
							public setPreferredApplication(param0: string): com.google.android.gms.fitness.SessionsApi.ViewIntentBuilder;
							public constructor(param0: globalAndroid.content.Context);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class SessionsClient extends com.google.android.gms.common.api.GoogleApi<com.google.android.gms.fitness.FitnessOptions> {
						public static class: java.lang.Class<com.google.android.gms.fitness.SessionsClient>;
						public registerForSessions(param0: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public startSession(param0: com.google.android.gms.fitness.data.Session): com.google.android.gms.tasks.Task<java.lang.Void>;
						public stopSession(param0: string): com.google.android.gms.tasks.Task<java.util.List<com.google.android.gms.fitness.data.Session>>;
						public insertSession(param0: com.google.android.gms.fitness.request.SessionInsertRequest): com.google.android.gms.tasks.Task<java.lang.Void>;
						public unregisterForSessions(param0: globalAndroid.app.PendingIntent): com.google.android.gms.tasks.Task<java.lang.Void>;
						public readSession(param0: com.google.android.gms.fitness.request.SessionReadRequest): com.google.android.gms.tasks.Task<com.google.android.gms.fitness.result.SessionReadResponse>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class BleDevice extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.BleDevice>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.BleDevice>;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getAddress(): string;
							public getSupportedProfiles(): java.util.List<string>;
							public hashCode(): number;
							public getName(): string;
							public describeContents(): number;
							public toString(): string;
							public getDataTypes(): java.util.List<com.google.android.gms.fitness.data.DataType>;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class Bucket extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.Bucket>;
							public static TYPE_TIME: number;
							public static TYPE_SESSION: number;
							public static TYPE_ACTIVITY_TYPE: number;
							public static TYPE_ACTIVITY_SEGMENT: number;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Bucket>;
							public constructor();
							public getSession(): com.google.android.gms.fitness.data.Session;
							public getActivityType(): number;
							public hashCode(): number;
							public getEndTime(param0: java.util.concurrent.TimeUnit): number;
							public getBucketType(): number;
							public constructor(param0: com.google.android.gms.fitness.data.RawBucket, param1: java.util.List<com.google.android.gms.fitness.data.DataSource>);
							public toString(): string;
							public getStartTime(param0: java.util.concurrent.TimeUnit): number;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public describeContents(): number;
							public getDataSet(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.data.DataSet;
							public getDataSets(): java.util.List<com.google.android.gms.fitness.data.DataSet>;
							public equals(param0: any): boolean;
							public getActivity(): string;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class DataPoint extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.DataPoint>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataPoint>;
							public constructor();
							public getDataSource(): com.google.android.gms.fitness.data.DataSource;
							public hashCode(): number;
							public getEndTime(param0: java.util.concurrent.TimeUnit): number;
							public toString(): string;
							public getOriginalDataSource(): com.google.android.gms.fitness.data.DataSource;
							public getTimestamp(param0: java.util.concurrent.TimeUnit): number;
							public getStartTime(param0: java.util.concurrent.TimeUnit): number;
							public static create(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.data.DataPoint;
							public getDataType(): com.google.android.gms.fitness.data.DataType;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: com.google.android.gms.fitness.data.DataSource, param1: number, param2: number, param3: native.Array<com.google.android.gms.fitness.data.Value>, param4: com.google.android.gms.fitness.data.DataSource, param5: number, param6: number);
							public static extract(param0: globalAndroid.content.Intent): com.google.android.gms.fitness.data.DataPoint;
							public describeContents(): number;
							public setFloatValues(param0: native.Array<number>): com.google.android.gms.fitness.data.DataPoint;
							public setIntValues(param0: native.Array<number>): com.google.android.gms.fitness.data.DataPoint;
							public setTimeInterval(param0: number, param1: number, param2: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.data.DataPoint;
							public getValue(param0: com.google.android.gms.fitness.data.Field): com.google.android.gms.fitness.data.Value;
							public equals(param0: any): boolean;
							public setTimestamp(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.data.DataPoint;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class DataSet extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.DataSet>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataSet>;
							public constructor();
							public getDataSource(): com.google.android.gms.fitness.data.DataSource;
							public constructor(param0: com.google.android.gms.fitness.data.RawDataSet, param1: java.util.List<com.google.android.gms.fitness.data.DataSource>);
							public addAll(param0: java.lang.Iterable<com.google.android.gms.fitness.data.DataPoint>): void;
							public hashCode(): number;
							public add(param0: com.google.android.gms.fitness.data.DataPoint): void;
							public toString(): string;
							public getDataType(): com.google.android.gms.fitness.data.DataType;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public createDataPoint(): com.google.android.gms.fitness.data.DataPoint;
							public getDataPoints(): java.util.List<com.google.android.gms.fitness.data.DataPoint>;
							public describeContents(): number;
							public isEmpty(): boolean;
							public equals(param0: any): boolean;
							public static create(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.data.DataSet;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class DataSource extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.DataSource>;
							public static EXTRA_DATA_SOURCE: string;
							public static TYPE_RAW: number;
							public static TYPE_DERIVED: number;
							public static DATA_QUALITY_BLOOD_PRESSURE_ESH2002: number;
							public static DATA_QUALITY_BLOOD_PRESSURE_ESH2010: number;
							public static DATA_QUALITY_BLOOD_PRESSURE_AAMI: number;
							public static DATA_QUALITY_BLOOD_PRESSURE_BHS_A_A: number;
							public static DATA_QUALITY_BLOOD_PRESSURE_BHS_A_B: number;
							public static DATA_QUALITY_BLOOD_PRESSURE_BHS_B_A: number;
							public static DATA_QUALITY_BLOOD_PRESSURE_BHS_B_B: number;
							public static DATA_QUALITY_BLOOD_GLUCOSE_ISO151972003: number;
							public static DATA_QUALITY_BLOOD_GLUCOSE_ISO151972013: number;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataSource>;
							public constructor();
							public getType(): number;
							public hashCode(): number;
							public getDataQualityStandards(): native.Array<number>;
							public static extract(param0: globalAndroid.content.Intent): com.google.android.gms.fitness.data.DataSource;
							public toString(): string;
							public getAppPackageName(): string;
							public constructor(param0: com.google.android.gms.fitness.data.DataType, param1: string, param2: number, param3: com.google.android.gms.fitness.data.Device, param4: any /* com.google.android.gms.fitness.data.zzb*/, param5: string, param6: native.Array<number>);
							public getDataType(): com.google.android.gms.fitness.data.DataType;
							public toDebugString(): string;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getStreamName(): string;
							public getName(): string;
							public getStreamIdentifier(): string;
							public getDevice(): com.google.android.gms.fitness.data.Device;
							public equals(param0: any): boolean;
						}
						export module DataSource {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.DataSource.Builder>;
								public build(): com.google.android.gms.fitness.data.DataSource;
								public setDataType(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.data.DataSource.Builder;
								public setType(param0: number): com.google.android.gms.fitness.data.DataSource.Builder;
								public setDataQualityStandards(param0: native.Array<number>): com.google.android.gms.fitness.data.DataSource.Builder;
								public setName(param0: string): com.google.android.gms.fitness.data.DataSource.Builder;
								public constructor();
								public setStreamName(param0: string): com.google.android.gms.fitness.data.DataSource.Builder;
								public setDevice(param0: com.google.android.gms.fitness.data.Device): com.google.android.gms.fitness.data.DataSource.Builder;
								public setAppPackageName(param0: string): com.google.android.gms.fitness.data.DataSource.Builder;
								public setAppPackageName(param0: globalAndroid.content.Context): com.google.android.gms.fitness.data.DataSource.Builder;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class DataType extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.DataType>;
							public static MIME_TYPE_PREFIX: string;
							public static TYPE_STEP_COUNT_DELTA: com.google.android.gms.fitness.data.DataType;
							public static TYPE_STEP_COUNT_CUMULATIVE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_STEP_COUNT_CADENCE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_ACTIVITY_SEGMENT: com.google.android.gms.fitness.data.DataType;
							public static TYPE_CALORIES_CONSUMED: com.google.android.gms.fitness.data.DataType;
							public static TYPE_CALORIES_EXPENDED: com.google.android.gms.fitness.data.DataType;
							public static TYPE_BASAL_METABOLIC_RATE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_POWER_SAMPLE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_ACTIVITY_SAMPLE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_ACTIVITY_SAMPLES: com.google.android.gms.fitness.data.DataType;
							public static TYPE_HEART_RATE_BPM: com.google.android.gms.fitness.data.DataType;
							public static TYPE_LOCATION_SAMPLE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_LOCATION_TRACK: com.google.android.gms.fitness.data.DataType;
							public static TYPE_DISTANCE_DELTA: com.google.android.gms.fitness.data.DataType;
							public static TYPE_DISTANCE_CUMULATIVE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_SPEED: com.google.android.gms.fitness.data.DataType;
							public static TYPE_CYCLING_WHEEL_REVOLUTION: com.google.android.gms.fitness.data.DataType;
							public static TYPE_CYCLING_WHEEL_RPM: com.google.android.gms.fitness.data.DataType;
							public static TYPE_CYCLING_PEDALING_CUMULATIVE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_CYCLING_PEDALING_CADENCE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_HEIGHT: com.google.android.gms.fitness.data.DataType;
							public static TYPE_WEIGHT: com.google.android.gms.fitness.data.DataType;
							public static TYPE_BODY_FAT_PERCENTAGE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_NUTRITION: com.google.android.gms.fitness.data.DataType;
							public static TYPE_HYDRATION: com.google.android.gms.fitness.data.DataType;
							public static TYPE_WORKOUT_EXERCISE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_MOVE_MINUTES: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_MOVE_MINUTES: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_ACTIVITY_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_BASAL_METABOLIC_RATE_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_STEP_COUNT_DELTA: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_DISTANCE_DELTA: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_CALORIES_CONSUMED: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_CALORIES_EXPENDED: com.google.android.gms.fitness.data.DataType;
							public static TYPE_HEART_POINTS: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_HEART_POINTS: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_HEART_RATE_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_LOCATION_BOUNDING_BOX: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_POWER_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_SPEED_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_BODY_FAT_PERCENTAGE_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_WEIGHT_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_HEIGHT_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_NUTRITION_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_HYDRATION: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_INPUT_TYPES: java.util.Set<com.google.android.gms.fitness.data.DataType>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataType>;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public static getMimeType(param0: com.google.android.gms.fitness.data.DataType): string;
							public getName(): string;
							public describeContents(): number;
							public static getAggregatesForInput(param0: com.google.android.gms.fitness.data.DataType): java.util.List<com.google.android.gms.fitness.data.DataType>;
							public getFields(): java.util.List<com.google.android.gms.fitness.data.Field>;
							public indexOf(param0: com.google.android.gms.fitness.data.Field): number;
							public toString(): string;
							public constructor(param0: string, param1: string, param2: string, param3: native.Array<com.google.android.gms.fitness.data.Field>);
							public equals(param0: any): boolean;
						}
						export module DataType {
							export class zza extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.DataType.zza>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class DataUpdateNotification extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.DataUpdateNotification>;
							public static ACTION: string;
							public static OPERATION_INSERT: number;
							public static OPERATION_DELETE: number;
							public static OPERATION_UPDATE: number;
							public static EXTRA_DATA_UPDATE_NOTIFICATION: string;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataUpdateNotification>;
							public constructor();
							public getDataSource(): com.google.android.gms.fitness.data.DataSource;
							public getDataType(): com.google.android.gms.fitness.data.DataType;
							public getOperationType(): number;
							public getUpdateEndTime(param0: java.util.concurrent.TimeUnit): number;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: number, param1: number, param2: number, param3: com.google.android.gms.fitness.data.DataSource, param4: com.google.android.gms.fitness.data.DataType);
							public hashCode(): number;
							public static getDataUpdateNotification(param0: globalAndroid.content.Intent): com.google.android.gms.fitness.data.DataUpdateNotification;
							public toString(): string;
							public equals(param0: any): boolean;
							public getUpdateStartTime(param0: java.util.concurrent.TimeUnit): number;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class Device extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.Device>;
							public static TYPE_UNKNOWN: number;
							public static TYPE_PHONE: number;
							public static TYPE_TABLET: number;
							public static TYPE_WATCH: number;
							public static TYPE_CHEST_STRAP: number;
							public static TYPE_SCALE: number;
							public static TYPE_HEAD_MOUNTED: number;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Device>;
							public constructor();
							public getType(): number;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public getManufacturer(): string;
							public constructor(param0: string, param1: string, param2: string, param3: number, param4: number);
							public constructor(param0: string, param1: string, param2: string, param3: number);
							public toString(): string;
							public static getLocalDevice(param0: globalAndroid.content.Context): com.google.android.gms.fitness.data.Device;
							public getUid(): string;
							public equals(param0: any): boolean;
							public getModel(): string;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class Field extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.Field>;
							public static FIELD_ACTIVITY: com.google.android.gms.fitness.data.Field;
							public static FIELD_CONFIDENCE: com.google.android.gms.fitness.data.Field;
							public static FIELD_ACTIVITY_CONFIDENCE: com.google.android.gms.fitness.data.Field;
							public static FIELD_STEPS: com.google.android.gms.fitness.data.Field;
							public static FIELD_STEP_LENGTH: com.google.android.gms.fitness.data.Field;
							public static FIELD_DURATION: com.google.android.gms.fitness.data.Field;
							public static FIELD_BPM: com.google.android.gms.fitness.data.Field;
							public static FIELD_LATITUDE: com.google.android.gms.fitness.data.Field;
							public static FIELD_LONGITUDE: com.google.android.gms.fitness.data.Field;
							public static FIELD_ACCURACY: com.google.android.gms.fitness.data.Field;
							public static FIELD_ALTITUDE: com.google.android.gms.fitness.data.Field;
							public static FIELD_DISTANCE: com.google.android.gms.fitness.data.Field;
							public static FIELD_HEIGHT: com.google.android.gms.fitness.data.Field;
							public static FIELD_WEIGHT: com.google.android.gms.fitness.data.Field;
							public static FIELD_CIRCUMFERENCE: com.google.android.gms.fitness.data.Field;
							public static FIELD_PERCENTAGE: com.google.android.gms.fitness.data.Field;
							public static FIELD_SPEED: com.google.android.gms.fitness.data.Field;
							public static FIELD_RPM: com.google.android.gms.fitness.data.Field;
							public static FIELD_REVOLUTIONS: com.google.android.gms.fitness.data.Field;
							public static FIELD_CALORIES: com.google.android.gms.fitness.data.Field;
							public static FIELD_WATTS: com.google.android.gms.fitness.data.Field;
							public static FIELD_VOLUME: com.google.android.gms.fitness.data.Field;
							public static FIELD_MEAL_TYPE: com.google.android.gms.fitness.data.Field;
							public static MEAL_TYPE_UNKNOWN: number;
							public static MEAL_TYPE_BREAKFAST: number;
							public static MEAL_TYPE_LUNCH: number;
							public static MEAL_TYPE_DINNER: number;
							public static MEAL_TYPE_SNACK: number;
							public static FIELD_FOOD_ITEM: com.google.android.gms.fitness.data.Field;
							public static FIELD_NUTRIENTS: com.google.android.gms.fitness.data.Field;
							public static NUTRIENT_CALORIES: string;
							public static NUTRIENT_TOTAL_FAT: string;
							public static NUTRIENT_SATURATED_FAT: string;
							public static NUTRIENT_UNSATURATED_FAT: string;
							public static NUTRIENT_POLYUNSATURATED_FAT: string;
							public static NUTRIENT_MONOUNSATURATED_FAT: string;
							public static NUTRIENT_TRANS_FAT: string;
							public static NUTRIENT_CHOLESTEROL: string;
							public static NUTRIENT_SODIUM: string;
							public static NUTRIENT_POTASSIUM: string;
							public static NUTRIENT_TOTAL_CARBS: string;
							public static NUTRIENT_DIETARY_FIBER: string;
							public static NUTRIENT_SUGAR: string;
							public static NUTRIENT_PROTEIN: string;
							public static NUTRIENT_VITAMIN_A: string;
							public static NUTRIENT_VITAMIN_C: string;
							public static NUTRIENT_CALCIUM: string;
							public static NUTRIENT_IRON: string;
							public static FIELD_EXERCISE: com.google.android.gms.fitness.data.Field;
							public static FIELD_REPETITIONS: com.google.android.gms.fitness.data.Field;
							public static FIELD_RESISTANCE: com.google.android.gms.fitness.data.Field;
							public static FIELD_RESISTANCE_TYPE: com.google.android.gms.fitness.data.Field;
							public static RESISTANCE_TYPE_UNKNOWN: number;
							public static RESISTANCE_TYPE_BARBELL: number;
							public static RESISTANCE_TYPE_CABLE: number;
							public static RESISTANCE_TYPE_DUMBBELL: number;
							public static RESISTANCE_TYPE_KETTLEBELL: number;
							public static RESISTANCE_TYPE_MACHINE: number;
							public static RESISTANCE_TYPE_BODY: number;
							public static FIELD_NUM_SEGMENTS: com.google.android.gms.fitness.data.Field;
							public static FIELD_AVERAGE: com.google.android.gms.fitness.data.Field;
							public static FIELD_MAX: com.google.android.gms.fitness.data.Field;
							public static FIELD_MIN: com.google.android.gms.fitness.data.Field;
							public static FIELD_LOW_LATITUDE: com.google.android.gms.fitness.data.Field;
							public static FIELD_LOW_LONGITUDE: com.google.android.gms.fitness.data.Field;
							public static FIELD_HIGH_LATITUDE: com.google.android.gms.fitness.data.Field;
							public static FIELD_HIGH_LONGITUDE: com.google.android.gms.fitness.data.Field;
							public static FIELD_OCCURRENCES: com.google.android.gms.fitness.data.Field;
							public static FIELD_INTENSITY: com.google.android.gms.fitness.data.Field;
							public static FORMAT_INT32: number;
							public static FORMAT_FLOAT: number;
							public static FORMAT_STRING: number;
							public static FORMAT_MAP: number;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Field>;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public getName(): string;
							public isOptional(): java.lang.Boolean;
							public toString(): string;
							public getFormat(): number;
							public equals(param0: any): boolean;
						}
						export module Field {
							export class zza extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.Field.zza>;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class Goal extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.Goal>;
							public static OBJECTIVE_TYPE_METRIC: number;
							public static OBJECTIVE_TYPE_DURATION: number;
							public static OBJECTIVE_TYPE_FREQUENCY: number;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal>;
							public getFrequencyObjective(): com.google.android.gms.fitness.data.Goal.FrequencyObjective;
							public hashCode(): number;
							public getEndTime(param0: java.util.Calendar, param1: java.util.concurrent.TimeUnit): number;
							public toString(): string;
							public getRecurrence(): com.google.android.gms.fitness.data.Goal.Recurrence;
							public getObjectiveType(): number;
							public getDurationObjective(): com.google.android.gms.fitness.data.Goal.DurationObjective;
							public getStartTime(param0: java.util.Calendar, param1: java.util.concurrent.TimeUnit): number;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getActivityName(): string;
							public getMetricObjective(): com.google.android.gms.fitness.data.Goal.MetricObjective;
							public equals(param0: any): boolean;
							public getCreateTime(param0: java.util.concurrent.TimeUnit): number;
						}
						export module Goal {
							export class DurationObjective extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.Goal.DurationObjective>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal.DurationObjective>;
								public toString(): string;
								public getDuration(param0: java.util.concurrent.TimeUnit): number;
								public equals(param0: any): boolean;
								public constructor();
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public constructor(param0: number, param1: java.util.concurrent.TimeUnit);
								public hashCode(): number;
							}
							export class FrequencyObjective extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.Goal.FrequencyObjective>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal.FrequencyObjective>;
								public toString(): string;
								public constructor(param0: number);
								public equals(param0: any): boolean;
								public getFrequency(): number;
								public constructor();
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public hashCode(): number;
							}
							export class MetricObjective extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.Goal.MetricObjective>;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal.MetricObjective>;
								public toString(): string;
								public getValue(): number;
								public getDataTypeName(): string;
								public equals(param0: any): boolean;
								public constructor();
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public hashCode(): number;
								public constructor(param0: string, param1: number, param2: number);
								public constructor(param0: string, param1: number);
							}
							export class MismatchedGoalException extends java.lang.IllegalStateException {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.Goal.MismatchedGoalException>;
								public constructor(param0: string, param1: java.lang.Throwable, param2: boolean, param3: boolean);
								public constructor(param0: java.lang.Throwable);
								public constructor(param0: string, param1: java.lang.Throwable);
								public constructor();
								public constructor(param0: string);
							}
							export class ObjectiveType extends java.lang.Object implements java.lang.annotation.Annotation {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.Goal.ObjectiveType>;
								/**
								 * Constructs a new instance of the com.google.android.gms.fitness.data.Goal$ObjectiveType interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
								 */
								public constructor(implementation: {
									equals(param0: any): boolean;
									hashCode(): number;
									toString(): string;
									annotationType(): java.lang.Class<any>;
								});
								public constructor();
								public toString(): string;
								public equals(param0: any): boolean;
								public annotationType(): java.lang.Class<any>;
								public hashCode(): number;
							}
							export class Recurrence extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.Goal.Recurrence>;
								public static UNIT_DAY: number;
								public static UNIT_WEEK: number;
								public static UNIT_MONTH: number;
								public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal.Recurrence>;
								public toString(): string;
								public constructor(param0: number, param1: number);
								public equals(param0: any): boolean;
								public constructor();
								public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
								public getCount(): number;
								public hashCode(): number;
								public getUnit(): number;
							}
							export module Recurrence {
								export class RecurrenceUnit extends java.lang.Object implements java.lang.annotation.Annotation {
									public static class: java.lang.Class<com.google.android.gms.fitness.data.Goal.Recurrence.RecurrenceUnit>;
									/**
									 * Constructs a new instance of the com.google.android.gms.fitness.data.Goal$Recurrence$RecurrenceUnit interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
									 */
									public constructor(implementation: {
										equals(param0: any): boolean;
										hashCode(): number;
										toString(): string;
										annotationType(): java.lang.Class<any>;
									});
									public constructor();
									public hashCode(): number;
									public equals(param0: any): boolean;
									public annotationType(): java.lang.Class<any>;
									public toString(): string;
								}
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class HealthDataTypes extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.HealthDataTypes>;
							public static TYPE_BLOOD_PRESSURE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_BLOOD_GLUCOSE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_OXYGEN_SATURATION: com.google.android.gms.fitness.data.DataType;
							public static TYPE_BODY_TEMPERATURE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_BASAL_BODY_TEMPERATURE: com.google.android.gms.fitness.data.DataType;
							public static TYPE_CERVICAL_MUCUS: com.google.android.gms.fitness.data.DataType;
							public static TYPE_CERVICAL_POSITION: com.google.android.gms.fitness.data.DataType;
							public static TYPE_MENSTRUATION: com.google.android.gms.fitness.data.DataType;
							public static TYPE_OVULATION_TEST: com.google.android.gms.fitness.data.DataType;
							public static TYPE_VAGINAL_SPOTTING: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_BLOOD_PRESSURE_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_BLOOD_GLUCOSE_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_OXYGEN_SATURATION_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_BODY_TEMPERATURE_SUMMARY: com.google.android.gms.fitness.data.DataType;
							public static AGGREGATE_BASAL_BODY_TEMPERATURE_SUMMARY: com.google.android.gms.fitness.data.DataType;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class HealthFields extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.HealthFields>;
							public static FIELD_BLOOD_PRESSURE_SYSTOLIC: com.google.android.gms.fitness.data.Field;
							public static FIELD_BLOOD_PRESSURE_SYSTOLIC_AVERAGE: com.google.android.gms.fitness.data.Field;
							public static FIELD_BLOOD_PRESSURE_SYSTOLIC_MIN: com.google.android.gms.fitness.data.Field;
							public static FIELD_BLOOD_PRESSURE_SYSTOLIC_MAX: com.google.android.gms.fitness.data.Field;
							public static FIELD_BLOOD_PRESSURE_DIASTOLIC: com.google.android.gms.fitness.data.Field;
							public static FIELD_BLOOD_PRESSURE_DIASTOLIC_AVERAGE: com.google.android.gms.fitness.data.Field;
							public static FIELD_BLOOD_PRESSURE_DIASTOLIC_MIN: com.google.android.gms.fitness.data.Field;
							public static FIELD_BLOOD_PRESSURE_DIASTOLIC_MAX: com.google.android.gms.fitness.data.Field;
							public static FIELD_BODY_POSITION: com.google.android.gms.fitness.data.Field;
							public static BODY_POSITION_STANDING: number;
							public static BODY_POSITION_SITTING: number;
							public static BODY_POSITION_LYING_DOWN: number;
							public static BODY_POSITION_SEMI_RECUMBENT: number;
							public static FIELD_BLOOD_PRESSURE_MEASUREMENT_LOCATION: com.google.android.gms.fitness.data.Field;
							public static BLOOD_PRESSURE_MEASUREMENT_LOCATION_LEFT_WRIST: number;
							public static BLOOD_PRESSURE_MEASUREMENT_LOCATION_RIGHT_WRIST: number;
							public static BLOOD_PRESSURE_MEASUREMENT_LOCATION_LEFT_UPPER_ARM: number;
							public static BLOOD_PRESSURE_MEASUREMENT_LOCATION_RIGHT_UPPER_ARM: number;
							public static FIELD_BLOOD_GLUCOSE_LEVEL: com.google.android.gms.fitness.data.Field;
							public static FIELD_TEMPORAL_RELATION_TO_MEAL: com.google.android.gms.fitness.data.Field;
							public static FIELD_TEMPORAL_RELATION_TO_MEAL_GENERAL: number;
							public static FIELD_TEMPORAL_RELATION_TO_MEAL_FASTING: number;
							public static FIELD_TEMPORAL_RELATION_TO_MEAL_BEFORE_MEAL: number;
							public static FIELD_TEMPORAL_RELATION_TO_MEAL_AFTER_MEAL: number;
							public static FIELD_TEMPORAL_RELATION_TO_SLEEP: com.google.android.gms.fitness.data.Field;
							public static TEMPORAL_RELATION_TO_SLEEP_FULLY_AWAKE: number;
							public static TEMPORAL_RELATION_TO_SLEEP_BEFORE_SLEEP: number;
							public static TEMPORAL_RELATION_TO_SLEEP_ON_WAKING: number;
							public static TEMPORAL_RELATION_TO_SLEEP_DURING_SLEEP: number;
							public static FIELD_BLOOD_GLUCOSE_SPECIMEN_SOURCE: com.google.android.gms.fitness.data.Field;
							public static BLOOD_GLUCOSE_SPECIMEN_SOURCE_INTERSTITIAL_FLUID: number;
							public static BLOOD_GLUCOSE_SPECIMEN_SOURCE_CAPILLARY_BLOOD: number;
							public static BLOOD_GLUCOSE_SPECIMEN_SOURCE_PLASMA: number;
							public static BLOOD_GLUCOSE_SPECIMEN_SOURCE_SERUM: number;
							public static BLOOD_GLUCOSE_SPECIMEN_SOURCE_TEARS: number;
							public static BLOOD_GLUCOSE_SPECIMEN_SOURCE_WHOLE_BLOOD: number;
							public static FIELD_OXYGEN_SATURATION: com.google.android.gms.fitness.data.Field;
							public static FIELD_OXYGEN_SATURATION_AVERAGE: com.google.android.gms.fitness.data.Field;
							public static FIELD_OXYGEN_SATURATION_MIN: com.google.android.gms.fitness.data.Field;
							public static FIELD_OXYGEN_SATURATION_MAX: com.google.android.gms.fitness.data.Field;
							public static FIELD_SUPPLEMENTAL_OXYGEN_FLOW_RATE: com.google.android.gms.fitness.data.Field;
							public static FIELD_SUPPLEMENTAL_OXYGEN_FLOW_RATE_AVERAGE: com.google.android.gms.fitness.data.Field;
							public static FIELD_SUPPLEMENTAL_OXYGEN_FLOW_RATE_MIN: com.google.android.gms.fitness.data.Field;
							public static FIELD_SUPPLEMENTAL_OXYGEN_FLOW_RATE_MAX: com.google.android.gms.fitness.data.Field;
							public static FIELD_OXYGEN_THERAPY_ADMINISTRATION_MODE: com.google.android.gms.fitness.data.Field;
							public static OXYGEN_THERAPY_ADMINISTRATION_MODE_NASAL_CANULA: number;
							public static FIELD_OXYGEN_SATURATION_SYSTEM: com.google.android.gms.fitness.data.Field;
							public static OXYGEN_SATURATION_SYSTEM_PERIPHERAL_CAPILLARY: number;
							public static FIELD_OXYGEN_SATURATION_MEASUREMENT_METHOD: com.google.android.gms.fitness.data.Field;
							public static OXYGEN_SATURATION_MEASUREMENT_METHOD_PULSE_OXIMETRY: number;
							public static FIELD_BODY_TEMPERATURE: com.google.android.gms.fitness.data.Field;
							public static FIELD_BODY_TEMPERATURE_MEASUREMENT_LOCATION: com.google.android.gms.fitness.data.Field;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_AXILLARY: number;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_FINGER: number;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_FOREHEAD: number;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_ORAL: number;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_RECTAL: number;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_TEMPORAL_ARTERY: number;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_TOE: number;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_TYMPANIC: number;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_WRIST: number;
							public static BODY_TEMPERATURE_MEASUREMENT_LOCATION_VAGINAL: number;
							public static FIELD_CERVICAL_MUCUS_TEXTURE: com.google.android.gms.fitness.data.Field;
							public static CERVICAL_MUCUS_TEXTURE_DRY: number;
							public static CERVICAL_MUCUS_TEXTURE_STICKY: number;
							public static CERVICAL_MUCUS_TEXTURE_CREAMY: number;
							public static CERVICAL_MUCUS_TEXTURE_WATERY: number;
							public static CERVICAL_MUCUS_TEXTURE_EGG_WHITE: number;
							public static FIELD_CERVICAL_MUCUS_AMOUNT: com.google.android.gms.fitness.data.Field;
							public static CERVICAL_MUCUS_AMOUNT_LIGHT: number;
							public static CERVICAL_MUCUS_AMOUNT_MEDIUM: number;
							public static CERVICAL_MUCUS_AMOUNT_HEAVY: number;
							public static FIELD_CERVICAL_POSITION: com.google.android.gms.fitness.data.Field;
							public static CERVICAL_POSITION_LOW: number;
							public static CERVICAL_POSITION_MEDIUM: number;
							public static CERVICAL_POSITION_HIGH: number;
							public static FIELD_CERVICAL_DILATION: com.google.android.gms.fitness.data.Field;
							public static CERVICAL_DILATION_CLOSED: number;
							public static CERVICAL_DILATION_MEDIUM: number;
							public static CERVICAL_DILATION_OPEN: number;
							public static FIELD_CERVICAL_FIRMNESS: com.google.android.gms.fitness.data.Field;
							public static CERVICAL_FIRMNESS_SOFT: number;
							public static CERVICAL_FIRMNESS_MEDIUM: number;
							public static CERVICAL_FIRMNESS_FIRM: number;
							public static FIELD_MENSTRUAL_FLOW: com.google.android.gms.fitness.data.Field;
							public static MENSTRUAL_FLOW_SPOTTING: number;
							public static MENSTRUAL_FLOW_LIGHT: number;
							public static MENSTRUAL_FLOW_MEDIUM: number;
							public static MENSTRUAL_FLOW_HEAVY: number;
							public static FIELD_OVULATION_TEST_RESULT: com.google.android.gms.fitness.data.Field;
							public static OVULATION_TEST_RESULT_NEGATIVE: number;
							public static OVULATION_TEST_RESULT_POSITIVE: number;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class MapValue extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.internal.ReflectedParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.MapValue>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.MapValue>;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: number, param1: number);
							public hashCode(): number;
							public describeContents(): number;
							public asFloat(): number;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class RawBucket extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.RawBucket>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.RawBucket>;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: number, param1: number, param2: com.google.android.gms.fitness.data.Session, param3: number, param4: java.util.List<com.google.android.gms.fitness.data.RawDataSet>, param5: number, param6: boolean);
							public toString(): string;
							public equals(param0: any): boolean;
							public constructor(param0: com.google.android.gms.fitness.data.Bucket, param1: java.util.List<com.google.android.gms.fitness.data.DataSource>);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class RawDataPoint extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.RawDataPoint>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.RawDataPoint>;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public getTimestampNanos(): number;
							public toString(): string;
							public constructor(param0: number, param1: number, param2: native.Array<com.google.android.gms.fitness.data.Value>, param3: number, param4: number, param5: number, param6: number);
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class RawDataSet extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.RawDataSet>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.RawDataSet>;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: number, param1: java.util.List<com.google.android.gms.fitness.data.RawDataPoint>, param2: boolean);
							public constructor(param0: com.google.android.gms.fitness.data.DataSet, param1: java.util.List<com.google.android.gms.fitness.data.DataSource>);
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class Session extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.Session>;
							public static EXTRA_SESSION: string;
							public static MIME_TYPE_PREFIX: string;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Session>;
							public constructor();
							public static getMimeType(param0: string): string;
							public getActiveTime(param0: java.util.concurrent.TimeUnit): number;
							public hashCode(): number;
							public getEndTime(param0: java.util.concurrent.TimeUnit): number;
							public toString(): string;
							public getAppPackageName(): string;
							public constructor(param0: number, param1: number, param2: string, param3: string, param4: string, param5: number, param6: any /* com.google.android.gms.fitness.data.zzb*/, param7: java.lang.Long);
							public getDescription(): string;
							public getStartTime(param0: java.util.concurrent.TimeUnit): number;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getName(): string;
							public static extract(param0: globalAndroid.content.Intent): com.google.android.gms.fitness.data.Session;
							public getIdentifier(): string;
							public equals(param0: any): boolean;
							public hasActiveTime(): boolean;
							public isOngoing(): boolean;
							public getActivity(): string;
						}
						export module Session {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.Session.Builder>;
								public setDescription(param0: string): com.google.android.gms.fitness.data.Session.Builder;
								public setEndTime(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.data.Session.Builder;
								public setName(param0: string): com.google.android.gms.fitness.data.Session.Builder;
								public setIdentifier(param0: string): com.google.android.gms.fitness.data.Session.Builder;
								public setStartTime(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.data.Session.Builder;
								public constructor();
								public setActivity(param0: string): com.google.android.gms.fitness.data.Session.Builder;
								public setActiveTime(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.data.Session.Builder;
								public build(): com.google.android.gms.fitness.data.Session;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class Subscription extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.Subscription>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Subscription>;
							public getDataSource(): com.google.android.gms.fitness.data.DataSource;
							public getDataType(): com.google.android.gms.fitness.data.DataType;
							public toDebugString(): string;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public equals(param0: any): boolean;
						}
						export module Subscription {
							export class zza extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.data.Subscription.zza>;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class Value extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.Value>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Value>;
							public constructor();
							public clearKey(param0: string): void;
							public getKeyValue(param0: string): java.lang.Float;
							public hashCode(): number;
							public setKeyValue(param0: string, param1: number): void;
							public setInt(param0: number): void;
							public toString(): string;
							public setFloat(param0: number): void;
							public asActivity(): string;
							public isSet(): boolean;
							public getFormat(): number;
							public constructor(param0: number);
							public asInt(): number;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public setString(param0: string): void;
							public asString(): string;
							public asFloat(): number;
							public setActivity(param0: string): void;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class WorkoutExercises extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.WorkoutExercises>;
							public static PUSHUP: string;
							public static CLOSE_GRIP_PUSHUP: string;
							public static PIKE_PUSHUP: string;
							public static BENCH_PRESS: string;
							public static INCLINE_BENCH_PRESS: string;
							public static DECLINE_BENCH_PRESS: string;
							public static CLOSE_GRIP_BENCH_PRESS: string;
							public static FLY: string;
							public static PULLOVER: string;
							public static DIP: string;
							public static TRICEPS_DIP: string;
							public static CHEST_DIP: string;
							public static SHOULDER_PRESS: string;
							public static PIKE_PRESS: string;
							public static ARNOLD_PRESS: string;
							public static MILITARY_PRESS: string;
							public static LATERAL_RAISE: string;
							public static FRONT_RAISE: string;
							public static REAR_LATERAL_RAISE: string;
							public static CLEAN: string;
							public static CLEAN_JERK: string;
							public static HANG_CLEAN: string;
							public static POWER_CLEAN: string;
							public static HANG_POWER_CLEAN: string;
							public static ROW: string;
							public static UPRIGHT_ROW: string;
							public static HIGH_ROW: string;
							public static PULLUP: string;
							public static CHINUP: string;
							public static PULLDOWN: string;
							public static SHRUG: string;
							public static BACK_EXTENSION: string;
							public static GOOD_MORNING: string;
							public static BICEP_CURL: string;
							public static TRICEPS_EXTENSION: string;
							public static JM_PRESS: string;
							public static SQUAT: string;
							public static LEG_PRESS: string;
							public static LEG_CURL: string;
							public static LEG_EXTENSION: string;
							public static WALL_SIT: string;
							public static STEP_UP: string;
							public static DEADLIFT: string;
							public static SINGLE_LEG_DEADLIFT: string;
							public static STRAIGHT_LEG_DEADLIFT: string;
							public static RDL_DEADLIFT: string;
							public static RLD_DEADLIFT: string;
							public static LUNGE: string;
							public static REAR_LUNGE: string;
							public static SIDE_LUNGE: string;
							public static SITUP: string;
							public static CRUNCH: string;
							public static LEG_RAISE: string;
							public static HIP_RAISE: string;
							public static V_UPS: string;
							public static TWISTING_SITUP: string;
							public static TWISTING_CRUNCH: string;
							public static PLANK: string;
							public static SIDE_PLANK: string;
							public static HIP_THRUST: string;
							public static HIP_BRIDGE: string;
							public static SINGLE_LEG_HIP_BRIDGE: string;
							public static HIP_EXTENSION: string;
							public static RUSSIAN_TWIST: string;
							public static SWING: string;
							public static CALF_RAISE: string;
							public static STANDING_CALF_RAISE: string;
							public static SEATED_CALF_RAISE: string;
							public static CALF_PRESS: string;
							public static THRUSTER: string;
							public static JUMPING_JACK: string;
							public static BURPEE: string;
							public static HIGH_KNEE_RUN: string;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zza extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zza>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzaa extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.RawDataSet> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzaa>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzab extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal.Recurrence> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzab>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzac extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzac>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzad extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Session> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzad>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzae extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzae>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.zzae>*/;
							public constructor();
							public getSession(): com.google.android.gms.fitness.data.Session;
							public constructor(param0: com.google.android.gms.fitness.data.Session, param1: com.google.android.gms.fitness.data.DataSet);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public getDataSet(): com.google.android.gms.fitness.data.DataSet;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzaf extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.zzae>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzaf>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzag extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzag>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzah extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Subscription> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzah>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzai extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Value> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzai>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzb extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzb>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.zzb>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: string, param1: string);
							public getPackageName(): string;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzc extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.zzb>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzc>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzd extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.BleDevice> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzd>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zze extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Bucket> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zze>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzf extends com.google.android.gms.internal.fitness.zzg<com.google.android.gms.fitness.data.DataPoint,com.google.android.gms.fitness.data.DataType> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzf>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzg extends com.google.android.gms.internal.fitness.zzh<com.google.android.gms.fitness.data.DataType> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzg>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzh extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataPoint> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzh>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzi extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataSet> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzi>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzj extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzj>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzk extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataSource> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzk>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzl extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataType> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzl>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzm extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzm>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzn extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.DataUpdateNotification> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzn>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzo extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Device> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzo>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzp extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal.DurationObjective> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzp>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzq extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Field> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzq>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzr extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal.FrequencyObjective> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzr>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzs extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzs>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzt extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzt>;
							/**
							 * Constructs a new instance of the com.google.android.gms.fitness.data.zzt interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zzc(param0: com.google.android.gms.fitness.data.DataPoint): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export abstract class zzu extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.fitness.data.zzt {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzu>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzv extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.fitness.data.zzt {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzv>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzw extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.MapValue> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzw>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzx extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.Goal.MetricObjective> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzx>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzy extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.RawBucket> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzy>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module data {
						export class zzz extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.data.RawDataPoint> {
							public static class: java.lang.Class<com.google.android.gms.fitness.data.zzz>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export abstract class BleScanCallback extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.BleScanCallback>;
							public constructor();
							public onDeviceFound(param0: com.google.android.gms.fitness.data.BleDevice): void;
							public onScanStopped(): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class DataDeleteRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.DataDeleteRequest>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataDeleteRequest>;
							public constructor();
							public getDataSources(): java.util.List<com.google.android.gms.fitness.data.DataSource>;
							public hashCode(): number;
							public constructor(param0: com.google.android.gms.fitness.request.DataDeleteRequest, param1: any /* com.google.android.gms.internal.fitness.zzcq*/);
							public getEndTime(param0: java.util.concurrent.TimeUnit): number;
							public toString(): string;
							public getDataTypes(): java.util.List<com.google.android.gms.fitness.data.DataType>;
							public deleteAllData(): boolean;
							public getStartTime(param0: java.util.concurrent.TimeUnit): number;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public equals(param0: any): boolean;
							public deleteAllSessions(): boolean;
							public getSessions(): java.util.List<com.google.android.gms.fitness.data.Session>;
						}
						export module DataDeleteRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.DataDeleteRequest.Builder>;
								public addDataSource(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.request.DataDeleteRequest.Builder;
								public setTimeInterval(param0: number, param1: number, param2: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.DataDeleteRequest.Builder;
								public deleteAllData(): com.google.android.gms.fitness.request.DataDeleteRequest.Builder;
								public deleteAllSessions(): com.google.android.gms.fitness.request.DataDeleteRequest.Builder;
								public addDataType(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.request.DataDeleteRequest.Builder;
								public constructor();
								public addSession(param0: com.google.android.gms.fitness.data.Session): com.google.android.gms.fitness.request.DataDeleteRequest.Builder;
								public build(): com.google.android.gms.fitness.request.DataDeleteRequest;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class DataReadRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.DataReadRequest>;
							public static NO_LIMIT: number;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataReadRequest>;
							public constructor();
							public getFilteredDataQualityStandards(): java.util.List<java.lang.Integer>;
							public getDataSources(): java.util.List<com.google.android.gms.fitness.data.DataSource>;
							public hashCode(): number;
							public constructor(param0: com.google.android.gms.fitness.request.DataReadRequest, param1: any /* com.google.android.gms.internal.fitness.zzbh*/);
							public getAggregatedDataSources(): java.util.List<com.google.android.gms.fitness.data.DataSource>;
							public getEndTime(param0: java.util.concurrent.TimeUnit): number;
							public getBucketType(): number;
							public toString(): string;
							public getDataTypes(): java.util.List<com.google.android.gms.fitness.data.DataType>;
							public getStartTime(param0: java.util.concurrent.TimeUnit): number;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getActivityDataSource(): com.google.android.gms.fitness.data.DataSource;
							public getBucketDuration(param0: java.util.concurrent.TimeUnit): number;
							public getAggregatedDataTypes(): java.util.List<com.google.android.gms.fitness.data.DataType>;
							public equals(param0: any): boolean;
							public getLimit(): number;
						}
						export module DataReadRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.DataReadRequest.Builder>;
								public aggregate(param0: com.google.android.gms.fitness.data.DataType, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public addFilteredDataQualityStandard(param0: number): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public build(): com.google.android.gms.fitness.request.DataReadRequest;
								public aggregate(param0: com.google.android.gms.fitness.data.DataSource, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public constructor();
								public bucketByTime(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public bucketByActivityType(param0: number, param1: java.util.concurrent.TimeUnit, param2: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public bucketBySession(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public setTimeRange(param0: number, param1: number, param2: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public bucketByActivitySegment(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public enableServerQueries(): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public read(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public bucketByActivityType(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public read(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public bucketByActivitySegment(param0: number, param1: java.util.concurrent.TimeUnit, param2: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.request.DataReadRequest.Builder;
								public setLimit(param0: number): com.google.android.gms.fitness.request.DataReadRequest.Builder;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class DataSourcesRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.DataSourcesRequest>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataSourcesRequest>;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public toString(): string;
							public getDataTypes(): java.util.List<com.google.android.gms.fitness.data.DataType>;
							public constructor(param0: com.google.android.gms.fitness.request.DataSourcesRequest, param1: any /* com.google.android.gms.internal.fitness.zzbk*/);
						}
						export module DataSourcesRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.DataSourcesRequest.Builder>;
								public setDataTypes(param0: native.Array<com.google.android.gms.fitness.data.DataType>): com.google.android.gms.fitness.request.DataSourcesRequest.Builder;
								public setDataSourceTypes(param0: native.Array<number>): com.google.android.gms.fitness.request.DataSourcesRequest.Builder;
								public build(): com.google.android.gms.fitness.request.DataSourcesRequest;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class DataTypeCreateRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.DataTypeCreateRequest>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataTypeCreateRequest>;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: com.google.android.gms.fitness.request.DataTypeCreateRequest, param1: any /* com.google.android.gms.internal.fitness.zzbn*/);
							public getName(): string;
							public getFields(): java.util.List<com.google.android.gms.fitness.data.Field>;
							public toString(): string;
							public equals(param0: any): boolean;
						}
						export module DataTypeCreateRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.DataTypeCreateRequest.Builder>;
								public setName(param0: string): com.google.android.gms.fitness.request.DataTypeCreateRequest.Builder;
								public build(): com.google.android.gms.fitness.request.DataTypeCreateRequest;
								public addField(param0: com.google.android.gms.fitness.data.Field): com.google.android.gms.fitness.request.DataTypeCreateRequest.Builder;
								public constructor();
								public addField(param0: string, param1: number): com.google.android.gms.fitness.request.DataTypeCreateRequest.Builder;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class DataUpdateListenerRegistrationRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest>;
							public constructor();
							public getDataSource(): com.google.android.gms.fitness.data.DataSource;
							public getDataType(): com.google.android.gms.fitness.data.DataType;
							public constructor(param0: com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest, param1: globalAndroid.os.IBinder);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public getIntent(): globalAndroid.app.PendingIntent;
							public constructor(param0: com.google.android.gms.fitness.data.DataSource, param1: com.google.android.gms.fitness.data.DataType, param2: globalAndroid.app.PendingIntent, param3: globalAndroid.os.IBinder);
							public toString(): string;
							public equals(param0: any): boolean;
						}
						export module DataUpdateListenerRegistrationRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest.Builder>;
								public setDataType(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest.Builder;
								public setDataSource(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest.Builder;
								public constructor();
								public setPendingIntent(param0: globalAndroid.app.PendingIntent): com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest.Builder;
								public build(): com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class DataUpdateRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.DataUpdateRequest>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataUpdateRequest>;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: number, param1: number, param2: com.google.android.gms.fitness.data.DataSet, param3: globalAndroid.os.IBinder);
							public getCallbackBinder(): globalAndroid.os.IBinder;
							public getEndTime(param0: java.util.concurrent.TimeUnit): number;
							public getDataSet(): com.google.android.gms.fitness.data.DataSet;
							public toString(): string;
							public equals(param0: any): boolean;
							public constructor(param0: com.google.android.gms.fitness.request.DataUpdateRequest, param1: globalAndroid.os.IBinder);
							public getStartTime(param0: java.util.concurrent.TimeUnit): number;
						}
						export module DataUpdateRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.DataUpdateRequest.Builder>;
								public build(): com.google.android.gms.fitness.request.DataUpdateRequest;
								public setTimeInterval(param0: number, param1: number, param2: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.DataUpdateRequest.Builder;
								public setDataSet(param0: com.google.android.gms.fitness.data.DataSet): com.google.android.gms.fitness.request.DataUpdateRequest.Builder;
								public constructor();
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class GoalsReadRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.GoalsReadRequest>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.GoalsReadRequest>;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public getObjectiveTypes(): java.util.List<java.lang.Integer>;
							public getActivityNames(): java.util.List<string>;
							public toString(): string;
							public getDataTypes(): java.util.List<com.google.android.gms.fitness.data.DataType>;
							public constructor(param0: com.google.android.gms.fitness.request.GoalsReadRequest, param1: any /* com.google.android.gms.internal.fitness.zzbq*/);
							public equals(param0: any): boolean;
						}
						export module GoalsReadRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.GoalsReadRequest.Builder>;
								public addDataType(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.request.GoalsReadRequest.Builder;
								public build(): com.google.android.gms.fitness.request.GoalsReadRequest;
								public addObjectiveType(param0: number): com.google.android.gms.fitness.request.GoalsReadRequest.Builder;
								public constructor();
								public addActivity(param0: string): com.google.android.gms.fitness.request.GoalsReadRequest.Builder;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class OnDataPointListener extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.OnDataPointListener>;
							/**
							 * Constructs a new instance of the com.google.android.gms.fitness.request.OnDataPointListener interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								onDataPoint(param0: com.google.android.gms.fitness.data.DataPoint): void;
							});
							public constructor();
							public onDataPoint(param0: com.google.android.gms.fitness.data.DataPoint): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class SensorRequest extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.SensorRequest>;
							public static ACCURACY_MODE_LOW: number;
							public static ACCURACY_MODE_DEFAULT: number;
							public static ACCURACY_MODE_HIGH: number;
							public getDataSource(): com.google.android.gms.fitness.data.DataSource;
							public getDataType(): com.google.android.gms.fitness.data.DataType;
							public hashCode(): number;
							public getSamplingRate(param0: java.util.concurrent.TimeUnit): number;
							public getFastestRate(param0: java.util.concurrent.TimeUnit): number;
							public toString(): string;
							public equals(param0: any): boolean;
							public getMaxDeliveryLatency(param0: java.util.concurrent.TimeUnit): number;
							public getAccuracyMode(): number;
							public static fromLocationRequest(param0: com.google.android.gms.fitness.data.DataSource, param1: com.google.android.gms.location.LocationRequest): com.google.android.gms.fitness.request.SensorRequest;
						}
						export module SensorRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.SensorRequest.Builder>;
								public setDataSource(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.request.SensorRequest.Builder;
								public setSamplingRate(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.SensorRequest.Builder;
								public build(): com.google.android.gms.fitness.request.SensorRequest;
								public setDataType(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.request.SensorRequest.Builder;
								public setMaxDeliveryLatency(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.SensorRequest.Builder;
								public setAccuracyMode(param0: number): com.google.android.gms.fitness.request.SensorRequest.Builder;
								public setFastestRate(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.SensorRequest.Builder;
								public constructor();
								public setTimeout(param0: number, param1: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.SensorRequest.Builder;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class SessionInsertRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.SessionInsertRequest>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.SessionInsertRequest>;
							public constructor();
							public constructor(param0: com.google.android.gms.fitness.request.SessionInsertRequest, param1: any /* com.google.android.gms.internal.fitness.zzcq*/);
							public getSession(): com.google.android.gms.fitness.data.Session;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public getDataSets(): java.util.List<com.google.android.gms.fitness.data.DataSet>;
							public getAggregateDataPoints(): java.util.List<com.google.android.gms.fitness.data.DataPoint>;
							public equals(param0: any): boolean;
						}
						export module SessionInsertRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.SessionInsertRequest.Builder>;
								public setSession(param0: com.google.android.gms.fitness.data.Session): com.google.android.gms.fitness.request.SessionInsertRequest.Builder;
								public addAggregateDataPoint(param0: com.google.android.gms.fitness.data.DataPoint): com.google.android.gms.fitness.request.SessionInsertRequest.Builder;
								public constructor();
								public addDataSet(param0: com.google.android.gms.fitness.data.DataSet): com.google.android.gms.fitness.request.SessionInsertRequest.Builder;
								public build(): com.google.android.gms.fitness.request.SessionInsertRequest;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class SessionReadRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.SessionReadRequest>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.SessionReadRequest>;
							public constructor();
							public constructor(param0: com.google.android.gms.fitness.request.SessionReadRequest, param1: any /* com.google.android.gms.internal.fitness.zzck*/);
							public getDataSources(): java.util.List<com.google.android.gms.fitness.data.DataSource>;
							public hashCode(): number;
							public getEndTime(param0: java.util.concurrent.TimeUnit): number;
							public getExcludedPackages(): java.util.List<string>;
							public toString(): string;
							public getDataTypes(): java.util.List<com.google.android.gms.fitness.data.DataType>;
							public getStartTime(param0: java.util.concurrent.TimeUnit): number;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getSessionId(): string;
							public includeSessionsFromAllApps(): boolean;
							public getSessionName(): string;
							public equals(param0: any): boolean;
						}
						export module SessionReadRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.SessionReadRequest.Builder>;
								public read(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.request.SessionReadRequest.Builder;
								public build(): com.google.android.gms.fitness.request.SessionReadRequest;
								public excludePackage(param0: string): com.google.android.gms.fitness.request.SessionReadRequest.Builder;
								public setTimeInterval(param0: number, param1: number, param2: java.util.concurrent.TimeUnit): com.google.android.gms.fitness.request.SessionReadRequest.Builder;
								public setSessionId(param0: string): com.google.android.gms.fitness.request.SessionReadRequest.Builder;
								public constructor();
								public read(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.request.SessionReadRequest.Builder;
								public readSessionsFromAllApps(): com.google.android.gms.fitness.request.SessionReadRequest.Builder;
								public enableServerQueries(): com.google.android.gms.fitness.request.SessionReadRequest.Builder;
								public setSessionName(param0: string): com.google.android.gms.fitness.request.SessionReadRequest.Builder;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class StartBleScanRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.StartBleScanRequest>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.StartBleScanRequest>;
							public constructor();
							public constructor(param0: java.util.List<com.google.android.gms.fitness.data.DataType>, param1: any /* com.google.android.gms.fitness.request.zzae*/, param2: number, param3: any /* com.google.android.gms.internal.fitness.zzcq*/);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getTimeoutSecs(): number;
							public toString(): string;
							public getDataTypes(): java.util.List<com.google.android.gms.fitness.data.DataType>;
						}
						export module StartBleScanRequest {
							export class Builder extends java.lang.Object {
								public static class: java.lang.Class<com.google.android.gms.fitness.request.StartBleScanRequest.Builder>;
								public setBleScanCallback(param0: com.google.android.gms.fitness.request.BleScanCallback): com.google.android.gms.fitness.request.StartBleScanRequest.Builder;
								public setTimeoutSecs(param0: number): com.google.android.gms.fitness.request.StartBleScanRequest.Builder;
								public setDataTypes(param0: native.Array<com.google.android.gms.fitness.data.DataType>): com.google.android.gms.fitness.request.StartBleScanRequest.Builder;
								public constructor();
								public build(): com.google.android.gms.fitness.request.StartBleScanRequest;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zza extends com.google.android.gms.fitness.request.zzaf {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zza>;
							public onDeviceFound(param0: com.google.android.gms.fitness.data.BleDevice): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public onScanStopped(): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public release(): void;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzaa extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzaa>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzaa>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: any /* com.google.android.gms.internal.fitness.zzcq*/);
							public toString(): string;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzab extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzaa>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzab>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzac extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzac>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzad extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.GoalsReadRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzad>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzae extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzae>;
							/**
							 * Constructs a new instance of the com.google.android.gms.fitness.request.zzae interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								onDeviceFound(param0: com.google.android.gms.fitness.data.BleDevice): void;
								onScanStopped(): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
							public onDeviceFound(param0: com.google.android.gms.fitness.data.BleDevice): void;
							public onScanStopped(): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export abstract class zzaf extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.fitness.request.zzae {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzaf>;
							public constructor();
							public onDeviceFound(param0: com.google.android.gms.fitness.data.BleDevice): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public onScanStopped(): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzag extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.fitness.request.zzae {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzag>;
							public asBinder(): globalAndroid.os.IBinder;
							public onDeviceFound(param0: com.google.android.gms.fitness.data.BleDevice): void;
							public onScanStopped(): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzah extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzah>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzah>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: any /* com.google.android.gms.internal.fitness.zzer*/);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzai extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzah>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzai>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzaj extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzaj>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzaj>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: com.google.android.gms.fitness.data.DataType, param1: any /* com.google.android.gms.internal.fitness.zzch*/);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzak extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzaj>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzak>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzal extends com.google.android.gms.fitness.data.zzu {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzal>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public release(): void;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzam extends com.google.android.gms.common.api.internal.ListenerHolder.Notifier<com.google.android.gms.fitness.request.OnDataPointListener> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzam>;
							public onNotifyListenerFailed(): void;
							public notifyListener(param0: any): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzan extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzan>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzao extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzao>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzao>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: com.google.android.gms.fitness.request.SensorRequest, param1: any /* com.google.android.gms.fitness.data.zzt*/, param2: globalAndroid.app.PendingIntent, param3: any /* com.google.android.gms.internal.fitness.zzcq*/);
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzap extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzao>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzap>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzaq extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzaq>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzar extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzar>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzar>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public toString(): string;
							public constructor(param0: any /* com.google.android.gms.fitness.data.zzt*/, param1: globalAndroid.app.PendingIntent, param2: any /* com.google.android.gms.internal.fitness.zzcq*/);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzas extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzar>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzas>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzat extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzat>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzau extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.SessionInsertRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzau>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzav extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzav>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzaw extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.SessionReadRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzaw>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzax extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzax>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzax>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: globalAndroid.app.PendingIntent, param1: any /* com.google.android.gms.internal.fitness.zzcq*/, param2: number);
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzay extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzax>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzay>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzaz extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzaz>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzaz>*/;
							public constructor();
							public constructor(param0: com.google.android.gms.fitness.data.Session, param1: any /* com.google.android.gms.internal.fitness.zzcq*/);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzb extends com.google.android.gms.common.api.internal.ListenerHolder.Notifier<com.google.android.gms.fitness.request.BleScanCallback> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzb>;
							public onNotifyListenerFailed(): void;
							public notifyListener(param0: any): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzba extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzaz>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzba>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbb extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbb>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbb>*/;
							public constructor();
							public constructor(param0: string, param1: string, param2: any /* com.google.android.gms.internal.fitness.zzcn*/);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbc extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbb>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbc>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbd extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbd>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbd>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: globalAndroid.app.PendingIntent, param1: any /* com.google.android.gms.internal.fitness.zzcq*/);
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbe extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbd>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbe>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbf extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbf>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbg extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.StartBleScanRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbg>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbh extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbh>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbh>*/;
							public constructor();
							public constructor(param0: any /* com.google.android.gms.fitness.request.zzae*/, param1: any /* com.google.android.gms.internal.fitness.zzcq*/);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbi extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbh>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbi>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbj extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbj>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbj>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public toString(): string;
							public constructor(param0: com.google.android.gms.fitness.data.Subscription, param1: boolean, param2: any /* com.google.android.gms.internal.fitness.zzcq*/);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbk extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbj>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbk>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbl extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbl>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbl>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public toString(): string;
							public constructor(param0: string, param1: any /* com.google.android.gms.internal.fitness.zzcq*/);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbm extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbl>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbm>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbn extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbn>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbn>*/;
							public constructor();
							public constructor(param0: com.google.android.gms.fitness.data.DataType, param1: com.google.android.gms.fitness.data.DataSource, param2: any /* com.google.android.gms.internal.fitness.zzcq*/);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzbo extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzbn>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzbo>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzc extends com.google.android.gms.common.api.internal.ListenerHolder.Notifier<com.google.android.gms.fitness.request.BleScanCallback> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzc>;
							public onNotifyListenerFailed(): void;
							public notifyListener(param0: any): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzd extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzd>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zze extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zze>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zze>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public toString(): string;
							public constructor(param0: string, param1: com.google.android.gms.fitness.data.BleDevice, param2: any /* com.google.android.gms.internal.fitness.zzcq*/);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzf extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zze>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzf>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzg extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzg>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzg>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public toString(): string;
							public constructor(param0: any /* com.google.android.gms.internal.fitness.zzbe*/, param1: com.google.android.gms.fitness.data.DataType, param2: boolean);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzh extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzg>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzh>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzi extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzi>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzj extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataDeleteRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzj>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzk extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzk>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzk>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public equals(param0: any): boolean;
							public constructor(param0: com.google.android.gms.fitness.data.DataSet, param1: any /* com.google.android.gms.internal.fitness.zzcq*/, param2: boolean);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzl extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzk>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzl>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzm extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzm>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzn extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataReadRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzn>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzo extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzo>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzp extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataSourcesRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzp>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzq extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzq>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzr extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataTypeCreateRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzr>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzs extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzs>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzs>*/;
							public constructor();
							public constructor(param0: string, param1: any /* com.google.android.gms.internal.fitness.zzbn*/);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzt extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzs>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzt>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzu extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzu>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzv extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzv>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzw extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzw>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzw>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: globalAndroid.app.PendingIntent, param1: globalAndroid.os.IBinder);
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzx extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.zzw>*/ {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzx>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzy extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzy>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module request {
						export class zzz extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.request.DataUpdateRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.request.zzz>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class BleDevicesResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.BleDevicesResult>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.BleDevicesResult>;
							public constructor();
							public getStatus(): com.google.android.gms.common.api.Status;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public constructor(param0: java.util.List<com.google.android.gms.fitness.data.BleDevice>, param1: com.google.android.gms.common.api.Status);
							public getClaimedBleDevices(): java.util.List<com.google.android.gms.fitness.data.BleDevice>;
							public equals(param0: any): boolean;
							public getClaimedBleDevices(param0: com.google.android.gms.fitness.data.DataType): java.util.List<com.google.android.gms.fitness.data.BleDevice>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class DailyTotalResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.DailyTotalResult>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.DailyTotalResult>;
							public getTotal(): com.google.android.gms.fitness.data.DataSet;
							public getStatus(): com.google.android.gms.common.api.Status;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class DataReadResponse extends com.google.android.gms.common.api.Response<com.google.android.gms.fitness.result.DataReadResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.DataReadResponse>;
							public constructor();
							public getDataSet(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.data.DataSet;
							public getBuckets(): java.util.List<com.google.android.gms.fitness.data.Bucket>;
							public getStatus(): com.google.android.gms.common.api.Status;
							public constructor(param0: com.google.android.gms.fitness.result.DataReadResult);
							public getDataSet(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.data.DataSet;
							public getDataSets(): java.util.List<com.google.android.gms.fitness.data.DataSet>;
							public constructor(param0: any);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class DataReadResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.DataReadResult>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.DataReadResult>;
							public getDataSet(param0: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.fitness.data.DataSet;
							public getBuckets(): java.util.List<com.google.android.gms.fitness.data.Bucket>;
							public getStatus(): com.google.android.gms.common.api.Status;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public getDataSet(param0: com.google.android.gms.fitness.data.DataType): com.google.android.gms.fitness.data.DataSet;
							public toString(): string;
							public getDataSets(): java.util.List<com.google.android.gms.fitness.data.DataSet>;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class DataSourcesResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.DataSourcesResult>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.DataSourcesResult>;
							public constructor();
							public getDataSources(): java.util.List<com.google.android.gms.fitness.data.DataSource>;
							public getStatus(): com.google.android.gms.common.api.Status;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: java.util.List<com.google.android.gms.fitness.data.DataSource>, param1: com.google.android.gms.common.api.Status);
							public getDataSources(param0: com.google.android.gms.fitness.data.DataType): java.util.List<com.google.android.gms.fitness.data.DataSource>;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class DataTypeResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.DataTypeResult>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.DataTypeResult>;
							public constructor();
							public getDataType(): com.google.android.gms.fitness.data.DataType;
							public getStatus(): com.google.android.gms.common.api.Status;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: com.google.android.gms.common.api.Status, param1: com.google.android.gms.fitness.data.DataType);
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class GoalsResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.GoalsResult>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.GoalsResult>;
							public constructor();
							public getStatus(): com.google.android.gms.common.api.Status;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getGoals(): java.util.List<com.google.android.gms.fitness.data.Goal>;
							public constructor(param0: com.google.android.gms.common.api.Status, param1: java.util.List<com.google.android.gms.fitness.data.Goal>);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class ListSubscriptionsResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.ListSubscriptionsResult>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.ListSubscriptionsResult>;
							public constructor();
							public constructor(param0: java.util.List<com.google.android.gms.fitness.data.Subscription>, param1: com.google.android.gms.common.api.Status);
							public getSubscriptions(param0: com.google.android.gms.fitness.data.DataType): java.util.List<com.google.android.gms.fitness.data.Subscription>;
							public getStatus(): com.google.android.gms.common.api.Status;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public getSubscriptions(): java.util.List<com.google.android.gms.fitness.data.Subscription>;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class SessionReadResponse extends com.google.android.gms.common.api.Response<com.google.android.gms.fitness.result.SessionReadResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.SessionReadResponse>;
							public constructor();
							public constructor(param0: com.google.android.gms.fitness.result.SessionReadResult);
							public getStatus(): com.google.android.gms.common.api.Status;
							public getDataSet(param0: com.google.android.gms.fitness.data.Session, param1: com.google.android.gms.fitness.data.DataType): java.util.List<com.google.android.gms.fitness.data.DataSet>;
							public getDataSet(param0: com.google.android.gms.fitness.data.Session): java.util.List<com.google.android.gms.fitness.data.DataSet>;
							public constructor(param0: any);
							public getSessions(): java.util.List<com.google.android.gms.fitness.data.Session>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class SessionReadResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.SessionReadResult>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.SessionReadResult>;
							public constructor();
							public getStatus(): com.google.android.gms.common.api.Status;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public getDataSet(param0: com.google.android.gms.fitness.data.Session, param1: com.google.android.gms.fitness.data.DataType): java.util.List<com.google.android.gms.fitness.data.DataSet>;
							public hashCode(): number;
							public constructor(param0: java.util.List<com.google.android.gms.fitness.data.Session>, param1: any /* java.util.List<com.google.android.gms.fitness.data.zzae>*/, param2: com.google.android.gms.common.api.Status);
							public toString(): string;
							public getDataSet(param0: com.google.android.gms.fitness.data.Session): java.util.List<com.google.android.gms.fitness.data.DataSet>;
							public equals(param0: any): boolean;
							public getSessions(): java.util.List<com.google.android.gms.fitness.data.Session>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class SessionStopResult extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable implements com.google.android.gms.common.api.Result {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.SessionStopResult>;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.SessionStopResult>;
							public constructor();
							public getStatus(): com.google.android.gms.common.api.Status;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public constructor(param0: com.google.android.gms.common.api.Status, param1: java.util.List<com.google.android.gms.fitness.data.Session>);
							public toString(): string;
							public equals(param0: any): boolean;
							public getSessions(): java.util.List<com.google.android.gms.fitness.data.Session>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class zza extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.BleDevicesResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.zza>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class zzb extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.DailyTotalResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.zzb>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class zzc extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.DataReadResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.zzc>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class zzd extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.DataSourcesResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.zzd>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class zze extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.DataTypeResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.zze>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class zzf extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.GoalsResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.zzf>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class zzg extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.ListSubscriptionsResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.zzg>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class zzh extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.SessionReadResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.zzh>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module result {
						export class zzi extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.result.SessionStopResult> {
							public static class: java.lang.Class<com.google.android.gms.fitness.result.zzi>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module service {
						export abstract class FitnessSensorService extends globalAndroid.app.Service {
							public static class: java.lang.Class<com.google.android.gms.fitness.service.FitnessSensorService>;
							public static SERVICE_INTERFACE: string;
							public constructor();
							public onCreate(): void;
							public onUnregister(param0: com.google.android.gms.fitness.data.DataSource): boolean;
							public onTrimMemory(param0: number): void;
							public onRegister(param0: com.google.android.gms.fitness.service.FitnessSensorServiceRequest): boolean;
							public constructor(param0: globalAndroid.content.Context);
							public onBind(param0: globalAndroid.content.Intent): globalAndroid.os.IBinder;
							public onFindDataSources(param0: java.util.List<com.google.android.gms.fitness.data.DataType>): java.util.List<com.google.android.gms.fitness.data.DataSource>;
						}
						export module FitnessSensorService {
							export class zza extends com.google.android.gms.internal.fitness.zzez {
								public static class: java.lang.Class<com.google.android.gms.fitness.service.FitnessSensorService.zza>;
								public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
								public isBinderAlive(): boolean;
								public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
								public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
								public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
								public getInterfaceDescriptor(): string;
								public pingBinder(): boolean;
								public asBinder(): globalAndroid.os.IBinder;
								public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
								public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
								public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							}
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module service {
						export class FitnessSensorServiceRequest extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.fitness.service.FitnessSensorServiceRequest>;
							public static UNSPECIFIED: number;
							public static CREATOR: globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.service.FitnessSensorServiceRequest>;
							public getDataSource(): com.google.android.gms.fitness.data.DataSource;
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public hashCode(): number;
							public getSamplingRate(param0: java.util.concurrent.TimeUnit): number;
							public getDispatcher(): com.google.android.gms.fitness.service.SensorEventDispatcher;
							public getBatchInterval(param0: java.util.concurrent.TimeUnit): number;
							public toString(): string;
							public equals(param0: any): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module service {
						export class SensorEventDispatcher extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.service.SensorEventDispatcher>;
							/**
							 * Constructs a new instance of the com.google.android.gms.fitness.service.SensorEventDispatcher interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								publish(param0: com.google.android.gms.fitness.data.DataPoint): void;
								publish(param0: java.util.List<com.google.android.gms.fitness.data.DataPoint>): void;
							});
							public constructor();
							public publish(param0: java.util.List<com.google.android.gms.fitness.data.DataPoint>): void;
							public publish(param0: com.google.android.gms.fitness.data.DataPoint): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module service {
						export class zza extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.fitness.service.zza>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module service {
						export class zzb extends globalAndroid.os.Parcelable.Creator<com.google.android.gms.fitness.service.FitnessSensorServiceRequest> {
							public static class: java.lang.Class<com.google.android.gms.fitness.service.zzb>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export module service {
						export class zzc extends java.lang.Object implements com.google.android.gms.fitness.service.SensorEventDispatcher {
							public static class: java.lang.Class<com.google.android.gms.fitness.service.zzc>;
							public publish(param0: java.util.List<com.google.android.gms.fitness.data.DataPoint>): void;
							public publish(param0: com.google.android.gms.fitness.data.DataPoint): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zza extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zza>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzb extends com.google.android.gms.common.api.internal.RegisterListenerMethod<com.google.android.gms.internal.fitness.zzp,com.google.android.gms.fitness.request.BleScanCallback> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzb>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzc extends com.google.android.gms.common.api.internal.UnregisterListenerMethod<com.google.android.gms.internal.fitness.zzp,com.google.android.gms.fitness.request.BleScanCallback> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzc>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzd extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzd>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zze extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zze>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzf extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzf>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzg extends java.lang.Object {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzg>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzh extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzh>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzi extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzi>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzj extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzj>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzk extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzk>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzl extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzl>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzm extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzm>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzn extends com.google.android.gms.common.api.internal.RegisterListenerMethod<com.google.android.gms.internal.fitness.zzas,com.google.android.gms.fitness.request.OnDataPointListener> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzn>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzo extends com.google.android.gms.common.api.internal.UnregisterListenerMethod<com.google.android.gms.internal.fitness.zzas,com.google.android.gms.fitness.request.OnDataPointListener> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzo>;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module fitness {
					export class zzp extends java.lang.Object implements com.google.android.gms.common.internal.PendingResultUtil.ResultConverter<any, any> {
						public static class: java.lang.Class<com.google.android.gms.fitness.zzp>;
						public convert(param0: any): any;
						public convert(param0: com.google.android.gms.common.api.Result): any;
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zza extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zza>;
							public constructor(param0: globalAndroid.os.IBinder, param1: string);
							public asBinder(): globalAndroid.os.IBinder;
							public obtainAndWriteInterfaceToken(): globalAndroid.os.Parcel;
							public transactOneway(param0: number, param1: globalAndroid.os.Parcel): void;
							public transactAndReadExceptionReturnVoid(param0: number, param1: globalAndroid.os.Parcel): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzaa extends com.google.android.gms.internal.fitness.zzy<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzaa>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzab extends java.lang.Object /* com.google.android.gms.internal.fitness.zzn<com.google.android.gms.internal.fitness.zzbx>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzab>;
							public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
							public requiresAccount(): boolean;
							public getStartServiceAction(): string;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public isConnecting(): boolean;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getMinApkVersion(): number;
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzac extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzac>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzad extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzab,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzad>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzae<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.fitness.zzab>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzae<any>>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzaf extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzab,com.google.android.gms.fitness.FitnessOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzaf>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzag extends java.lang.Object /* com.google.android.gms.internal.fitness.zzn<com.google.android.gms.internal.fitness.zzbz>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzag>;
							public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
							public requiresAccount(): boolean;
							public getStartServiceAction(): string;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public isConnecting(): boolean;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getMinApkVersion(): number;
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzah extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzah>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzai extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzag,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzai>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzaj<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.fitness.zzag>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzaj<any>>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzak extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzag,com.google.android.gms.fitness.FitnessOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzak>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzal extends com.google.android.gms.internal.fitness.zzaj<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzal>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzam extends java.lang.Object /* com.google.android.gms.internal.fitness.zzn<com.google.android.gms.internal.fitness.zzcb>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzam>;
							public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
							public requiresAccount(): boolean;
							public getStartServiceAction(): string;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public isConnecting(): boolean;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getMinApkVersion(): number;
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzan extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzan>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzao extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzam,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzao>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzap<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.fitness.zzam>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzap<any>>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzaq extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzam,com.google.android.gms.fitness.FitnessOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzaq>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzar extends com.google.android.gms.internal.fitness.zzap<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzar>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzas extends java.lang.Object /* com.google.android.gms.internal.fitness.zzn<com.google.android.gms.internal.fitness.zzcd>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzas>;
							public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
							public requiresAccount(): boolean;
							public getStartServiceAction(): string;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public isConnecting(): boolean;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getMinApkVersion(): number;
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzat extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzat>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzau extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzas,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzau>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzav<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.fitness.zzas>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzav<any>>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzaw extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzas,com.google.android.gms.fitness.FitnessOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzaw>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzax extends com.google.android.gms.internal.fitness.zzav<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzax>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzay extends java.lang.Object /* com.google.android.gms.internal.fitness.zzn<com.google.android.gms.internal.fitness.zzcf>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzay>;
							public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
							public requiresAccount(): boolean;
							public getStartServiceAction(): string;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public isConnecting(): boolean;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getMinApkVersion(): number;
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzaz extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzaz>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzb extends globalAndroid.os.Binder implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzb>;
							public constructor();
							public onTransact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzba extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzay,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzba>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzbb<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.fitness.zzay>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbb<any>>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbc extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzay,com.google.android.gms.fitness.FitnessOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbc>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzbd extends com.google.android.gms.internal.fitness.zzbb<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbd>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbe extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbe>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzbe interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.result.DailyTotalResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzbf extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzbe {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbf>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbg extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzbe {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbg>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbh extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbh>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzbh interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.result.DataReadResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzbi extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzbh {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbi>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbj extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzbh {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbj>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbk extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbk>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzbk interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.result.DataSourcesResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzbl extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzbk {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbl>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbm extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzbk {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbm>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbn extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbn>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzbn interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.result.DataTypeResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzbo extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzbn {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbo>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbp extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzbn {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbp>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbq extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbq>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzbq interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.result.GoalsResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzbr extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzbq {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbr>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbs extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzbq {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbs>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbt extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbt>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzbt interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.request.StartBleScanRequest): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzbh*/): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zze*/): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzbl*/): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzah*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbu extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzbt {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbu>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbv extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbv>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzbv interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.request.DataTypeCreateRequest): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzs*/): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzaa*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbw extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzbv {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbw>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbx extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbx>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzbx interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.request.GoalsReadRequest): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzby extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzbx {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzby>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzbz extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzbz>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzbz interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.request.DataReadRequest): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzk*/): void;
								zza(param0: com.google.android.gms.fitness.request.DataDeleteRequest): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzg*/): void;
								zza(param0: com.google.android.gms.fitness.request.DataUpdateRequest): void;
								zza(param0: com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzw*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzc extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzc>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzca extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzbz {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzca>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcb extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcb>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzcb interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: any /* com.google.android.gms.fitness.request.zzbj*/): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzbn*/): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzaj*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcc extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzcb {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcc>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcd extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcd>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzcd interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.request.DataSourcesRequest): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzao*/): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzar*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzce extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzcd {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzce>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcf extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcf>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzcf interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: any /* com.google.android.gms.fitness.request.zzaz*/): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzbb*/): void;
								zza(param0: com.google.android.gms.fitness.request.SessionInsertRequest): void;
								zza(param0: com.google.android.gms.fitness.request.SessionReadRequest): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzax*/): void;
								zza(param0: any /* com.google.android.gms.fitness.request.zzbd*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcg extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzcf {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcg>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzch extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzch>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzch interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.result.ListSubscriptionsResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzci extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzch {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzci>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcj extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzch {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcj>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzck extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzck>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzck interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.result.SessionReadResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzcl extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzck {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcl>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcm extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzck {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcm>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcn extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcn>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzcn interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.result.SessionStopResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzco extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzcn {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzco>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcp extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzcn {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcp>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcq extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcq>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzcq interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								onResult(param0: com.google.android.gms.common.api.Status): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
							public onResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzcr extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzcq {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcr>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public onResult(param0: com.google.android.gms.common.api.Status): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcs extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzcq {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcs>;
							public asBinder(): globalAndroid.os.IBinder;
							public onResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzct extends java.lang.Object implements com.google.android.gms.fitness.BleApi {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzct>;
							public constructor();
							public unclaimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public claimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public startBleScan(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.StartBleScanRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public claimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public unclaimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public listClaimedBleDevices(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.BleDevicesResult>;
							public stopBleScan(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.BleScanCallback): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcu extends com.google.android.gms.internal.fitness.zzu {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcu>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcv extends com.google.android.gms.internal.fitness.zzu {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcv>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcw extends com.google.android.gms.internal.fitness.zzu {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcw>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcx extends com.google.android.gms.internal.fitness.zzu {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcx>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcy extends com.google.android.gms.internal.fitness.zzu {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcy>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzcz extends com.google.android.gms.internal.fitness.zzs<com.google.android.gms.fitness.result.BleDevicesResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzcz>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzd extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzd>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzd interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
							});
							public constructor();
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzda extends com.google.android.gms.internal.fitness.zzes {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzda>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdb extends java.lang.Object implements com.google.android.gms.fitness.ConfigApi {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdb>;
							public constructor();
							public createCustomDataType(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataTypeCreateRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataTypeResult>;
							public disableFit(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public readDataType(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataTypeResult>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdc extends com.google.android.gms.internal.fitness.zzy<com.google.android.gms.fitness.result.DataTypeResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdc>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdd extends com.google.android.gms.internal.fitness.zzy<com.google.android.gms.fitness.result.DataTypeResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdd>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzde extends com.google.android.gms.internal.fitness.zzaa {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzde>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdf extends com.google.android.gms.internal.fitness.zzbo {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdf>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdg extends java.lang.Object implements com.google.android.gms.fitness.GoalsApi {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdg>;
							public constructor();
							public readCurrentGoals(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.GoalsReadRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.GoalsResult>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdh extends com.google.android.gms.internal.fitness.zzae<com.google.android.gms.fitness.result.GoalsResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdh>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdi extends com.google.android.gms.internal.fitness.zzbr {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdi>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdj extends java.lang.Object implements com.google.android.gms.fitness.HistoryApi {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdj>;
							public constructor();
							public readDailyTotal(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DailyTotalResult>;
							public insertData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataSet): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public readData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataReadRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataReadResult>;
							public readDailyTotalFromLocalDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DailyTotalResult>;
							public deleteData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataDeleteRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public updateData(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataUpdateRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public registerDataUpdateListener(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataUpdateListenerRegistrationRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public unregisterDataUpdateListener(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdk extends com.google.android.gms.internal.fitness.zzal {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdk>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdl extends com.google.android.gms.internal.fitness.zzal {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdl>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdm extends com.google.android.gms.internal.fitness.zzal {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdm>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdn extends com.google.android.gms.internal.fitness.zzal {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdn>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdo extends com.google.android.gms.internal.fitness.zzal {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdo>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdp extends com.google.android.gms.internal.fitness.zzaj<com.google.android.gms.fitness.result.DataReadResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdp>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdq extends com.google.android.gms.internal.fitness.zzaj<com.google.android.gms.fitness.result.DailyTotalResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdq>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdr extends com.google.android.gms.internal.fitness.zzbf {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdr>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzds extends com.google.android.gms.internal.fitness.zzbi {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzds>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdt extends java.lang.Object implements com.google.android.gms.fitness.RecordingApi {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdt>;
							public constructor();
							public subscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public subscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public unsubscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.Subscription): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public listSubscriptions(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.ListSubscriptionsResult>;
							public unsubscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public unsubscribe(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataSource): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public listSubscriptions(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.DataType): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.ListSubscriptionsResult>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdu extends com.google.android.gms.internal.fitness.zzap<com.google.android.gms.fitness.result.ListSubscriptionsResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdu>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdv extends com.google.android.gms.internal.fitness.zzap<com.google.android.gms.fitness.result.ListSubscriptionsResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdv>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdw extends com.google.android.gms.internal.fitness.zzar {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdw>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdx extends com.google.android.gms.internal.fitness.zzar {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdx>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdy extends com.google.android.gms.internal.fitness.zzar {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdy>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzdz extends com.google.android.gms.internal.fitness.zzci {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzdz>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zze extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zze>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzea extends java.lang.Object implements com.google.android.gms.fitness.SensorsApi {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzea>;
							public constructor();
							public findDataSources(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.DataSourcesRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.DataSourcesResult>;
							public remove(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public add(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SensorRequest, param2: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public add(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SensorRequest, param2: com.google.android.gms.fitness.request.OnDataPointListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public remove(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.OnDataPointListener): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzeb extends com.google.android.gms.internal.fitness.zzav<com.google.android.gms.fitness.result.DataSourcesResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzeb>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzec extends com.google.android.gms.internal.fitness.zzax {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzec>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzed extends com.google.android.gms.internal.fitness.zzax {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzed>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzee extends java.lang.Object implements com.google.android.gms.fitness.SessionsApi {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzee>;
							public constructor();
							public registerForSessions(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public stopSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.SessionStopResult>;
							public insertSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SessionInsertRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public readSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.SessionReadRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.SessionReadResult>;
							public unregisterForSessions(param0: com.google.android.gms.common.api.GoogleApiClient, param1: globalAndroid.app.PendingIntent): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public startSession(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.Session): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzef extends com.google.android.gms.internal.fitness.zzbd {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzef>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzeg extends com.google.android.gms.internal.fitness.zzbb<com.google.android.gms.fitness.result.SessionStopResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzeg>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzeh extends com.google.android.gms.internal.fitness.zzbd {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzeh>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzei extends com.google.android.gms.internal.fitness.zzbb<com.google.android.gms.fitness.result.SessionReadResult> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzei>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzej extends com.google.android.gms.internal.fitness.zzbd {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzej>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzek extends com.google.android.gms.internal.fitness.zzbd {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzek>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzel extends com.google.android.gms.internal.fitness.zzcl {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzel>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzem extends com.google.android.gms.internal.fitness.zzco {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzem>;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzen extends com.google.android.gms.internal.fitness.zzcr {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzen>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public constructor(param0: com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder<com.google.android.gms.common.api.Status>);
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public onResult(param0: com.google.android.gms.common.api.Status): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzeo extends com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzeo>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzep extends com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzep>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzeq extends java.lang.Object implements com.google.android.gms.fitness.BleApi {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzeq>;
							public constructor();
							public unclaimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public claimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: string): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public startBleScan(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.StartBleScanRequest): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public claimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public unclaimBleDevice(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.data.BleDevice): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
							public listClaimedBleDevices(param0: com.google.android.gms.common.api.GoogleApiClient): com.google.android.gms.common.api.PendingResult<com.google.android.gms.fitness.result.BleDevicesResult>;
							public stopBleScan(param0: com.google.android.gms.common.api.GoogleApiClient, param1: com.google.android.gms.fitness.request.BleScanCallback): com.google.android.gms.common.api.PendingResult<com.google.android.gms.common.api.Status>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzer extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzer>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzer interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: com.google.android.gms.fitness.result.BleDevicesResult): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzes extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzer {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzes>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzet extends com.google.android.gms.internal.fitness.zza implements com.google.android.gms.internal.fitness.zzer {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzet>;
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzeu extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzeu>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.fitness.zzeu>*/;
							public constructor();
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public constructor(param0: java.util.List<com.google.android.gms.fitness.data.DataType>);
							public toString(): string;
							public getDataTypes(): java.util.List<com.google.android.gms.fitness.data.DataType>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzev extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.fitness.zzeu>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzev>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzew extends com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzew>;
							public static CREATOR: any /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.fitness.zzew>*/;
							public constructor();
							public getDataSource(): com.google.android.gms.fitness.data.DataSource;
							public constructor(param0: com.google.android.gms.fitness.data.DataSource);
							public writeToParcel(param0: globalAndroid.os.Parcel, param1: number): void;
							public toString(): string;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzex extends java.lang.Object /* globalAndroid.os.Parcelable.Creator<com.google.android.gms.internal.fitness.zzew>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzex>;
							public constructor();
							public newArray(param0: number): native.Array<any>;
							public createFromParcel(param0: globalAndroid.os.Parcel): any;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzey extends java.lang.Object implements globalAndroid.os.IInterface {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzey>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzey interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zza(param0: any /* com.google.android.gms.internal.fitness.zzeu*/, param1: any /* com.google.android.gms.internal.fitness.zzbk*/): void;
								zza(param0: com.google.android.gms.fitness.service.FitnessSensorServiceRequest, param1: any /* com.google.android.gms.internal.fitness.zzcq*/): void;
								zza(param0: any /* com.google.android.gms.internal.fitness.zzew*/, param1: any /* com.google.android.gms.internal.fitness.zzcq*/): void;
								asBinder(): globalAndroid.os.IBinder;
							});
							public constructor();
							public asBinder(): globalAndroid.os.IBinder;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzez extends com.google.android.gms.internal.fitness.zzb implements com.google.android.gms.internal.fitness.zzey {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzez>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public dispatchTransaction(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzf extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzf>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzfa extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzfa>;
							public static getMimeType(param0: string): string;
							public static getName(param0: number): string;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzg<DP, DT>  extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzg<any,any>>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzg<any,any> interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zzb(): any /* com.google.android.gms.internal.fitness.zzh<DT>*/;
								zzb(param0: DP): string;
								zza(param0: DP): DT;
								zza(param0: DP, param1: java.util.concurrent.TimeUnit): number;
								zzc(param0: DP, param1: number): boolean;
								zzb(param0: DP, param1: number): number;
								zza(param0: DP, param1: number): number;
							});
							public constructor();
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzh<DT>  extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzh<any>>;
							/**
							 * Constructs a new instance of the com.google.android.gms.internal.fitness.zzh<any> interface with the provided implementation. An empty constructor exists calling super() when extending the interface class.
							 */
							public constructor(implementation: {
								zzb(param0: string): boolean;
								zzd(param0: DT): string;
								zzc(param0: DT): number;
								zzf(param0: DT, param1: number): string;
								zze(param0: DT, param1: number): boolean;
								zzd(param0: DT, param1: number): number;
							});
							public constructor();
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzi extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzi>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzj extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzj>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzk extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzk>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzl extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzl>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzm extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzm>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzn<T>  extends com.google.android.gms.common.internal.GmsClient<any> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzn<any>>;
							public getStartServiceAction(): string;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public validateScopes(param0: java.util.Set<com.google.android.gms.common.api.Scope>): java.util.Set<com.google.android.gms.common.api.Scope>;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param7: string);
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: number, param3: com.google.android.gms.common.internal.ClientSettings);
							public getConnectionHint(): globalAndroid.os.Bundle;
							public createServiceInterface(param0: globalAndroid.os.IBinder): any;
							public requiresAccount(): boolean;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param4: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener, param5: com.google.android.gms.common.internal.ClientSettings);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param4: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener, param5: string);
							public isConnecting(): boolean;
							public getMinApkVersion(): number;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: number, param3: com.google.android.gms.common.internal.ClientSettings, param4: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param5: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Looper, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailability, param4: number, param5: com.google.android.gms.common.internal.ClientSettings, param6: com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, param7: com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener);
							public constructor(param0: globalAndroid.content.Context, param1: globalAndroid.os.Handler, param2: com.google.android.gms.common.internal.GmsClientSupervisor, param3: com.google.android.gms.common.GoogleApiAvailabilityLight, param4: number, param5: com.google.android.gms.common.internal.BaseGmsClient.BaseConnectionCallbacks, param6: com.google.android.gms.common.internal.BaseGmsClient.BaseOnConnectionFailedListener);
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzo extends com.google.android.gms.internal.fitness.zzbl {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzo>;
							public constructor();
							public dump(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public queryLocalInterface(param0: string): globalAndroid.os.IInterface;
							public pingBinder(): boolean;
							public dumpAsync(param0: java.io.FileDescriptor, param1: native.Array<string>): void;
							public transact(param0: number, param1: globalAndroid.os.Parcel, param2: globalAndroid.os.Parcel, param3: number): boolean;
							public constructor(param0: com.google.android.gms.common.api.internal.BaseImplementation.ResultHolder<com.google.android.gms.fitness.result.DataSourcesResult>);
							public getInterfaceDescriptor(): string;
							public asBinder(): globalAndroid.os.IBinder;
							public isBinderAlive(): boolean;
							public constructor(param0: string);
							public linkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): void;
							public dump(param0: java.io.FileDescriptor, param1: java.io.PrintWriter, param2: native.Array<string>): void;
							public unlinkToDeath(param0: globalAndroid.os.IBinder.DeathRecipient, param1: number): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzp extends java.lang.Object /* com.google.android.gms.internal.fitness.zzn<com.google.android.gms.internal.fitness.zzbt>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzp>;
							public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
							public requiresAccount(): boolean;
							public getStartServiceAction(): string;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public isConnecting(): boolean;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getMinApkVersion(): number;
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzq extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzq>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzr extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzp,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzr>;
							public constructor();
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzs<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.fitness.zzp>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzs<any>>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzt extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzp,com.google.android.gms.fitness.FitnessOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzt>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzu extends com.google.android.gms.internal.fitness.zzs<com.google.android.gms.common.api.Status> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzu>;
							public setResult(param0: any): void;
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzv extends java.lang.Object /* com.google.android.gms.internal.fitness.zzn<com.google.android.gms.internal.fitness.zzbv>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzv>;
							public static API: com.google.android.gms.common.api.Api<com.google.android.gms.common.api.Api.ApiOptions.NoOptions>;
							public requiresAccount(): boolean;
							public getStartServiceAction(): string;
							public disconnect(): void;
							public isConnected(): boolean;
							public providesSignIn(): boolean;
							public requiresGooglePlayServices(): boolean;
							public getServiceBrokerBinder(): globalAndroid.os.IBinder;
							public getServiceDescriptor(): string;
							public dump(param0: string, param1: java.io.FileDescriptor, param2: java.io.PrintWriter, param3: native.Array<string>): void;
							public isConnecting(): boolean;
							public getAvailableFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getMinApkVersion(): number;
							public getEndpointPackageName(): string;
							public getSignInIntent(): globalAndroid.content.Intent;
							public connect(param0: com.google.android.gms.common.internal.BaseGmsClient.ConnectionProgressReportCallbacks): void;
							public onUserSignOut(param0: com.google.android.gms.common.internal.BaseGmsClient.SignOutCallbacks): void;
							public getRequiredFeatures(): native.Array<com.google.android.gms.common.Feature>;
							public getConnectionHint(): globalAndroid.os.Bundle;
							public getRemoteService(param0: com.google.android.gms.common.internal.IAccountAccessor, param1: java.util.Set<com.google.android.gms.common.api.Scope>): void;
							public requiresSignIn(): boolean;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzw extends java.lang.Object {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzw>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzx extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzv,com.google.android.gms.common.api.Api.ApiOptions.NoOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzx>;
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export abstract class zzy<R>  extends java.lang.Object /* com.google.android.gms.common.api.internal.BaseImplementation.ApiMethodImpl<any,com.google.android.gms.internal.fitness.zzv>*/ {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzy<any>>;
							public constructor();
							public setResult(param0: any): void;
							public constructor(param0: globalAndroid.os.Looper);
							public constructor(param0: com.google.android.gms.common.api.GoogleApiClient);
							public setFailedResult(param0: com.google.android.gms.common.api.Status): void;
							public constructor(param0: com.google.android.gms.common.api.Api.AnyClientKey<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
							public constructor(param0: com.google.android.gms.common.api.internal.BasePendingResult.CallbackHandler<any>);
							public constructor(param0: com.google.android.gms.common.api.Api<any>, param1: com.google.android.gms.common.api.GoogleApiClient);
						}
					}
				}
			}
		}
	}
}

declare module com {
	export module google {
		export module android {
			export module gms {
				export module internal {
					export module fitness {
						export class zzz extends com.google.android.gms.common.api.Api.AbstractClientBuilder<com.google.android.gms.internal.fitness.zzv,com.google.android.gms.fitness.FitnessOptions> {
							public static class: java.lang.Class<com.google.android.gms.internal.fitness.zzz>;
						}
					}
				}
			}
		}
	}
}

//Generics information:
//com.google.android.gms.internal.fitness.zzae:1
//com.google.android.gms.internal.fitness.zzaj:1
//com.google.android.gms.internal.fitness.zzap:1
//com.google.android.gms.internal.fitness.zzav:1
//com.google.android.gms.internal.fitness.zzbb:1
//com.google.android.gms.internal.fitness.zzg:2
//com.google.android.gms.internal.fitness.zzh:1
//com.google.android.gms.internal.fitness.zzn:1
//com.google.android.gms.internal.fitness.zzs:1
//com.google.android.gms.internal.fitness.zzy:1

