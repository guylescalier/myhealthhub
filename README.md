# MyHealthHub
Mobile app to synchronize data between health data providers (Samsung, Google, Withings, ...)

## State
- Samsung Health connection : Done
- Google Fit connection : Done
- Withings connection : Working on it
